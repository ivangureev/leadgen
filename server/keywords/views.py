# -*- coding: utf-8 -*-

from django.db import models

from tools.rest import RESTHandler
from tools.response import Response
from keywords.models import KeySet, KeyWord

@Response.standart
def keysets(request, id):
    handler = RESTHandler(
        request=request,
        model=KeySet,
        annotate_fields={'count': models.Count('keywords')},
        element_id=id)
    
    if handler.format == 'GET':
        return handler.read()  
    elif handler.format == 'POST':
        return handler.create()
    elif handler.format == 'PUT':
        return handler.update()
    elif handler.format == 'DELETE':
        return handler.delete()
    else:
        raise Exception("Undefined method")
        
@Response.standart
def selected_keywords(request, id):
    handler = RESTHandler(
        request=request,
        m2m={'keysets': ('keysets', KeySet)},
        model=KeyWord,
        element_id=id)
    
    if handler.format == 'GET':
        return handler.read()    
    elif handler.format == 'POST':
        return handler.create()
    elif handler.format == 'PUT':
        return handler.update()
    elif handler.format == 'DELETE':
        return handler.delete()
    else:
        raise Exception("Undefined method")
        
@Response.standart
def keywords(request, id):
    handler = RESTHandler(
        request=request,
        m2m={'keysets': ('keysets', KeySet)},
        model=KeyWord,
        annotate_fields={'count': models.Count('keysets')},
        element_id=id)
    
    if handler.format == 'GET':
        return handler.read()  
    elif handler.format == 'POST':
        return handler.create()
    elif handler.format == 'PUT':
        return handler.update()
    elif handler.format == 'DELETE':
        return handler.delete()
    else:
        raise Exception("Undefined method")
    