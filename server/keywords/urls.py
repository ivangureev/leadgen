# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url

import views

urlpatterns = patterns('',
   url(r'^keysets/(?P<id>\d*)$\/?', views.keysets),
   url(r'^keywords/(?P<id>\d*)$\/?', views.keywords),
   url(r'^selected_keywords/(?P<id>\d*)$\/?', views.selected_keywords),
)