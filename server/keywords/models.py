# -*- coding: utf-8 -*-

from django.db import models

from categories.models import Category

class KeySet(models.Model):
    """
    Class to represent sets of key words. It groups key words to sets
    """
    category = models.ForeignKey(Category)
    name = models.CharField(max_length=50)
    price = models.DecimalField(max_digits=11, decimal_places=2)
    last_update = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return self.name

    class Meta:
        db_table = "s_keys_sets"
        
class KeyWord(models.Model):
    """
    Class to represent keyword items. Any keyword is a part of KeySet 
    """
    # optional relations
    keysets = models.ManyToManyField(KeySet, db_table="s_keywords_comm", related_name="keywords")
    # mandatory relation
    category = models.ForeignKey(Category, related_name="keywords")
    key = models.CharField(max_length=255)
    last_update = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return self.key
    
    class Meta:
        db_table = "s_key_words"
