# -*- coding: utf-8 -*-
# Django settings for newproject project.

# DEBUG = False
DEBUG = True
TEMPLATE_DEBUG = True

ADMINS = (
    ('Ivan Gureev', 'ivangureev@gmail.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'sales_system',                      # Or path to database file if using sqlite3.
        'USER': 'sas_user',                      # Not used with sqlite3.
        'PASSWORD': 'lemur',                  # Not used with sqlite3.
        # 'USER': 'sales_system',                      # Not used with sqlite3.
        # 'PASSWORD': 'que5t10n',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# backup system
# DBBACKUP_STORAGE = 'dbbackup.storage.filesystem_storage'
# DBBACKUP_FILESYSTEM_DIRECTORY = '/home/www-dev/django.dev.webpp.ru/dumps'

DBBACKUP_STORAGE = 'dbbackup.storage.s3_storage'
DBBACKUP_S3_BUCKET = 'salesystemsbackups'
DBBACKUP_S3_ACCESS_KEY = 'AKIAJURURKA4OMAUNMPA'
DBBACKUP_S3_SECRET_KEY = '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'

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Moscow'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'ru-ru'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = '/home/worker/server/public'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = 'http://192.168.1.254:8000/public/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'nxi&amp;tq*qpe!o@@nfv%ehbm))&amp;)!^by2ha=y9ao&amp;&amp;dn9%k106i('

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    #'django.middleware.csrf.CsrfViewMiddleware',              YOU MUSH FIX THIS BACKDOOR
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'raven.contrib.django.middleware.Sentry404CatchMiddleware'
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'newproject.urls' 

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'newproject.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    "/home/worker/server/templates",
)

SENTRY_DSN = 'http://faf97f22b87f4352847ea09d542376d2:ec67e77c3f184f3eac763d5b0ad35a48@176.9.24.81:9000/2'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'djcelery',
    'raven.contrib.django',
    'raven.contrib.django.celery',
    'dbbackup',

    'catalogparser',
    'categories',
    'keywords',
    'spider',
    'competitors',
    'uploading',
    'constants',

    # Uncomment the next line to enable admin documentation:
    #'django.contrib.admindocs',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'root': {
        'level': 'ERROR',
        'handlers': ['sentry'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'sentry': {
            'level': 'INFO',
            'class': 'raven.contrib.django.handlers.SentryHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'django.db.backends': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
        },
        'raven': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'sentry.errors': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
        },
        'server': {
            'level': 'DEBUG',
            'handlers': ['sentry'],
            'propagate': False,
        },
        'celery_server': {
            'level': 'INFO',
            'handlers': ['sentry'],
            'propagate': False,
        },
    },
}

import os
curpath = os.path.dirname(__file__)
TEMPLATE_STATIC_PATH = "{0}/../templates/".format(curpath)

### Django-celery configs
import djcelery
djcelery.setup_loader()
from kombu import Exchange, Queue

CELERY_REDIRECT_STDOUTS = False

# Broker settings
BROKER_URL = "amqp://celery:lemon@localhost:5672/celeryhost"

# Additional settings
CELERY_IMPORTS = ("spider.tasks", "uploading.tasks")

# Max task executing time(soft)
CELERYD_TASK_SOFT_TIME_LIMIT = 120

# Max task executing time(hard)
CELERYD_TASK_TIME_LIMIT = 360

# Routing
CELERY_QUEUES = (
    Queue('local_q', Exchange('ex1')),
    Queue('pushing_results_q', Exchange('ex2')),
    Queue('periodic_q', Exchange('ex3')),
)

CELERY_DEFAULT_EXCHANGE = 'default'

# Task: queue to send
CELERY_ROUTES = {
    'catalogparser.tasks.parse_2gis': {'queue': 'local_q'},
    'worker.get_domain_info': {'queue': 'getting_results_q'},
    'worker.get_domain_availability': {'queue': 'getting_results_q'},
    'worker.get_serp_yandex': {'queue': 'getting_results_q'},
    'uploading.tasks.send_units': {'queue': 'local_q'},
    'uploading.tasks.upload': {'queue': 'local_q'},
    'uploading.tasks.set_coeffs': {'queue': 'local_q'},
    'uploading.tasks.send_to_queue': {'queue': 'local_q'},
    'spider.tasks.refresh_sites_activity': {'queue': 'periodic_q'},
    'spider.tasks.refresh_sites_authority': {'queue': 'periodic_q'},
    'spider.tasks.refresh_serp': {'queue': 'periodic_q'},
    'spider.tasks.regenerate_cross_serp': {'queue': 'periodic_q'}} 

# Periodic Tasks
CELERY_TIMEZONE = 'UTC'

from celery.schedules import crontab

if DEBUG:
    CELERYBEAT_SCHEDULE = {
        'refresh_sites_activity': {
            'task': 'spider.tasks.refresh_sites_activity',
            'schedule': crontab(hour=1),
        },
        'refresh_sites_authority': {
            'task': 'spider.tasks.refresh_sites_authority',
            'schedule': crontab(hour=1),
        },
        'refresh_serp': {
            'task': 'spider.tasks.refresh_serp',
            'schedule': crontab(hour=1),
        },
        'regenerate_cross_serp': {
            'task': 'spider.tasks.regenerate_cross_serp',
            'schedule': crontab(hour=1),
        },
    }
else:
    CELERYBEAT_SCHEDULE = {
        'refresh_sites_activity': {
            'task': 'spider.tasks.refresh_sites_activity',
            'schedule': crontab(hour=1, day_of_week='sun'),
        },
        'refresh_sites_authority': {
            'task': 'spider.tasks.refresh_sites_authority',
            'schedule': crontab(hour=1, day_of_week='sun'),
        },
        'refresh_serp': {
            'task': 'spider.tasks.refresh_serp',
            'schedule': crontab(hour=1, day_of_week='sun'),
        },
        'regenerate_cross_serp': {
            'task': 'spider.tasks.regenerate_cross_serp',
            'schedule': crontab(hour=13, day_of_week='sun'),
        },
    }

# Celery logging

import logging
from celery import signals

logger = logging.getLogger('celery_server')

def worker_ready_log(signal, **kw):
    sender = kw['sender']
    hostname = getattr(sender, 'hostname', 'Undefined worker')
    logger.info('Worker started', exc_info=True, extra={
        'culprit': hostname
    })

def task_prerun_log(sender, task_id, task, signal, args, kwargs, **kw):
    logger.info('Start task running...', exc_info=True, extra={
        'culprit': task.name,
        'position arguments': args,
        'key arguments': kwargs,
        'task_id': task_id
    })

def task_success_log(result, sender, signal, **kw):
    logger.info('Task success', exc_info=True, extra={
        'culprit': sender.name,
        'result': result,
    })

def task_failure_log(exception, traceback, sender, task_id, signal, args, kwargs, einfo, **kw):
    logger.error('Task failure', exc_info=True, extra={
        'culprit': sender.name,
        'exception': exception,
        'traceback': traceback,
        'position arguments': args,
        'key arguments': kwargs,
        'task_id': task_id
    })

signals.task_prerun.connect(task_prerun_log)
signals.task_success.connect(task_success_log)
signals.task_failure.connect(task_failure_log)
signals.worker_ready.connect(worker_ready_log)