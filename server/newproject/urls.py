# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.conf.urls.defaults import *
from django.contrib import admin
from django.conf import settings

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'newproject.views.home'),
    
    url(r'^catalogparser/', include('catalogparser.urls')),
    url(r'^categories/', include('categories.urls')),
    url(r'^keywords/', include('keywords.urls')),
    url(r'^spider/', include('spider.urls')),
    url(r'^competitors/', include('competitors.urls')),
    url(r'^uploading/', include('uploading.urls')),
    url(r'^constants/', include('constants.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    (r'^public/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
)
