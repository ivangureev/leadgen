Ext.define('Sales.Utilities', {
    statics: {
        addM2M: function (record, m2mField, addedId) {
            relationIds = eval(record.data[m2mField])
            relationIds.push(addedId);
            
            record.set(m2mField, Ext.encode(relationIds))
            record.store.commitChanges();
        },
        removeM2M: function (record, m2mField, removedId) {
            relationIds = eval(record.data[m2mField])
            relationIds.splice(relationIds.indexOf(removedId), 1);
            
            record.set(m2mField, Ext.encode(relationIds))
            record.store.commitChanges();
        },
        removeM2MorElement: function (store, record, m2mField, removedId) {
            this.removeM2M(record, m2mField, removedId);
            relationIds = eval(record.data[m2mField])
            if (relationIds.length == 0)
                store.remove(record);
        },
        // // checkUniq: function (store, chechingRecords, fieldName) {
        // checkUniq: function (store, chechingRecords, fieldValueGetter) {
        //     for(var index in chechingRecords){
        //         record = chechingRecords[index];
        //         value = fieldValueGetter(record);
        //         recs = store.queryBy(function (record, id) {
        //             console.log(record);
        //             if(fieldValueGetter(record) == value)
        //                 return true;
        //             else
        //                 return false;
        //         });

        //         console.log("!");
        //         console.log(recs);
        //         console.log("?");

        //         if(recs.length != 0)
        //             return false
        //     }
        //     return true;
        // },

        // for local checking. findRecord uses just filtred storage
        // need for normal working d&d
        checkUniq: function (store, chichingRecords, fieldName) {
            for(var index in chichingRecords){
                record = chichingRecords[index];
                value = record.data[fieldName];
                rec = store.findRecord(fieldName, value);
                
                // if already exist
                if(rec)
                    return false
            }
            return true;
        },
        lazyStoreload: function (store) {
            setTimeout(function() { 
                store.load();
            }, 100)
        },
        formatDate: function(value, p, record) {
            return Ext.Date.dateFormat(value, 'd.m.Y G:i:s')
        },
        formatPercentage: function(value, p, record) {
            return Ext.String.format("{0}%", value)
        },
        formatDomain: function(value, p, record) {
            return Ext.String.format(
                '<a href="http://{0}" target="_blank">{1}</a>',
                value,
                record.data.competitor_title
            );
        },
        formatRubles: function(value) {
            return Ext.String.format("{0} p.", value)
        },
    }
});