Ext.define('Sales.view.upload.List' ,{
    extend: 'Ext.grid.Panel',
    alias : 'widget.uploadlist',
    title : 'Uploads',
    
    store: 'Uploads',
    disabled: true,
    
    selType: 'rowmodel',
    
    dockedItems: [{
        xtype: 'toolbar',
        items: [{
            xtype: 'button',
            action: 'make-upload',
            text: 'Make upload for category',
            iconCls: 'icon-add',
        }]
    },{
        xtype: 'pagingtoolbar',
        store: 'Uploads',
        dock: 'bottom',
        displayInfo: true,
    }],
    
    features: [{
        ftype: 'filters',
        encode: true,
        paramPrefix: "find",
        filters: [{
            type: 'string',
            dataIndex: 'unit_name'
        },{
            type: 'string',
            dataIndex: 'domains'
        },{
            type: 'numeric',
            dataIndex: 'step'
        },{
            type: 'date',
            dataIndex: 'last_update'
        }]
    }],

    initComponent: function() {
        this.columns = [
            {
                header: 'Unit',  
                dataIndex: 'unit_name',  
                flex: 1
            },{
                header: 'Domains',  
                dataIndex: 'domains',  
                sortable: false,
                flex: 1
            },{
                header: 'Step',  
                dataIndex: 'step',  
                flex: 1
            },{
                header: 'Upload time',
                dataIndex: 'last_update',
                align: 'right',
                width: 150,
                renderer: Sales.Utilities.formatDate
            },
        ];

        this.callParent(arguments);
    }
});