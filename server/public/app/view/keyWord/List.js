Ext.define('Sales.view.keyWord.List' ,{
    extend: 'Ext.grid.Panel',
    alias : 'widget.keyWordlist',
    title : 'Key Words',
    
    store: 'Keywords',
    disabled: true,
    
    selType: 'rowmodel',
    
    plugins: [{
        ptype: 'cellediting'
    }],
    
    multiSelect: true,
    viewConfig: {
        copy: true,
        plugins: {
            ptype: 'gridviewdragdrop',
            dragGroup: 'keywords_firstGridDDGroup',
            // dropGroup: 'keywords_secondGridDDGroup'
        }
    },
    
    dockedItems: [{
        xtype: 'toolbar',
        items: [{
            xtype: 'button',
            action: 'add',
            text: 'Add keywords',
            iconCls: 'icon-add',
        }, '-', {
            xtype: 'button',
            text: 'Remove keywords',
            iconCls: 'icon-delete',
            action: 'delete',
        }]
    },{
        xtype: 'pagingtoolbar',
        store: 'Keywords',
        dock: 'bottom',
        displayInfo: true,
    }],
    
    features: [{
        ftype: 'filters',
        encode: true,
        paramPrefix: "find",
        filters: [{
            type: 'string',
            dataIndex: 'key'
        },{
            type: 'numeric',
            dataIndex: 'count'
        }]
    }],

    initComponent: function() {
        this.columns = [
            {
                header: 'Keyword',  
                dataIndex: 'key',  
                flex: 1
            },{
                header: 'Count',  
                dataIndex: 'count',  
                flex: 1,
                align: 'center',
            },
        ];

        this.callParent(arguments);
    }
});