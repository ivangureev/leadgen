Ext.define('Sales.view.keyWord.Create', {
    extend: 'Ext.window.Window',
    alias : 'widget.createkeyword',

    title : 'Create Key words',
    layout: 'fit',
    autoShow: true,
    modal: true,

    initComponent: function() {
        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        this.items = [
            {
                xtype: 'form',
                bodyPadding: 10,
                items: [
                    {
                        xtype: 'textareafield',
                        name : 'keywords',
                        fieldLabel: 'Key words',
                        minHeight: 300,
                        minWidth: 450,
                        grow: true,
                        allowBlank: false,
                        afterLabelTextTpl: required,
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text: 'Save',
                action: 'save'
            },
            {
                text: 'Cancel',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    }
});