Ext.define('Sales.view.keyWordExpanded.Chartcontainer' ,{
    extend: 'Ext.Panel',
    requires: ['Sales.view.keyWordExpanded.Chart'],
    alias : 'widget.keyWordExpandedchartcontainer',
    title : 'Keywords',

    disabled: true,

    layout: 'fit',

    lbar: [{
    		xtype: 'button',
            action: 'add',
            iconCls: 'icon-add',
            tooltip: 'Add to selected competitors'
        },
    ],

    items: [{
        xtype: 'keyWordExpandedchart',
        flex:1,
    }]
});