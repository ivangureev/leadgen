Ext.define('Sales.view.keyWordExpanded.Chart' ,{
    extend: 'Ext.chart.Chart',
    xtype: 'chart',
    alias : 'widget.keyWordExpandedchart',
    
    style: 'background:#fff',
    animate: true,
    shadow: true,
    store: 'Keywordsexpanded',

    axes: [{
        type: 'Numeric',
        position: 'left',
        fields: ['related_position_yandex'],
        title: 'Top positions, %',
        grid: true,
        minimum: 0,
        maximum: 100,
        majorTickSteps: 9,
        label: {
            renderer: function(v) {
                return Ext.String.format("{0}%",v);
            },
        }

    }, {
        type: 'Category',
        position: 'bottom',
        fields: ['key'],
        title: 'Keywords',
        label: {
            renderer: function(v) {
                return Ext.String.ellipsis(v, 15, false);
            },
            font: '11px',
            rotate: {
                degrees: 270
            }
        }
    }],
    series: [{
        type: 'column',
        axis: 'left',
        highlight: true,
        tips: {
            trackMouse: true,
            width: 140,
            renderer: function(storeItem, item) {
                this.setTitle(storeItem.get('key') + ': ' + storeItem.get('position_yandex'));
            }
        },
        label: {
            display: 'insideEnd',
            'text-anchor': 'middle',
            field: 'related_position_yandex',
            color: '#333'
        },
        xField: 'key',
        yField: 'related_position_yandex'
    }]
});

