Ext.define('Sales.view.keyWordExpanded.List' ,{
    extend: 'Ext.grid.Panel',
    alias : 'widget.keyWordExpandedlist',
    title : 'Key words',
    
    store: 'Keywordsexpanded',
    disabled: true,
    
    selType: 'rowmodel',
    
    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'Keywordsexpanded',
        dock: 'bottom',
        displayInfo: true,
    }],
    
    features: [{
        ftype: 'filters',
        encode: true,
        paramPrefix: "find",
        filters: [{
            type: 'string',
            dataIndex: 'key'
        },{
            type: 'numeric',
            dataIndex: 'position_yandex'
        }]
    }],
    
    initComponent: function() {
        this.columns = [
            {
                header: 'Key',
                dataIndex: 'key',
                flex: 1,
            },{
                header: 'Yandex position',
                dataIndex: 'position_yandex',
                flex: 1,
                align: 'center',
            }
        ];

        this.callParent(arguments);
    }
});

