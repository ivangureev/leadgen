Ext.define('Sales.view.constant.List' ,{
    extend: 'Ext.grid.Panel',
    require: ['Ext.ux.grid.FiltersFeature'],
    alias : 'widget.constantlist',
    title : 'All Constants',
    
    store: 'Constants',
    
    selType: 'rowmodel',

    plugins: [{
        ptype: 'cellediting'
    }],
    
    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'Constants',
        dock: 'bottom',
        displayInfo: true,
    }],

    features: [{
        ftype: 'filters',
        encode: true,
        paramPrefix: "find",
        filters: [{
            type: 'string',
            dataIndex: 'name'
        },{
            type: 'string',
            dataIndex: 'description'
        },{
            type: 'string',
            dataIndex: 'value'
        },{
            type: 'date',
            dataIndex: 'last_update'
        }]
    }],
    
    initComponent: function() {
        Ext.apply(this, {
            columns: [{
                header: 'Name',
                dataIndex: 'name',
                width: 280,
                editor: {
                    allowBlank: false
                }
            },{
                header: 'Description',
                dataIndex: 'description',
                align: 'left',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            },{
                header: 'Value',
                dataIndex: 'value',
                align: 'center',
                editor: {
                    allowBlank: false
                }
            },{
                header: 'Last update',
                dataIndex: 'last_update',
                align: 'right',
                width: 150,
                renderer: Sales.Utilities.formatDate
            }],
        });

        this.callParent(arguments);
    }
});

