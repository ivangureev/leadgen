Ext.define('Sales.view.keySet.List' ,{
    extend: 'Ext.grid.Panel',
    alias : 'widget.keySetlist',
    title : 'Key sets',
    
    store: 'Keysets',
    disabled: true,
    
    selType: 'rowmodel',
    
    plugins: [{
        ptype: 'cellediting'
    }],
    
    dockedItems: [{
        xtype: 'toolbar',
        items: [{
            xtype: 'button',
            action: 'add',
            text: 'Create keyset',
            iconCls: 'icon-add',
        }, '-', {
            xtype: 'button',
            text: 'Remove keyset',
            iconCls: 'icon-delete',
            action: 'delete',
        }]
    },{
        xtype: 'pagingtoolbar',
        store: 'Keysets',
        dock: 'bottom',
        displayInfo: true,
    }],
    
    features: [{
        ftype: 'filters',
        encode: true,
        paramPrefix: "find",
        filters: [{
            type: 'string',
            dataIndex: 'name'
        },{
            type: 'numeric',
            dataIndex: 'price'
        }]
    }],

    initComponent: function() {
        this.columns = [
            {
                header: 'Name',
                dataIndex: 'name',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            },{
                header: 'Price',
                dataIndex: 'price',
                flex: 1,
                align: 'center',
                renderer: Sales.Utilities.formatRubles,
                editor: {
                    allowBlank: false
                }
            },{
                header: 'Count',
                dataIndex: 'count',
                align: 'center',
                flex: 1,
            }
        ];

        this.callParent(arguments);
    }
});

