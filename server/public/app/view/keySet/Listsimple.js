Ext.define('Sales.view.keySet.Listsimple' ,{
    extend: 'Ext.grid.Panel',
    alias : 'widget.keySetlistsimple',
    title : 'Key sets',
    
    store: 'Keysets',
    disabled: true,
    
    selType: 'rowmodel',
    
    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'Keysets',
        dock: 'bottom',
        displayInfo: true,
    }],
    
    features: [{
        ftype: 'filters',
        encode: true,
        paramPrefix: "find",
        filters: [{
            type: 'string',
            dataIndex: 'name'
        },{
            type: 'numeric',
            dataIndex: 'price'
        }]
    }],
    
    initComponent: function() {
        this.columns = [
            {
                header: 'Name',
                dataIndex: 'name',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            },{
                header: 'Price',
                dataIndex: 'price',
                flex: 1,
                align: 'center',
                renderer: Sales.Utilities.formatRubles,
                editor: {
                    allowBlank: false
                }
            },{
                header: 'Count',
                dataIndex: 'count',
                align: 'center',
                flex: 1,
            }
        ];

        this.callParent(arguments);
    }
});

