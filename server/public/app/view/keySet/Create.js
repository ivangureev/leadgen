Ext.define('Sales.view.keySet.Create', {
    extend: 'Ext.window.Window',
    alias : 'widget.createkeyset',

    title : 'Create Key set',
    layout: 'fit',
    autoShow: true,
    modal: true,

    initComponent: function() {
        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        this.items = [
            {
                xtype: 'form',
                bodyPadding: 10,
                items: [
                    {
                        xtype: 'textfield',
                        name : 'name',
                        fieldLabel: 'Name',
                        allowBlank: false,
                        afterLabelTextTpl: required,
                    },
                    {
                        xtype: 'numberfield',
                        name: 'price',
                        fieldLabel: 'Price',
                        allowDecimals: true,
                        decimalPrecision: 2,
                        minValue: 0,
                        hideTrigger: true,
                        allowBlank: false,
                        afterLabelTextTpl: required,
                    },
                ]
            }
        ];

        this.buttons = [
            {
                text: 'Save',
                action: 'save'
            },
            {
                text: 'Cancel',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    }
});