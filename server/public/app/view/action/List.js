Ext.define('Sales.view.action.List' ,{
    extend: 'Ext.Panel',
    alias : 'widget.actionlist',

    title : 'Actions',
    width: 150,
    split: true,
    collapsible: true,
    collapsed: true,
    floatable: false,

    layout:'accordion',

    items: [{
        title: 'Parse source',
        layout: {
            type: 'vbox',
            align : 'stretch',
            pack  : 'start',
        },
        items: [{
            xtype:'button',
            text: 'Start parsing',
            menu : {
                items: [{
                    iconCls: 'icon-2gis',
                    text: '2GIS',
                    action: 'parse_2gis',
                }]
            },
            minHeight: 48
        }]
    },{
        title: "Domain's info",
        layout: {
            type: 'vbox',
            align : 'stretch',
            pack  : 'start',
        },
        items: [{
            xtype:'button',
            text: 'Complite',
            action: 'complete_sites_info',
            minHeight: 48
        },{
            xtype:'button',
            text: 'Reset',
            action: 'set_sites_info',
            minHeight: 48
        },{
            xtype:'button',
            text: 'Update sites avaliability',
            action: 'update_sites_availability',
            minHeight: 48
        }]
    },{
        title: "SERP",
        layout: {
            type: 'vbox',
            align : 'stretch',
            pack  : 'start',
        },
        items: [{
            xtype:'button',
            text: 'Complite',
            menu : {
                items: [{
                    text: 'All',
                    action: 'complete_serps',
                },{
                    text: 'Yandex',
                    action: 'complete_serps_yandex',
                    iconCls: 'icon-yandex',
                }]
            },
            minHeight: 48
        },{
            xtype:'button',
            text: 'Reset',
            menu : {
                items: [{
                    text: 'All',
                    action: 'set_serps',
                },{
                    text: 'Yandex',
                    action: 'set_serps_yandex',
                    iconCls: 'icon-yandex',
                }]
            },
            minHeight: 48
        }]
    },{
        title: "Uploading",
        layout: {
            type: 'vbox',
            align : 'stretch',
            pack  : 'start',
        },
        items: [{
            xtype:'button',
            text: 'Send last update',
            action: 'send_last_update',
            minHeight: 48
        }]
    }]
});