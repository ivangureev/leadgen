Ext.define('Sales.view.category.Listsimple' ,{
    extend: 'Ext.grid.Panel',
    alias : 'widget.categorylistsimple',
    title : 'All Categories',
    
    store: 'Categories',
    
    selType: 'rowmodel',
    
    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'Categories',
        dock: 'bottom',
        displayInfo: true,
    }],
    
    features: [{
        ftype: 'filters',
        encode: true,
        paramPrefix: "find",
        filters: [{
            type: 'string',
            dataIndex: 'name'
        },{
            type: 'date',
            dataIndex: 'last_update'
        }]
    }],
    
    initComponent: function() {
        Ext.apply(this, {
            columns: [{
                header: 'Name',
                dataIndex: 'name',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }, {
                header: 'Last update',
                dataIndex: 'last_update',
                align: 'right',
                width: 150,
                renderer: Sales.Utilities.formatDate
            }],
        });

        this.callParent(arguments);
    }
});

