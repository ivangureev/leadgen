Ext.define('Sales.view.category.List' ,{
    extend: 'Ext.grid.Panel',
    require: ['Ext.ux.grid.FiltersFeature'],
    alias : 'widget.categorylist',
    title : 'All Categories',
    
    store: 'Categories',
    multiSelect: true,
    
    selType: 'rowmodel',

    plugins: [{
        ptype: 'cellediting'
    }],
    
    dockedItems: [{
        xtype: 'toolbar',
        items: [{
            xtype: 'button',
            action: 'add',
            text: 'Create new category',
            iconCls: 'icon-add',
        }, '-', {
            xtype: 'button',
            text: 'Remove category',
            iconCls: 'icon-delete',
            action: 'delete',
        }]
    },{
        xtype: 'pagingtoolbar',
        store: 'Categories',
        dock: 'bottom',
        displayInfo: true,
    }],

    features: [{
        ftype: 'filters',
        encode: true,
        paramPrefix: "find",
        filters: [{
            type: 'string',
            dataIndex: 'name'
        },{
            type: 'date',
            dataIndex: 'last_update'
        }]
    }],
    
    initComponent: function() {
        Ext.apply(this, {
            columns: [{
                header: 'Name',
                dataIndex: 'name',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }, {
                header: 'Last update',
                dataIndex: 'last_update',
                align: 'right',
                width: 150,
                renderer: Sales.Utilities.formatDate
            }],
        });

        this.callParent(arguments);
    }
});

