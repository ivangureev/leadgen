Ext.define('Sales.view.category.Create', {
    extend: 'Ext.window.Window',
    alias : 'widget.createcategory',

    title : 'Create category',
    layout: 'fit',
    autoShow: true,
    modal: true,

    initComponent: function() {
        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        this.items = [
            {
                xtype: 'form',
                bodyPadding: 10,
                items: [
                    {
                        xtype: 'textfield',
                        name : 'name',
                        fieldLabel: 'Name',
                        allowBlank: false,
                        afterLabelTextTpl: required,
                    },
                ]
            }
        ];

        this.buttons = [
            {
                text: 'Save',
                action: 'save'
            },
            {
                text: 'Cancel',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    }
});