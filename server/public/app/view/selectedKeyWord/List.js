Ext.define('Sales.view.selectedKeyWord.List' ,{
    extend: 'Ext.grid.Panel',
    alias : 'widget.selectedKeyWordlist',
    title : 'Selected Key Words',
    
    store: 'Selectedkeywords',
    disabled: true,
    
    selType: 'rowmodel',
    
    multiSelect: true,
    viewConfig: {
        // copy: true,
        plugins: {
            ptype: 'gridviewdragdrop',
            // dragGroup: 'keywords_secondGridDDGroup',
            dropGroup: 'keywords_firstGridDDGroup'
        }
    },
    
    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'Selectedkeywords',
        dock: 'bottom',
        displayInfo: true,
    }],
    
    features: [{
        ftype: 'filters',
        encode: true,
        paramPrefix: "find",
        filters: [{
            type: 'string',
            dataIndex: 'key'
        }]
    }],

    initComponent: function() {
        this.columns = [
            {header: 'Keyword',  dataIndex: 'key',  flex: 1},
        ];

        this.callParent(arguments);
    }
});