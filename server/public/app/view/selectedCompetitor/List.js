Ext.define('Sales.view.selectedCompetitor.List' ,{
    extend: 'Ext.grid.Panel',
    requires: ['Ext.ux.PreviewPlugin'],
    alias : 'widget.selectedCompetitorlist',
    title : 'Selected competitors',
    
    store: 'Selectedcompetitors',
    disabled: true,
    
    selType: 'rowmodel',

    multiSelect: true,

    lbar: [{
            xtype: 'button',
            action: 'copy',
            iconCls: 'icon-add',
            tooltip: 'Copy to all other keysets'
        }, '-', {
            xtype: 'button',
            tooltip: 'Remove competitor',
            iconCls: 'icon-delete',
            action: 'delete',
        }
    ],

    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'Selectedcompetitors',
        dock: 'bottom',
        displayInfo: true,
    }],

    features: [{
        ftype: 'filters',
        encode: true,
        paramPrefix: "find",
        filters: [{
            type: 'string',
            dataIndex: 'competitor_domain'
        },{
            type: 'numeric',
            dataIndex: 'percentage'
        },{
            type: 'date',
            dataIndex: 'last_update'
        }]
    }],
    
    initComponent: function() {
        Ext.apply(this, {
            viewConfig: {
                // copy: true,
                plugins: [{
                    pluginId: 'SelectedCompetitorsSnippet',
                    ptype: 'preview',
                    bodyField: 'competitor_snippet',
                    previewExpanded: true
                },{
                    ptype: 'gridviewdragdrop',
                    // dragGroup: 'competitor_secondGridDDGroup',
                    dropGroup: 'competitor_firstGridDDGroup'
                }]
            },

            columns: [{
                header: 'Domain',
                dataIndex: 'competitor_domain',
                flex: 1,
                renderer: Sales.Utilities.formatDomain
            },{
                header: 'Percentage',
                dataIndex: 'percentage',
                align: 'center',
                renderer: Sales.Utilities.formatPercentage
            },{
                header: 'Last update',
                dataIndex: 'last_update',
                align: 'right',
                width: 150,
                renderer: Sales.Utilities.formatDate
            }],
        });

        this.callParent(arguments);
    }
});

