Ext.define('Sales.view.selectExtCategory.List' ,{
    extend: 'Ext.grid.Panel',
    alias : 'widget.selectExtCat',
    title : 'Selected Ext Categories',
    
    selType: 'rowmodel',
    disabled: true,
    multiSelect: true,
    
    store: 'Selectextcategories',
    
    viewConfig: {
        copy: true,
        plugins: {
            ptype: 'gridviewdragdrop',
            dragGroup: 'secondGridDDGroup',
            dropGroup: 'firstGridDDGroup'
        }
    },
    
    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'Selectextcategories',
        dock: 'bottom',
        displayInfo: true,
    }],
    
    features: [{
        ftype: 'filters',
        encode: true,
        paramPrefix: "find",
        filters: [{
            type: 'string',
            dataIndex: 'name'
        },{
            type: 'string',
            dataIndex: 'link'
        },{
            type: 'date',
            dataIndex: 'last_update'
        }]
    }],

    initComponent: function() {
        Ext.apply(this, {
            columns: [{
                header: 'Name',
                dataIndex: 'name',
                flex: 1,
            },{
                header: 'Link',
                dataIndex: 'link',  
                flex: 1
            },{
                header: 'Source',  
                dataIndex: 'source_name',  
                flex: 1
            },{
                header: 'Last update',
                dataIndex: 'last_update',
                align: 'right',
                width: 150,
                renderer: Sales.Utilities.formatDate
            }],
        });

        this.callParent(arguments);
    }
});