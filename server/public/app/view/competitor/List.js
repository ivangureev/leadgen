Ext.define('Sales.view.competitor.List' ,{
    extend: 'Ext.grid.Panel',
    requires: ['Ext.ux.PreviewPlugin',
               'Sales.Utilities'],
    alias : 'widget.competitorlist',
    title : 'Competitors',
    
    store: 'Competitors',
    disabled: true,
    
    selType: 'rowmodel',

    multiSelect: true,
    
    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'Competitors',
        dock: 'bottom',
        displayInfo: true,
    }],
    
    features: [{
        ftype: 'filters',
        encode: true,
        paramPrefix: "find",
        filters: [{
            type: 'string',
            dataIndex: 'competitor_domain'
        },{
            type: 'numeric',
            dataIndex: 'percentage'
        },{
            type: 'date',
            dataIndex: 'last_update'
        }]
    }],
    
    initComponent: function() {
        Ext.apply(this, {
            viewConfig: {
                copy: true,
                plugins: [{
                    pluginId: 'CompetitorsSnippet',
                    ptype: 'preview',
                    bodyField: 'competitor_snippet',
                    previewExpanded: true
                },{
                    ptype: 'gridviewdragdrop',
                    dragGroup: 'competitor_firstGridDDGroup',
                    // dropGroup: 'competitor_secondGridDDGroup'
                }]
            },

            columns: [{
                header: 'Domain',
                dataIndex: 'competitor_domain',
                flex: 1,
                renderer: Sales.Utilities.formatDomain
            },{
                header: 'Percentage',
                dataIndex: 'percentage',
                align: 'center',
                renderer: Sales.Utilities.formatPercentage
            },{
                header: 'Last update',
                dataIndex: 'last_update',
                align: 'right',
                width: 150,
                renderer: Sales.Utilities.formatDate
            }],
        });

        this.callParent(arguments);
    }
});

