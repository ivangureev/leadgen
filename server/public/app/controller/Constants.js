Ext.define('Sales.controller.Constants', {
    extend: 'Ext.app.Controller',
    requires: ['Ext.window.MessageBox',
               'Sales.Utilities',
               'Ext.ux.grid.FiltersFeature'],
    
    id: 'Constants',
    
    stores: [
        'Constants',
    ],
    
    refs: [{
        ref: 'constantList',
        selector: '#constants_tab constantlist'
    }],
    
    models: [
        'Constant'
    ],

    views: [
        'constant.List',
    ],

    init: function() {
        this.control(
        {
            
        });
    },
});