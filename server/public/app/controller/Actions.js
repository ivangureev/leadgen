Ext.define('Sales.controller.Actions', {
    extend: 'Ext.app.Controller',
    requires: ['Ext.window.MessageBox',
               'Sales.Utilities'],
    
    id: 'Actions',

    views: [
        'action.List'
    ],

    init: function() {
        this.control({
            'menu [action=parse_2gis]': {
                click: this.parse_2gis
            },
            'button[action=complete_sites_info]': {
                click: this.complete_sites_info
            },
            'button[action=update_sites_availability]': {
                click: this.update_sites_availability
            },
            'button[action=set_sites_info]': {
                click: this.set_sites_info
            },
            'menu [action=complete_serps]': {
                click: this.complete_serps
            },
            'menu [action=complete_serps_yandex]': {
                click: this.complete_serps_yandex
            },
            'menu [action=set_serps]': {
                click: this.set_serps
            },
            'menu [action=set_serps_yandex]': {
                click: this.set_serps_yandex
            },
            'button[action=send_last_update]': {
                click: this.send_last_update
            },
        });
    },

    parse_2gis: function(button, noAsk) {
        var self = this;
        if (noAsk !== true) {
            Ext.MessageBox.confirm('Confirm', 'Start parsing?', function(button){ self.parse_2gis(button, noAsk=true) }); return;
        }
        
        if (button == 'no')
            return;

        this._sendAction('/catalogparser/api/parse_2gis/');
    },

    complete_sites_info: function(button, noAsk) {
        var self = this;
        if (noAsk !== true) {
            Ext.MessageBox.confirm('Confirm', 'Complite sites information?', function(button){ self.complete_sites_info(button, noAsk=true) }); return;
        }
        
        if (button == 'no')
            return;

        this._sendActionForDelay('/spider/api/complete_sites_info/');
    },

    update_sites_availability: function(button, noAsk) {
        var self = this;
        if (noAsk !== true) {
            Ext.MessageBox.confirm('Confirm', 'Update sites availability?', function(button){ self.update_sites_availability(button, noAsk=true) }); return;
        }
        
        if (button == 'no')
            return;

        this._sendActionForDelay('/spider/api/update_sites_availability/');
    },

    set_sites_info: function(button, noAsk) {
        var self = this;
        if (noAsk !== true) {
            Ext.MessageBox.confirm('Confirm', 'Reset all sites information?', function(button){ self.set_sites_info(button, noAsk=true) }); return;
        }
        
        if (button == 'no')
            return;

        this._sendActionForDelay('/spider/api/set_sites_info/');
    },

    complete_serps: function(button, noAsk) {
        var self = this;
        if (noAsk !== true) {
            Ext.MessageBox.confirm('Confirm', 'Complite SERP?', function(button){ self.complete_serps(button, noAsk=true) }); return;
        }
        
        if (button == 'no')
            return;

        this._sendActionForDelay('/spider/api/complete_serps/');
    },

    complete_serps_yandex: function(button, noAsk) {
        var self = this;
        if (noAsk !== true) {
            Ext.MessageBox.confirm('Confirm', 'Complite SERP Yandex?', function(button){ self.complete_serps_yandex(button, noAsk=true) }); return;
        }
        
        if (button == 'no')
            return;

        this._sendActionForDelay('/spider/api/complete_serps_yandex/');
    },

    set_serps: function(button, noAsk) {
        var self = this;
        if (noAsk !== true) {
            Ext.MessageBox.confirm('Confirm', 'Reset all SERP?', function(button){ self.set_serps(button, noAsk=true) }); return;
        }
        
        if (button == 'no')
            return;

        this._sendActionForDelay('/spider/api/set_serps/');
    },

    set_serps_yandex: function(button, noAsk) {
        var self = this;
        if (noAsk !== true) {
            Ext.MessageBox.confirm('Confirm', 'Reset all Yandex SERP?', function(button){ self.set_serps_yandex(button, noAsk=true) }); return;
        }
        
        if (button == 'no')
            return;

        this._sendActionForDelay('/spider/api/set_serps_yandex/');
    },

    send_last_update: function(button, noAsk) {
        var self = this;
        if (noAsk !== true) {
            Ext.MessageBox.confirm('Confirm', 'Send last update?', function(button){ self.send_last_update(button, noAsk=true) }); return;
        }
        
        if (button == 'no')
            return;

        this._sendActionForDelay('/uploading/api/send_to_queue/');
    },

    _sendAction: function(url) {
        Ext.MessageBox.show({
           msg: 'Please wait...',
           progressText: 'Request processing...',
           width:300,
           wait:true,
           waitConfig: {interval:500},
       });

        Ext.Ajax.request({
            url: url,
            method: 'POST',
            success: function(response){
                Ext.MessageBox.hide();
                Ext.MessageBox.alert('Status', 'The task is complete.');
            }
        });
    },

    _sendActionForDelay: function(url) {
        Ext.MessageBox.show({
           msg: 'Please wait...',
           progressText: 'Request processing...',
           width:300,
           wait:true,
           waitConfig: {interval:500},
       });

        Ext.Ajax.request({
            url: url,
            method: 'POST',
            success: function(response){
                Ext.MessageBox.hide();
                Ext.MessageBox.alert('Status', 'The task assigned to perform.');
            }
        });
    },
});