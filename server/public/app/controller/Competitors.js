Ext.define('Sales.controller.Competitors', {
    extend: 'Ext.app.Controller',
    requires: ['Ext.window.MessageBox',
               'Sales.Utilities',
               'Ext.ux.grid.FiltersFeature'],
    
    id: 'Competitors',
    
    stores: [
        'Categories',
        'Keysets',
        'Competitors',
        'Selectedcompetitors',
        'Keywordsexpanded'
    ],
    
    refs: [{
        ref: 'categoryList',
        selector: '#competitors_tab categorylistsimple'
    },{
        ref: 'keySetList',
        selector: '#competitors_tab keySetlistsimple'
    },{
        ref: 'competitorList',
        selector: '#competitors_tab competitorlist'
    },{
        ref: 'selectedCompetitorList',
        selector: '#competitors_tab selectedCompetitorlist'
    },{
        ref: 'keyWordPanel',
        selector: '#competitors_tab keyWordExpandedchartcontainer'
    }
    // ,{
    //     ref: 'keyWordExpandedList',
    //     selector: '#competitors_tab keyWordExpandedlist'
    // }
    ],
    
    models: [
        'Category',
        'Extcategory',
        'Keyset',
        'Competitor',
        'Keywordexpanded'
    ],

    views: [
        'category.List',
        'keySet.Listsimple',
        'competitor.List',
        'selectedCompetitor.List',
        'keyWordExpanded.List',
        'keyWordExpanded.Chartcontainer',
    ],

    init: function() {
        this.control({
            '#competitors_tab categorylistsimple': {
                itemclick: this.showKeySet,
            },
            '#competitors_tab keySetlistsimple': {
                itemclick: this.showAllCompetitors,
            },
            '#competitors_tab competitorlist': {
                itemclick: this.showKeyWords,
            },
            '#competitors_tab selectedCompetitorlist button[action=delete]': {
                click: this.deleteSelectedCompetitor
            },
            '#competitors_tab selectedCompetitorlist button[action=copy]': {
                click: this.copySelectedCompetitors
            },
            '#competitors_tab keyWordExpandedchartcontainer button[action=add]': {
                click: this.addSelectedCompetitor
            },
            '#competitors_tab selectedCompetitorlist dataview': {
                beforedrop: this.checkAlreadySet,
                drop: this.selectCompetitor
            },
            '#competitors_tab competitorlist dataview': {
               beforedrop: this.removeRelation
            }
        });
    },

    // handle checking if already exist this competitor
    checkAlreadySet: function(node, data, dropRec, dropPosition) {
        var selection = this.getKeySetList().selModel.getSelection();
        
        if (!selection.length) {
            Ext.MessageBox.alert('Status', 'Please, select a key set');
            return false;
        }
        
        var store = this.getSelectedcompetitorsStore();
        return Sales.Utilities.checkUniq(store, data.records, 'competitor_domain');
        // return true;
    },
    
    // handle drop event. Dropping from competitor to selected competitor     
    selectCompetitor: function(node, data, dropRec, dropPosition) {
        var selection = this.getKeySetList().selModel.getSelection();
        if (!selection.length)
            return

        keySetId = selection[0].data.id;
        this.createSelectedCompetitors(data.records, keySetId);
        
        store = this.getSelectedcompetitorsStore();
        Sales.Utilities.lazyStoreload(store);
    },
    
    // handle drop event. Dropping from selected keyword of selected category to key words
    removeRelation: function(node, data, dropRec, dropPosition) {
        var selection = this.getKeySetList().selModel.getSelection();
        if (!selection.length) {
            Ext.MessageBox.alert('Status', 'Please, select a key set'); return;
        }
        
        keySetId = selection[0].data.id;
        this.removeSelectedCompetitors(data.records, keySetId);
        
        Sales.Utilities.lazyStoreload(this.getSelectedcompetitorsStore());
        
        // not allow drag
        return false;
    },

    // handle drop event. Dropping from selected keyword of selected category to key words
    removeSelectedCompetitors: function(records, keySetId) {
        store = this.getSelectedcompetitorsStore();
        Ext.each(records, function(record, index){
            store.remove(record);
            store.commitChanges();
        });
    },

    // handle to delete
    createSelectedCompetitors: function(records, keySetId) {
        store = this.getSelectedcompetitorsStore();
        Ext.each(records, function(record, index){
            new_element = new Object();
            new_element.key_set = keySetId;
            new_element.competitor_domain = record.data.competitor_domain;
            new_element.competitor_title = record.data.competitor_title;
            new_element.competitor_snippet = record.data.competitor_snippet;
            new_element.percentage = record.data.percentage;

            store.add(new_element);
            store.commitChanges();
        });
    },

    addSelectedCompetitor: function(button) {
        var competitors = this.getCompetitorList().selModel.getSelection();
        if (!competitors.length || competitors.length > 1) {
            Ext.MessageBox.alert('Status', 'Please, select a single competitor'); return;
        }
        
        var keysets = this.getKeySetList().selModel.getSelection();
        if (!keysets.length || keysets.length > 1) {
            Ext.MessageBox.alert('Status', 'Please, select a single key set'); return;
        }
        var keysetId = keysets[0].data.id;

        store = this.getSelectedcompetitorsStore();
        if(Sales.Utilities.checkUniq(store, competitors, 'competitor_domain'))
        {
            this.createSelectedCompetitors(competitors, keysetId);
            Sales.Utilities.lazyStoreload(store);
        }
    },

    // handle to delete
    copySelectedCompetitors: function(button, noAsk) {
        var self = this;
        if (noAsk !== true) {
            Ext.MessageBox.confirm('Confirm', 'Copy all competitors to other key sets?', function(button){ self.copySelectedCompetitors(button, noAsk=true) }); return;
        }
        
        if (button == 'no')
            return;

        var keysets = this.getKeySetList().selModel.getSelection();
        if (!keysets.length || keysets.length > 1) {
            Ext.MessageBox.alert('Status', 'Please, select a single key set'); return;
        }
        var keysetId = keysets[0].data.id;

        Ext.Ajax.request({
            url: '/competitors/api/copy_competitors',
            params: {
                key_set_id: keysetId
            },
            success: function(response){
                // console.log('copy_competitors OK');
            }
        });
    },

    // handle to delete
    deleteSelectedCompetitor: function(button, noAsk) {
        var self = this;
        if (noAsk !== true) {
            Ext.MessageBox.confirm('Confirm', 'Delete the competitor?', function(button){ self.deleteSelectedCompetitor(button, noAsk=true) }); return;
        }
        
        if (button == 'no')
            return;

        var selectionParent = this.getKeySetList().selModel.getSelection();
        if (!selectionParent.length) {
            Ext.MessageBox.alert('Status', 'Please, select a key set'); return;
        }
        
        var selection = this.getSelectedCompetitorList().selModel.getSelection();
        if (!selection.length) {
            Ext.MessageBox.alert('Status', 'Please, select a competitor'); return;
        }

        // console.log(selection);

        keySetId = selectionParent[0].data.id;
        this.removeSelectedCompetitors(selection, keySetId);
        
        Sales.Utilities.lazyStoreload(this.getSelectedcompetitorsStore());
    },
    
    // handle show key set of just selected category
    showKeySet: function(grid, record) {
        this.getKeySetList().enable();
        this.getCompetitorList().disable();
        this.getSelectedCompetitorList().disable();
        // this.getKeyWordExpandedList().disable();
        this.getKeyWordPanel().disable();
        
        var keysetStore = this.getKeysetsStore();
        keysetStore.filter('category__id', record.data.id);
    },

    showAllCompetitors: function(grid, record) {
        this.getCompetitorList().enable();
        this.getSelectedCompetitorList().enable();
        // this.getKeyWordExpandedList().disable();
        this.getKeyWordPanel().disable();
        
        var competitorsStore = this.getCompetitorsStore();
        competitorsStore.filter('key_set__id', record.data.id);

        var selectedcompetitorsStore = this.getSelectedcompetitorsStore();
        selectedcompetitorsStore.filter('key_set__id', record.data.id);
    },

    showKeyWords: function(grid, record) {
        // this.getKeyWordExpandedList().enable();
        this.getKeyWordPanel().enable();

        var keywordsexpandedStore = this.getKeywordsexpandedStore();
        keywordsexpandedStore.filter('id', record.data.id);
    },
});