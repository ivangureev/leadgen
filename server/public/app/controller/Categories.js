Ext.define('Sales.controller.Categories', {
    extend: 'Ext.app.Controller',
    requires: ['Ext.window.MessageBox',
               'Sales.Utilities',
               'Ext.ux.grid.FiltersFeature'],
    
    id: 'Categories',
    
    stores: [
        'Categories',
        'Freeextcategories',
        'Selectextcategories'
    ],
    
    refs: [{
        ref: 'categoryList',
        selector: '#categories_tab categorylist'
    },{
        ref: 'selectExtCatList',
        selector: '#categories_tab selectExtCat'
    },{
        ref: 'freetExtCatList',
        selector: '#categories_tab freeExtCat'
    }],
    
    models: [
        'Category',
        'Extcategory',
    ],

    views: [
        'category.List',
        'freeExtCategory.List',
        'selectExtCategory.List',
        'category.Create',
        'action.List'
    ],

    init: function() {
        this.control({
            '#categories_tab categorylist': {
                itemclick: this.showSelectExtCat,
            },
            '#categories_tab categorylist button[action=add]': {
                click: this.showCategoryForm
            },
            '#categories_tab categorylist button[action=delete]': {
                click: this.deleteCategory
            },
            'createcategory button[action=save]': {
                click: this.createCategory
            },
            '#categories_tab selectExtCat dataview': {
                beforedrop: this.checkCategory,
                drop: this.selectExtCat
            },
            '#categories_tab freeExtCat dataview': {
                beforedrop: this.checkCategory,
                drop: this.freeExtCat
            },
            '#categories_tab freeExtCat': {
                beforeselect: this.checkCategory
            },
        });
    },
    
    // handle checking if any category is selected  
    checkCategory: function() {
        var selection = this.getCategoryList().selModel.getSelection();
        
        if (!selection.length || selection.length > 1)
            return false;
        
        return true;
    },
    
    // handle checking selection
    checkSelection: function(grid) {
        var selection = this.getCategoryList().selModel.getSelection();
        if(selection.length == 1){        
            this.getSelectExtCatList().enable();
            this.getFreetExtCatList().enable();
        }
        else {
            this.getSelectExtCatList().disable();
            this.getFreetExtCatList().disable();
        }
    },
    
    // handle drop event. Dropping from free extCat to selected extCat of selected category     
    selectExtCat: function(node, data, dropRec, dropPosition) {
        var selection = this.getCategoryList().selModel.getSelection();
        if (!selection.length)
            return
        
        Ext.each(data.records, function(record, index){
            categoryId = selection[0].data.id;
            Sales.Utilities.addM2M(record, 'categories', categoryId);
        });

        this.getFreeextcategoriesStore().load();
    },
    
    // handle drop event. Dropping from selected extCat of selected category to free extCat
    freeExtCat: function(node, data, dropRec, dropPosition) {
        var selection = this.getCategoryList().selModel.getSelection();
        if (!selection.length)
            return
        
        Ext.each(data.records, function(record, index){
            categoryId = selection[0].data.id;
            Sales.Utilities.removeM2M(record, 'categories', categoryId);
        });
        
        this.getSelectextcategoriesStore().load();
    },
    
    // handle show selected extCat of just selected category
    showSelectExtCat: function(grid, record) {
        this.checkSelection(grid);

        var selectExtStore = this.getSelectextcategoriesStore();
        selectExtStore.filter('categories__id', record.data.id);
    },
    
    // handle to show form of create new category
    showCategoryForm: function(button) {
        Ext.widget('createcategory').down('form');
    },
    
    // handle to delete new category
    deleteCategory: function(button, noAsk) {
        var self = this;
        if (noAsk !== true) {
            Ext.MessageBox.confirm('Confirm', 'Delete the category?', function(button){ self.deleteCategory(button, noAsk=true) }); return;
        }
        
        if (button == 'no')
            return;
        
        var selection = this.getCategoryList().selModel.getSelection();
        if (!selection.length) {
            Ext.MessageBox.alert('Status', 'Please, select a category'); return;
        }
        
        this.getSelectextcategoriesStore().removeAll();
        
        for (index in selection)
        {
            this.getCategoriesStore().remove(selection[index]);
        }
    },
    
    // handle to add new category
    createCategory: function(button) {
        var win    = button.up('window'),
            form   = win.down('form'),
            values = form.getValues(),
            store  = this.getCategoriesStore();
            
        if(!form.getForm().isValid())
            return;
        
        store.add(values);
        store.load();
        
        win.close();
    },
});