Ext.define('Sales.controller.Uploads', {
    extend: 'Ext.app.Controller',
    requires: ['Ext.window.MessageBox',
               'Sales.Utilities',
               'Ext.ux.grid.FiltersFeature'],
    
    id: 'Uploads',
    
    stores: [
        'Categories',
        'Uploads',
    ],
    
    refs: [{
        ref: 'categoryList',
        selector: '#uploading_tab categorylistsimple'
    },{
        ref: 'uploadList',
        selector: '#uploading_tab uploadlist'
    }],
    
    models: [
        'Category',
        'Upload',
    ],

    views: [
        'category.Listsimple',
        'upload.List',
    ],

    init: function() {
        this.control({
            '#uploading_tab categorylistsimple': {
                itemclick: this.showUpload,
            }, 
            '#uploading_tab uploadlist button[action=make-upload]': {
                click: this.makeUpload
            },
        });
    },

    // handle show uploads
    showUpload: function(grid, record) {
        this.getUploadList().enable();
        
        var uploadStore = this.getUploadsStore();
        uploadStore.filter('category__id', record.data.id);
    },

    // handle to make upload
    makeUpload: function(button, noAsk) {
        var self = this;
        if (noAsk !== true) {
            Ext.MessageBox.confirm('Confirm', 'Make uploads for selected category?', function(button){ self.makeUpload(button, noAsk=true) }); return;
        }
        
        if (button == 'no')
            return;

        var categories = this.getCategoryList().selModel.getSelection();
        if (!categories.length || categories.length > 1) {
            Ext.MessageBox.alert('Status', 'Please, select a single category'); return;
        }
        var categoryId = categories[0].data.id;

        Ext.MessageBox.show({
           msg: 'Please wait...',
           progressText: 'Request processing...',
           width:300,
           wait:true,
           waitConfig: {interval:500},
       });

        Ext.Ajax.request({
            url: '/uploading/api/upload',
            method: 'POST',
            params: {
                category_id: categoryId
            },
            success: function(response){
                Ext.MessageBox.hide();
                Ext.MessageBox.alert('Status', 'The task assigned to perform.');
            }
        });
    },
});