Ext.define('Sales.controller.Keywords', {
    extend: 'Ext.app.Controller',
    requires: ['Ext.window.MessageBox',
               'Sales.Utilities',
               'Ext.ux.grid.FiltersFeature'],
    
    id: 'Keywords',
    
    stores: [
        'Categories',
        'Keysets',
        'Keywords',
        'Selectedkeywords'
    ],
    
    refs: [{
        ref: 'categoryList',
        selector: '#keywords_tab categorylistsimple'
    },{
        ref: 'keySetList',
        selector: '#keywords_tab keySetlist'
    },{
        ref: 'selectedKeyWordList',
        selector: '#keywords_tab selectedKeyWordlist'
    },{
        ref: 'keyWordList',
        selector: '#keywords_tab keyWordlist'
    }],
    
    models: [
        'Category',
        'Keyset',
        'Keyword',
    ],

    views: [
        'category.Listsimple',
        'keySet.List',
        'keySet.Create',
        'keyWord.List',
        'keyWord.Create',
        'selectedKeyWord.List'
    ],

    init: function() {
        this.control({
            '#keywords_tab categorylistsimple': {
                itemclick: this.showKeySet,
            },
            '#keywords_tab keySetlist': {
                itemclick: this.showSelKeyWord,
            },
            '#keywords_tab keySetlist button[action=add]': {
                click: this.showKeysetForm
            },
            '#keywords_tab keySetlist button[action=delete]': {
                click: this.deleteKeyset
            },
            'createkeyset button[action=save]': {
                click: this.createKeyset
            },
            '#keywords_tab keyWordlist button[action=add]': {
                click: this.showKeywordForm
            },
            '#keywords_tab keyWordlist button[action=delete]': {
                click: this.deleteKeyword
            },
            'createkeyword button[action=save]': {
                click: this.createKeyword
            },
            '#keywords_tab selectedKeyWordlist dataview': {
                beforedrop: this.checkAlreadySet,
                drop: this.selectKeyWord
            },
            '#keywords_tab keyWordlist dataview': {
               beforedrop: this.removeRelation
            }
        });
    },
    
    // handle checking if already exist this key word in key set
    checkAlreadySet: function(node, data, dropRec, dropPosition) {
        var selection = this.getKeySetList().selModel.getSelection();
        
        if (!selection.length) {
            Ext.MessageBox.alert('Status', 'Please, select a key set');
            return false;
        }
        
        var store = this.getSelectedkeywordsStore();
        return Sales.Utilities.checkUniq(store, data.records, 'id');
    },
    
    // handle drop event. Dropping from free key words to selected key words of selected category     
    selectKeyWord: function(node, data, dropRec, dropPosition) {
        var selection = this.getKeySetList().selModel.getSelection();
        if (!selection.length)
            return
        
        keySetId = selection[0].data.id;
        Ext.each(data.records, function(record, index){
            Sales.Utilities.addM2M(record, 'keysets', keySetId);
        });
        
        Sales.Utilities.lazyStoreload(this.getKeywordsStore());
        // иначе сбивается selection и надо выбирать его снова - неудобно очень
        //this.getKeysetsStore().load();
    },
    
    // handle drop event. Dropping from selected keyword of selected category to key words
    removeRelation: function(node, data, dropRec, dropPosition) {
        var selection = this.getKeySetList().selModel.getSelection();
        if (!selection.length) {
            Ext.MessageBox.alert('Status', 'Please, select a key set'); return;
        }
        
        Ext.each(data.records, function(record, index){
            keySetId = selection[0].data.id;
            Sales.Utilities.removeM2M(record, 'keysets', keySetId);
        });
        
        // this.getKeywordsStore().load();
        // this.getSelectedkeywordsStore().load();

        Sales.Utilities.lazyStoreload(this.getKeywordsStore());
        Sales.Utilities.lazyStoreload(this.getSelectedkeywordsStore());
        
        // not allow drag
        return false;
    },
    
    // handle show key set of just selected category
    showKeySet: function(grid, record) {
        this.getKeySetList().enable();
        this.getKeyWordList().disable();
        this.getSelectedKeyWordList().disable();
        
        var keysetStore = this.getKeysetsStore();
        keysetStore.filter('category__id', record.data.id);
        
        var keywordsStore = this.getKeywordsStore();
        keywordsStore.filter('category__id', record.data.id);
    },
    
    // handle show selected key words
    showSelKeyWord: function(grid, record) {
        this.getKeyWordList().enable();
        this.getSelectedKeyWordList().enable();
        
        var selectedkeywordsStore = this.getSelectedkeywordsStore();
        selectedkeywordsStore.filter('keysets__id', record.data.id);
    },
    
    // handle to show form of create new key set
    showKeysetForm: function(button) {
        var selection = this.getCategoryList().selModel.getSelection();
        if (!selection.length) {
            Ext.MessageBox.alert('Status', 'Please, select a category'); return;
        }
        
        Ext.widget('createkeyset').down('form');
    },
    
    // handle to delete
    deleteKeyset: function(button, noAsk) {
        var self = this;
        if (noAsk !== true) {
            Ext.MessageBox.confirm('Confirm', 'Delete the key set?', function(button){ self.deleteKeyset(button, noAsk=true) }); return;
        }
        
        if (button == 'no')
            return;
        
        var selection = this.getKeySetList().selModel.getSelection();
        if (!selection.length) {
            Ext.MessageBox.alert('Status', 'Please, select a key set'); return;
        }
        
        this.getSelectedkeywordsStore().removeAll();
        this.getKeywordsStore().load();
        
        this.getKeysetsStore().remove(selection[0]);
    },
    
    // handle to add new key set
    createKeyset: function(button) {
        var win    = button.up('window'),
            form   = win.down('form'),
            values = form.getValues(),
            store  = this.getKeysetsStore();
            
        if(!form.getForm().isValid())
            return;
        
        var selection = this.getCategoryList().selModel.getSelection();
        if (!selection.length) {
            Ext.MessageBox.alert('Status', 'Please, select a category'); return;
        }
        values.category = selection[0].data.id;
        
        store.add(values);
        store.load();
        
        win.close();
    },
    
    // handle to show form of create new key words
    showKeywordForm: function(button) {
        Ext.widget('createkeyword').down('form');
    },
    
    // handle to delete key words
    deleteKeyword: function(button, noAsk) {
        var self = this;
        if (noAsk !== true) {
            Ext.MessageBox.confirm('Confirm', 'Delete the key word?', function(button){ self.deleteKeyword(button, noAsk=true) }); return;
        }
        
        if (button == 'no')
            return;
        
        var selection = this.getKeyWordList().selModel.getSelection();
        if (!selection.length) {
            Ext.MessageBox.alert('Status', 'Please, select a key word'); return;
        }
        
        for (index in selection)
        {
            this.getKeywordsStore().remove(selection[index]);
        }
        
        //this.getKeysetsStore().load();
        this.getSelectedkeywordsStore().load();
    },
    
    // handle to add new key words
    createKeyword: function(button) {
        var win    = button.up('window'),
            form   = win.down('form'),
            values = form.getValues(),
            store  = this.getKeywordsStore();
            
        if(!form.getForm().isValid())
            return;
        
        var selection = this.getCategoryList().selModel.getSelection();
        if (!selection.length) {
            Ext.MessageBox.alert('Status', 'Please, select a category'); return;
        }
        var category = selection[0].data.id;
        
        var lines = values.keywords.split(/\r\n|\r|\n/);
        Ext.each(lines, function(value, index){
            store.add({key: value, category: category});
            store.commitChanges();
        });

        store.load();
        
        win.close();
    },
});