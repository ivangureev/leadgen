Ext.define('Sales.store.Competitors', {
    extend: 'Ext.data.Store',
    model: 'Sales.model.Competitor',
    pageSize: 10,
    autoSync:true,
    autoLoad: false,
    remoteSort: true,
    remoteFilter: true,

    proxy: {
        type: 'rest',
        url: '/competitors/competitors/',
        reader: {
            type: 'json',
            root: 'results',
            totalProperty: 'totalCount',
            successProperty: 'success'
        },
        writer: {
            type: 'json'
        }
    },
});