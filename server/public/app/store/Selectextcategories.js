Ext.define('Sales.store.Selectextcategories', {
    extend: 'Ext.data.Store',
    model: 'Sales.model.Extcategory',
    pageSize: 10,
    autoSync:true,
    //autoLoad: true,
    remoteSort: true,
    remoteFilter: true,

    proxy: {
        type: 'rest',
        url: '/categories/selected_ext_categories/',
        reader: {
            type: 'json',
            root: 'results',
            totalProperty: 'totalCount',
            successProperty: 'success'
        },
        writer: {
            type: 'json'
        }
    },
});