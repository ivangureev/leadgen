Ext.define('Sales.store.Selectedcompetitors', {
    extend: 'Ext.data.Store',
    model: 'Sales.model.Competitor',
    pageSize: 10,
    autoSync:true,
    autoLoad: false,
    remoteSort: true,
    remoteFilter: true,

    proxy: {
        type: 'rest',
        url: '/competitors/selected_competitors/',
        reader: {
            type: 'json',
            root: 'results',
            totalProperty: 'totalCount',
            successProperty: 'success'
        },
        writer: {
            type: 'json'
        }
    },
});