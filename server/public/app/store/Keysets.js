Ext.define('Sales.store.Keysets', {
    extend: 'Ext.data.Store',
    model: 'Sales.model.Keyset',
    pageSize: 10,
    autoSync:true,
    //autoLoad: true,
    remoteSort: true,
    remoteFilter: true,

    proxy: {
        type: 'rest',
        url: '/keywords/keysets/',
        reader: {
            type: 'json',
            root: 'results',
            totalProperty: 'totalCount',
            successProperty: 'success'
        },
        writer: {
            type: 'json'
        }
    },
});