Ext.define('Sales.store.Constants', {
    extend: 'Ext.data.Store',
    model: 'Sales.model.Constant',
    pageSize: 20,
    autoLoad: true,
    autoSync:true,
    remoteSort: true,
    remoteFilter: true,
    
    proxy: {
        type: 'rest',
        url: '/constants/constants/',
        reader: {
            type: 'json',
            root: 'results',
            totalProperty: 'totalCount',
            successProperty: 'success'
        },
        writer: {
            type: 'json'
        }
    },
});