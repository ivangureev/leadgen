Ext.define('Sales.store.Keywordsexpanded', {
    extend: 'Ext.data.Store',
    model: 'Sales.model.Keywordexpanded',
    pageSize: 10000,
    autoSync:true,
    autoLoad: false, // set FALSE
    remoteSort: true,
    remoteFilter: true,

    proxy: {
        type: 'rest',
        url: '/competitors/keywords/',
        reader: {
            type: 'json',
            root: 'results',
            totalProperty: 'totalCount',
            successProperty: 'success'
        },
        writer: {
            type: 'json'
        }
    },
});