Ext.define('Sales.store.Freeextcategories', {
    extend: 'Ext.data.Store',
    model: 'Sales.model.Extcategory',
    pageSize: 10,
    autoLoad: true,
    autoSync:true,
    remoteSort: true,
    remoteFilter: true,

    proxy: {
        type: 'rest',
        url: '/categories/ext_categories/',
        reader: {
            type: 'json',
            root: 'results',
            totalProperty: 'totalCount',
            successProperty: 'success'
        },
        writer: {
            type: 'json'
        }
    },
    
    filters: [{
        //property: 'categories__id',
        //value: null
        property: 'categories__isnull',
        value: true
    }]
});