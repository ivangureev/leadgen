Ext.define('Sales.store.Uploads', {
    extend: 'Ext.data.Store',
    model: 'Sales.model.Upload',
    pageSize: 10,
    autoSync:true,
    autoLoad: false,
    remoteSort: true,
    remoteFilter: true,

    proxy: {
        type: 'rest',
        url: '/uploading/uploads/',
        reader: {
            type: 'json',
            root: 'results',
            totalProperty: 'totalCount',
            successProperty: 'success'
        },
        writer: {
            type: 'json'
        }
    },
});