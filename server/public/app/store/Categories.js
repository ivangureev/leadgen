Ext.define('Sales.store.Categories', {
    extend: 'Ext.data.Store',
    model: 'Sales.model.Category',
    pageSize: 10,
    autoLoad: true,
    autoSync:true,
    remoteSort: true,
    remoteFilter: true,
    
    proxy: {
        type: 'rest',
        url: '/categories/categories/',
        reader: {
            type: 'json',
            root: 'results',
            totalProperty: 'totalCount',
            successProperty: 'success'
        },
        writer: {
            type: 'json'
        }
    },
    
    //listeners: {
    //    beforerequest: function  CSRFToken(conn, options) {
    //        alert("vdfvdf");
    //        if (!(/^http:.*/.test(options.url) || /^https:.*/.test(options.url))) {
    //            if (typeof(options.headers) == "undefined") {
    //                options.headers = {'X-CSRFToken': Ext.util.Cookies.get('csrftoken')};
    //            } else {
    //                options.headers.extend({'X-CSRFToken': Ext.util.Cookies.get('csrftoken')});
    //            }                        
    //        }
    //    }
    //},
});