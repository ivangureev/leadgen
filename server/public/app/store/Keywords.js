Ext.define('Sales.store.Keywords', {
    extend: 'Ext.data.Store',
    model: 'Sales.model.Keyword',
    pageSize: 10,
    autoSync:true,
    //autoLoad: true,
    remoteSort: true,
    remoteFilter: true,

    proxy: {
        type: 'rest',
        url: '/keywords/keywords/',
        reader: {
            type: 'json',
            root: 'results',
            totalProperty: 'totalCount',
            successProperty: 'success'
        },
        writer: {
            type: 'json'
        }
    },
});