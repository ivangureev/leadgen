Ext.define('Sales.store.Selectedkeywords', {
    extend: 'Ext.data.Store',
    model: 'Sales.model.Keyword',
    pageSize: 10,
    autoSync:true,
    autoLoad: false,
    remoteSort: true,
    remoteFilter: true,

    proxy: {
        type: 'rest',
        url: '/keywords/selected_keywords/',
        reader: {
            type: 'json',
            root: 'results',
            totalProperty: 'totalCount',
            successProperty: 'success'
        },
        writer: {
            type: 'json'
        }
    },
});