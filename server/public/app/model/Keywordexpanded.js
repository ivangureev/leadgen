Ext.define('Sales.model.Keywordexpanded', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'Competitor',  type: 'int'},
        {name: 'key',  type: 'string'},
        {name: 'position_yandex',  type: 'int'},
        {name: 'related_position_yandex',  type: 'int'},
    ],
    
    belongsTo: 'Competitor'
});