Ext.define('Sales.model.Constant', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'name',  type: 'string'},
        {name: 'code',  type: 'string'},
        {name: 'description',  type: 'string'},
        {name: 'value',  type: 'string'},
        {name: 'last_update',  type: 'date', dateFormat: 'timestamp'},
    ],
});