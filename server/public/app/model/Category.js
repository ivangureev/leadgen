Ext.define('Sales.model.Category', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'name',  type: 'string'},
        {name: 'last_update',  type: 'date', dateFormat: 'timestamp'},
    ],
    
    hasMany  : {model: 'Extcategory', name: 'ext_categories'}
});