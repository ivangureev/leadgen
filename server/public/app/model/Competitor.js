Ext.define('Sales.model.Competitor', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'key_set',  type: 'int'},
        {name: 'competitor_domain',  type: 'string'},
        {name: 'competitor_title',  type: 'string'},
        {name: 'competitor_snippet',  type: 'string'},
        {name: 'percentage',  type: 'float'},
        {name: 'last_update',  type: 'date', dateFormat: 'timestamp'},
    ],
    
    hasMany  : {model: 'Keywordexpanded', name: 'keywords'},
    belongsTo: 'Keyset'
});