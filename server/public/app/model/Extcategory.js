Ext.define('Sales.model.Extcategory', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'categories',  type: 'string'},
        {name: 'name',  type: 'string'},
        {name: 'link',  type: 'string'},
        {name: 'source_name',  type: 'string'},
        {name: 'last_update',  type: 'date', dateFormat: 'timestamp'},
    ],
    
    belongsTo: 'Category'
});