Ext.define('Sales.model.Keyset', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'category',  type: 'int'},
        {name: 'name',  type: 'string'},
        {name: 'price',  type: 'float'},
        {name: 'count',  type: 'int'},
    ],
    
    hasMany  : {model: 'Keyword', name: 'keywords'},
    belongsTo: 'Category'
});