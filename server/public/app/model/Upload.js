Ext.define('Sales.model.Upload', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'unit_name',  type: 'string'},
        {name: 'domains',  type: 'string'},
        {name: 'step',  type: 'int'},
        {name: 'last_update',  type: 'date', dateFormat: 'timestamp'},
    ],
});