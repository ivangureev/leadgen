Ext.define('Sales.model.Keyword', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'keysets',  type: 'string'},
        {name: 'category',  type: 'int'},
        {name: 'key',  type: 'string'},
        {name: 'count',  type: 'int'},
    ],
    
    belongsTo: 'Keyset'
});