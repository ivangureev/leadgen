Ext.application({
    name: 'Sales',

    appFolder: 'public/app',

    launch: function() {
        var self = this;
        Ext.create('Ext.container.Viewport', {
            layout: 'border',
            items: [{
                region: 'west',
                xtype: 'actionlist'
            },{
                region: 'center',
                xtype: 'tabpanel',
                activeTab: 3,
                items: [{
                    title: 'Categories connection',
                    id: 'categories_tab',
                    layout: {
                        type: 'vbox',
                        align : 'stretch',
                    },
                    items: [{
                        xtype: 'categorylist',
                        flex:1,
                        height: '50%'
                    },{
                        title: 'Categories relations',
                        flex:1,
                        height: '50%',
                        layout: {
                            type: 'border',
                        },
                        items: [{
                            xtype: 'selectExtCat',
                            region: 'west',
                            collapsible: false,
                            flex: 1
                        },{
                            xtype: 'freeExtCat',
                            region: 'center',
                            collapsible: false,
                            flex: 1
                        }]
                    }]
                },{
                    title: 'Keywords',  
                    id: 'keywords_tab',
                    layout: {
                        type: 'vbox',
                        align : 'stretch',
                    },
                    items: [{
                        xtype: 'categorylistsimple',
                        flex:1,
                        height: '50%'
                    },{
                        title: 'Key words',
                        flex:1,
                        height: '50%',
                        layout: {
                            type: 'hbox',
                            pack: 'start',
                            align: 'stretch'
                        },
                        items: [{
                            xtype: 'keySetlist',
                            width: '20%',
                            flex: 1
                        },{
                            xtype: 'selectedKeyWordlist',
                            width: '35%',
                            flex: 2
                        },{
                            xtype: 'keyWordlist',
                            width: '45%',
                            flex: 3
                        }]
                    }]
                },{
                    title: 'Competitors',  
                    id: 'competitors_tab',
                    layout: {
                        type: 'vbox',
                        align : 'stretch',
                    },
                    items: [{
                        flex:1,
                        height: '20%',
                        layout: {
                            type: 'hbox',
                            pack: 'start',
                            align: 'stretch'
                        },
                        items: [{
                            xtype: 'categorylistsimple',
                            width: '50%',
                            flex: 1
                        },{
                            xtype: 'keySetlistsimple',
                            width: '50%',
                            flex: 1
                        }]
                    },{
                        flex:2,
                        height: '40%',
                        layout: {
                            type: 'hbox',
                            pack: 'start',
                            align: 'stretch'
                        },
                        items: [{
                            xtype: 'selectedCompetitorlist',
                            width: '50%',
                            flex: 1
                        },{
                            xtype: 'competitorlist',
                            width: '50%',
                            flex: 1
                        }]
                    },{
                        xtype: 'keyWordExpandedchartcontainer',
                        height: '40%',
                        flex: 2
                    }]
                },{
                    title: 'Uploading',  
                    id: 'uploading_tab',
                    layout: {
                        type: 'vbox',
                        align : 'stretch',
                    },
                    items: [{
                        xtype: 'categorylistsimple',
                        flex:1,
                        height: '35%'
                    },{
                        xtype: 'uploadlist',
                        flex:2,
                        height: '65%'
                    }]
                },{
                    title: 'Constants',  
                    id: 'constants_tab',
                    layout: {
                        type: 'vbox',
                        align : 'stretch',
                    },
                    items: [{
                        xtype: 'constantlist',
                        flex:1,
                        height: '100%'
                    }]
                }]
            }]
        });
        
        Ext.Ajax.addListener("requestcomplete", function (conn, options){
            response = Ext.decode(options.responseText)
            if(response.success != true)
            {
                var exeption = "Error";
                if(response.exception)
                    exeption = response.exception;
                    
                Ext.MessageBox.alert('Response error', exeption);
                
                if(response.traceback)
                    console.log(response.traceback);
            }
        }, this);
        
        Ext.Ajax.addListener("requestexception", function (conn, options){
            Ext.MessageBox.alert('Server error', 'Check your connection and tell to your developer.');
        }, this);
    },
    
    controllers: [
        'Categories',
        'Keywords',
        'Competitors',
        'Actions',
        'Uploads',
        'Constants'
    ],
});