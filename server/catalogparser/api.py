# -*- coding: utf-8 -*-
import os

from django.conf import settings

from tools.baseapi import BaseApi
from doubleqis import Parse2GisCsv
from utilites import DbUtilites
from constants.models import Constant

class Api(BaseApi):
    """
    """
    def __init__(self, *args, **kargs):
        super(Api, self).__init__(*args, **kargs)
  
    def parse_2gis(self):
        """
        Parse 2Gis csv file
        """
        response = dict()

        curpath = os.path.dirname(__file__)
        file_relative_path = Constant.objects.get_value('PATH_2GIS_CSV_FILE') 
        file_path = "{0}/..{1}".format(curpath, file_relative_path)

        if not file_path:
            raise Exception("File path undefined")

        util = DbUtilites()

        #DbUtilites.clear_db()
        
        parser = Parse2GisCsv('2Gis', file_path)
        parser.parse()
        
        # Downloading categories comm
        # ???
        # util.parse_csv_categories('/home/www-dev/django.dev.webpp.ru/newproject/csv/categories_comm_small2.csv')
        
        # Move just parsed new ext units to units
        parser.export_ext_units()

        return response
