# -*- coding: utf-8 -*-

import csv

from django.core.exceptions import ObjectDoesNotExist

from models import *


class DbUtilites():
    def __init__(self):
        pass
    
    def parse_csv_categories(self, file_path, **kwargs):
        """
        Reset ext categories and categories connections using
        names of categories, not IDs.
        """
        with open(file_path, 'rb') as f:
            reader = csv.reader(f, delimiter=',', quotechar='"')

            # for each line in csv file
            for row in reader:
                self._put_category(row)
    
    @staticmethod            
    def clear_db(**kwargs):
        """
        Clear database tables for testing application
        """
        ExtCategory.objects.all().delete()
        ExtUnit.objects.all().delete()
        ExtUnitProperty.objects.all().delete()
        Stage.objects.all().delete()
        Unit.objects.all().delete()
        UnitProperty.objects.all().delete()
    
    @staticmethod
    def set_stage(stypeVal, stageVal, sessionVal, **kwargs):
        """
        Set stage.
        """
        stageEntry, created = Stage.objects.get_or_create(
            stype=stypeVal, session=sessionVal, defaults={'stage': stageVal})
        
        if not created:
            stageEntry.stage = stageVal
            stageEntry.save()
            
    @staticmethod
    def get_stage(stypeVal, sessionVal, **kwargs):
        """
        Get stage.
        """       
        try:
            stageEntry = Stage.objects.get(stype=stypeVal, session=sessionVal)
        except ObjectDoesNotExist:
            return None
        
        return stageEntry.stage
                
    def _put_category(self, info, **kwargs):
        """
        Create category if need and connect it with already
        existing ext categories.
        """
        # Checking fields
        if not info[0] or not info[1]:
            raise Exception("Empty necessary categories connections fields in csv file: {0}".format(info))
            
        category_name = info[0]
        ext_categories = [ext_cat.strip() for ext_cat in info[1].decode('utf-8').split(';')]
        
        # Create or get category
        category = self._set_category(category_name)
        
        # Set connection to category
        self._set_category_connections(category, ext_categories)
    
    def _set_category(self, category_name, **kwargs):
        """
        Create or get category.
        """
        category, created = Category.objects.get_or_create(
            name=category_name)
        
        return category
            
    def _set_category_connections(self, category, ext_categories, **kwargs):
        """
        Set connection to category.
        """
        for ext_cat in ext_categories:
            ext_cat_name, source_name = ext_cat.split(':')
            
            try:
                sourceEntry = Source.objects.get(name=source_name)
                ext_category = ExtCategory.objects.get(
                    name=ext_cat_name, source=sourceEntry)
                
            except ObjectDoesNotExist:
                continue
            
            category.ext_categories.add(ext_category)
            category.save()
            