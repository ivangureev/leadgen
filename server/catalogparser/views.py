# -*- coding: utf-8 -*-

from api import Api
import tasks
from tools.response import Response

@Response.api
def api(request, action): 
    api = Api(request)
    if action == 'parse_2gis':
        # for asynchronous call (task)
        tasks.parse_2gis.apply_async()
        return dict()