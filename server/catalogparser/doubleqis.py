# -*- coding: utf-8 -*-

import sys
import csv
import re
import uuid

from django.utils.encoding import smart_str, smart_unicode

from baseparser import BaseParser
from utilites import DbUtilites
from models import Source, ExtCategory, ExtUnit, ExtUnitProperty, Unit, UnitProperty, Stage

class Parse2GisCsv(BaseParser):
    """
    Class to Parse units from csv file(2Gis catalog is used).
    """
    def __init__(self, name, file_path):
        self.link = file_path
        #self.source_name = "2GisCsv"
        self.source_name = name

        # Add new Source if need
        source, created = Source.objects.get_or_create(
            name=self.source_name, defaults={'link': file_path})
    
    def parse(self, **kwargs):
        """
        Hight level function to parse data from csv to database.
        Function imports all organization using sepateting by categories.
        This function do not rewrite all data in database.
        It tries to update it first.
        """
        # Before parsing
        self.prepare_parse()
        
        with open(self.link, 'rb') as f:
            reader = csv.reader(f, delimiter=',', quotechar='"')
       
            # for each line in csv file
            for row in reader:
                self._processing_row(row)
                
        # After parsing
        self.cleanup_parse()
                
    def prepare_parse(self, **kwargs):
        """
        Set session id and remove all ext unit properties of this source.
        """
        # Init session of parsing
        self.session = str(uuid.uuid4())
        
        # Set stage
        DbUtilites.set_stage('ExtImport', 'Yes', self.session)
        
        # Removig props
        categories = ExtCategory.objects.filter(
            source=Source.objects.get(name=self.source_name))
        for category in categories.all():
            for unit in category.units.all():
                unit.properties.all().delete()
                
    def cleanup_parse(self, **kwargs):
        """
        Delete removed from source ext units.
        """
        categories = ExtCategory.objects.filter(
            source=Source.objects.get(name=self.source_name))
        for category in categories.all():
            category.units.exclude(session=self.session).delete()
            
        # Set stage
        DbUtilites.set_stage('ExtImport', 'Yes', self.session)
        DbUtilites.set_stage('ExtExport', 'No', self.session)
    
    def export_ext_units(self, **kwargs):
        """
        Move just parsed new ext units to units.
        """
        sessions = [stage.session
                    for stage in Stage.objects.filter(stype="ExtExport", stage="No")]
        ext_units = ExtUnit.objects.filter(session__in=sessions)
        
        # Add or update units
        for ext_unit in ext_units:
            self._export_ext_unit_to_unit(ext_unit)
            
        # Remove old properties from unit
        self._remove_old_properties()
        
        # Clustering
        self._clustering()
                          
    def _export_ext_unit_to_unit(self, ext_unit, **kwargs):
        """
        Create unit if need, add ext unit to unit, add category.
        """
        # First, get or create new unit
        unit, created = Unit.objects.get_or_create(
            name=ext_unit.name)

        # Second, add ext unit to unit
        unit.ext_units.add(ext_unit)

        # Third, add category to unit
        for ext_category in ext_unit.ext_categories.all():
            for category in ext_category.categories.all():
                unit.categories.add(category)
                
        unit.save()
                
        # Fourth, set unit props
        for prop in ext_unit.properties.all():
            unit_prop, created = UnitProperty.objects.get_or_create(
                unit=unit, ptype=prop.ptype, value=prop.value)
            
    def _remove_old_properties(self, **kwargs):
        """
        Remove old properties from unit
        """
        for unit in Unit.objects.all():
            # Getting all 'unit' properties
            unit_props = unit.properties.all()
            # Getting all properties from all ext units related with 'unit'
            ext_props = [ext_unit.properties.all() for ext_unit in unit.ext_units.all()]
            
            if len(ext_props) > 0:
                ext_props = ext_props[0]
            else:
                return

            ext_props_names = ["{0}_{1}".format(
                smart_str(ext_prop.ptype), smart_str(ext_prop.value))
                for ext_prop in ext_props]
            
            for unit_prop in unit_props.all():
                prop_name = "{0}_{1}".format(
                    smart_str(unit_prop.ptype), smart_str(unit_prop.value))
                if prop_name not in ext_props_names:
                    unit_prop.delete()
                    
    def _clustering(self, **kwargs):
        """
        Clustering unit. Based on equal property of different units
        """
        # Clustering by phone
        newest_eq_props = UnitProperty.objects.get_newest_equals("Phone")
        for prop in newest_eq_props:
            self._merge_unit(prop)
            
         # Clustering by site
        newest_eq_props = UnitProperty.objects.get_newest_equals("Site")
        for prop in newest_eq_props:
            self._merge_unit(prop)
            
         # Clustering by e-mail
        newest_eq_props = UnitProperty.objects.get_newest_equals("E-mail")
        for prop in newest_eq_props:
            self._merge_unit(prop)
        
    def _merge_unit(self, prop, **kwargs):
        """
        Merge unit based on newest property of equal properties
        """
        base = prop.unit
        merged = [item.unit
                    for item in UnitProperty.objects.filter(
                        ptype=prop.ptype, value=prop.value)]
        
        for child in merged:
            # Merge categories
            for category in child.categories.all():
                base.categories.add(category)
                
            # Merge ext units
            for ext_unit in child.ext_units.all():
                base.ext_units.add(ext_unit)
                
            # Merge properties
            for props in child.properties.all():
                base.properties.add(props)
                
            # Removing child
            child.delete()
        
        # Saving to update 'last_update' field    
        base.save()

    def _processing_row(self, info, **kwargs):
        """
        Works with one row in csv file. Parse it. 
        """
        # Checking fields
        if not info[0] or not info[2]:
            raise Exception("Empty necessary unit't fields in csv file: {0}".format(info))
            
        # Get or create Ext Category
        category = self._set_ext_category(info)
        
        # Get or create Ext Unit
        unit = self._set_ext_unit(category, info)
        
        # Set Ext Unit propetries
        self._set_ext_properties(unit, info)
    
    def _set_ext_category(self, info, **kwargs):
        """
        Get or create Ext Category
        """ 
        category_name = info[3]
        if not category_name:
            category_name = "Undefined"
        
        # Get or create Ext Category 
        category, created = ExtCategory.objects.get_or_create(
            name=category_name,
            source=Source.objects.get(name=self.source_name),
            defaults={'link': '', 'last_update': ''})
        
        return category

    def _set_ext_unit(self, category, info, **kwargs):
        """
        Get or create Ext Unit
        """ 
        unit, created = ExtUnit.objects.get_or_create(
            origin=info[0], defaults={'name': info[2]})
        if not created:
            unit.name = info[2]
            
        unit.ext_categories.add(category)
        unit.session = self.session
        unit.save()
            
        return unit
    
    def _set_ext_properties(self, unit, info, **kwargs):
        """
        Set Ext Unit propetries
        """
        if info[4] and info[5] and info[6]:
            address = "{0}, {1}, {2}".format(info[4], info[5], info[6])
            self._set_ext_prop(unit, "Address", address)
            
        if info[7]:
            self._set_ext_prop(unit, "Office", info[7])
            
        if info[8]:
            self._set_ext_prop(unit, "E-mail", info[8])
            
        if info[9]:
            self._set_ext_prop(unit, "Phone", info[9])
            
        if info[10]:
            self._set_ext_prop(unit, "Fax", info[10])
            
        if info[11]:
            self._set_ext_prop(unit, "Site", info[11])
                
    def _set_ext_prop(self, unit, ptype, data, **kwargs):
        """
        Set new props if need
        """
        # Get values
        if ptype == "Address":
            values = [data]
        elif ptype == "Site":
            values = self._get_sites(data)
        elif ptype == "Phone":
            values = [re.sub(r'\ |\?|\.|\!|\/|\;|\:|\(|\)|\-|\"', '', item) for item in data.decode('utf-8').split(',')]
        elif ptype == "Fax":
            values = [re.sub(r'\ |\?|\.|\!|\/|\;|\:|\(|\)|\-|\"', '', item) for item in data.decode('utf-8').split(',')]
        else:
            values = data.decode('utf-8').split(',')
        
        # Check values in ext units props
        for value in values:
            prop, created = ExtUnitProperty.objects.get_or_create(
                ext_unit=unit, ptype=ptype, value=value)
        
    def _get_sites(self, sites_str, **kwargs):
        """
        Parse string to get list of sites
        """
        result = []

        prepare = lambda item: re.sub(r'(www\.|http\:\/\/|https\:\/\/|\/.*)', '', item.strip())
        
        sites = [prepare(item) for item in sites_str.decode('utf-8').split(',')]

        # Getting sites URLs
        for site in sites:
            matching = re.match(u'([a-zа-яё0-9]([a-zа-яё0-9\-]{0,61}[a-zа-яё0-9])?\.)+[a-zа-яё0-9]{2,6}',
                site.strip(), re.U | re.I)
            if matching:
                result.append(matching.group(0))

        return result