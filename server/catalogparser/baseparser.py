# -*- coding: utf-8 -*-

from abc import ABCMeta, abstractmethod, abstractproperty

class BaseParser(object):
    __metaclass__ = ABCMeta
    
    link = None
    source_name = None
    session = None
    
    @abstractmethod
    def parse(self, **kwargs):
        """
        Base function to parse source. Must be defined in child class
        """
        pass
    
    @abstractmethod
    def export_ext_units(self, **kwargs):
        """
        Base function to move just parsed new ext units to units.
        Must be defined in child class
        """
        pass
    
    @abstractmethod
    def _clustering(self, **kwargs):
        """
        Base function to clustering unit.
        Must be defined in child class
        """
        pass