# -*- coding: utf-8 -*-

from django.db import models

class Source(models.Model):
    """
    Class to represent external catalogs
    """
    name = models.CharField(max_length=1000)
    link = models.URLField(null=True)
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        db_table = "s_source"

class ExtCategory(models.Model):
    """
    Class to represent categories from external catalogs
    """
    name = models.CharField(max_length=1000)
    link = models.URLField(null=True)
    source = models.ForeignKey(Source)
    last_update = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        db_table = "s_ext_categories"
        
class ExtUnit(models.Model):
    """
    Class to represent units in external categories
    """
    origin = models.CharField(max_length=1000)
    name = models.CharField(max_length=1000)
    ext_categories = models.ManyToManyField(ExtCategory, db_table="s_extcategories_extunits_comm", related_name="units")
    session = models.CharField(max_length=36)
    last_update = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        db_table = "s_ext_units"
        
class ExtUnitProperty(models.Model):
    """
    Class to represent property of external units
    """
    PTYPE_CHOICES = (
        ('Address', 'Адрес'),
        ('Phone', 'Телефон'),
        ('Office', 'Офис'),
        ('Fax', 'Факс'),
        ('E-mail', 'E-mail'),
        ('Site', 'Сайт')
    )
    ext_unit = models.ForeignKey(ExtUnit, related_name="properties")
    ptype = models.CharField(max_length=50, choices=PTYPE_CHOICES)
    value = models.CharField(max_length=255, db_index=True)
    
    def __unicode__(self):
        #return "{0} - {1}".format(self.ptype, self.value)
        return self.value
    
    class Meta:
        db_table = "s_etx_units_props"
        
class Category(models.Model):
    """
    Class to represent categories in internal catalog
    """
    name = models.CharField(max_length=1000)
    ext_categories = models.ManyToManyField(ExtCategory, db_table="s_categories_comm", related_name="categories")
    last_update = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        db_table = "s_categories"
        
class CategoryProperty(models.Model):
    """
    Class to represent property of internal categories
    """
    category = models.ForeignKey(Category)
    ptype = models.CharField(max_length=50)
    value = models.CharField(max_length=255, db_index=True)
    
    def __unicode__(self):
        #return "{0} - {1}".format(self.ptype, self.value)
        return self.value
    
    class Meta:
        db_table = "s_categories_props"

class Unit(models.Model):
    """
    Class to represent units in internal categories
    """
    name = models.CharField(max_length=1000)
    categories = models.ManyToManyField(Category, db_table="s_categories_units_comm", related_name="units")
    ext_units = models.ManyToManyField(ExtUnit, db_table="s_units_comm")
    last_update = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        db_table = "s_units"
        
class UnitPropertyManager(models.Manager):
    """
    Manager for UnitProperty
    """
    def get_newest_equals(self, ptype):
        """
        We look for the same elements (type, value),
        united in groups sorted by time of last modification,
        and takes the first element of the group as the most urgent.
        """
        elems = []
        raw_elems = self.raw('''SELECT `id`, `unit_id`, MAX(last_update)  as last_update , `ptype`, `value`
                                FROM `s_units_props`
                                WHERE `value` IN (
                                    SELECT `value`
                                    FROM `s_units_props`
                                    GROUP BY `value` HAVING count(*)>1)
                                AND `ptype` = %s
                                GROUP BY `value`
                                ORDER BY `last_update` DESC''', [ptype])
        for item in raw_elems:
            elems.append(item)
        
        return elems
        
class UnitProperty(models.Model):
    """
    Class to represent property of internal units
    """
    PTYPE_CHOICES = (
        ('Address', 'Адрес'),
        ('Phone', 'Телефон'),
        ('Office', 'Офис'),
        ('Fax', 'Факс'),
        ('E-mail', 'E-mail'),
        ('Site', 'Сайт')
    )
    unit = models.ForeignKey(Unit, related_name="properties")
    ptype = models.CharField(max_length=50, choices=PTYPE_CHOICES)
    value = models.CharField(max_length=255, db_index=True)
    last_update = models.DateTimeField(auto_now=True)
    
    objects = UnitPropertyManager()
    
    def __unicode__(self):
        #return "{0} - {1}".format(self.ptype, self.value)
        return self.value
    
    class Meta:
        db_table = "s_units_props"
        
class Stage(models.Model):
    """
    Class to represent stages of process
    """
    STYPE_CHOICES = (
        ('ExtImport', 'Импорт внешних юнитов'),
        ('ExtExport', 'Экспорт внешних юнитов'),
    )
    STAGE_CHOICES = (
        ('Yes', 'Да'),
        ('No', 'Нет'),
    )
    stype = models.CharField(max_length=50, choices=STYPE_CHOICES)
    stage = models.CharField(max_length=50, choices=STAGE_CHOICES)
    session = models.CharField(max_length=36)
    timestamp = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return "{0}:{1} - {2}".format(self.stype, self.session, self.stage)
    
    class Meta:
        db_table = "s_stages"
