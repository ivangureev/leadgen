# -*- coding: utf-8 -*-

import urllib2
import urllib
import simplejson
import logging

from celery import task
from django.conf import settings

from api import Api

def on_retry_handler(self, exc, task_id, args, kwargs, einfo):
    logger = logging.getLogger('celery_server')
    logger.warning('Task retry...', exc_info=True, extra={
        'exception': exc,
        'position arguments': args,
        'key arguments': kwargs,
        'task_id': task_id,
        'einfo': einfo
    })

@task(on_retry = on_retry_handler)
def parse_2gis(*args, **kwargs):
    try:
        Api().parse_2gis()
    except Exception, exc:
        raise parse_2gis.retry(exc=exc, countdown=2, max_retries=3)