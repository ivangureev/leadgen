# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
import views

urlpatterns = patterns('',
    url(r'^competitors/(?P<id>\d*)\/?$', views.competitors),
    url(r'^selected_competitors/(?P<id>\d*)\/?$', views.selected_competitors),
    url(r'^keywords/(?P<id>\d*)\/?$', views.keywords),

    url(r'^api/(?P<action>\w+)\/?$', views.api),
)