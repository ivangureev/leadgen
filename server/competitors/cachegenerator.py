# -*- coding: utf-8 -*-

import simplejson

from django.db import transaction
from django.core.exceptions import ObjectDoesNotExist

from keywords.models import KeySet
from spider.models import Search
from competitors.models import *

class CacheCrossSearchGenerator:
    """
    Class to generate cross table for represents
    From (key words has competitors)
    to (competitors has keywords)
    with competitors positions of current serp
    """


    class CrossList:
        """
        Internal class to represent list of
        unique competitors
        """
        def __init__(self):
            self.competitors = []
        
        def add(self, competitor_item):
            """
            Add new competitors to list
            if competitors already exists (equality by domain) then
            competitors will merge together (their keywords will concate together)
            """
            if not isinstance(competitor_item, CacheCrossSearchGenerator.CompetitorItem):
                raise Exception("competitor_item must be a CompetitorItem instance")
            
            try:
                index = self.competitors.index(competitor_item)
                self.competitors[index].merge(competitor_item)
            except ValueError:
                self.competitors.append(competitor_item)
                
    
    class CompetitorItem:
        """
        Internal class to represent item of competitor
        which has list of keyword items
        before saving to database need to calculate occurrences
        of self.keywords with all keywords in key set
        """
        def __init__(self, competitor_info):
            self.domain = competitor_info["domain"]
            self.title = competitor_info["title"]
            self.snippet = competitor_info["snippet"]

            self.items = []
            
        def __eq__(self, other):
            return (isinstance(other, self.__class__)
                and self.domain == other.domain)
    
        def __ne__(self, other):
            return not self.__eq__(other)

        def __str__(self):
            return unicode(self.domain)

        def __repr__(self):
            return self.__str__()
        
        def add(self, keyword_item):
            """
            Add new keyword item to list
            if keyword item already exists (equality by key) then
            new position of keyword in serp will add to keyword item
            """
            if not isinstance(keyword_item, CacheCrossSearchGenerator.KeyWordItem):
                raise Exception("keyword_item must be a KeyWordItem instance")
            
            try:
                index = self.items.index(keyword_item)
                self.items[index].add_position(keyword_item)
            except ValueError:
                self.items.append(keyword_item)

        def get_keywords(self):
            """
            Returns list of dict with key words information:
            [
                {'key': bay a car, 'position': {'2': 4, '3': 23}},
                ...
            ]
            """
            result = []
            for kwi in self.items:
                result.append(kwi.get_item())

            return result
                
        def merge(self, competitor_item):
            """
            Merge items (key word items) of two competitor's
            """
            self.items.extend(competitor_item.items)

        def get_percentage(self, keywords_count):
            # easy calculate: not for real life
            return (len(self.items) / float(keywords_count)) * 100.0
            

    class KeyWordItem:
        """
        Internal class to represent key word item
        contains key of keyword (as string) and 
        possitions of parent competitor at sept
        of this key
        """
        def __init__(self, key, position):
            self.key = key
            
            if not isinstance(position, dict):
                raise Exception("Position of KeyWordItem must be a dict instance")
            
            self.position = {position["search_vendor"]: position["position"]}
            
        def __eq__(self, other):
            return (isinstance(other, self.__class__)
                and self.key == other.key)
    
        def __ne__(self, other):
            return not self.__eq__(other)

        def __str__(self):
            return unicode(self.key)
            
        def __repr__(self):
            return self.__str__()
        
        def add_position(self, keyword_item):
            """
            Update possitions
            """
            self.position.update(keyword_item.position)
            
        def get_item(self):
            """
            Returns dict of this keyword
            """
            return {'key': self.key, 'position': self.position}
            
    def __init__(self):
        pass
    
    def generate(self, keyset_id):
        """
        Generate cross search
        """
        self.cross_searches = self.CrossList()
        self.keyset = KeySet.objects.get(pk=keyset_id)

        if not self.__check_need_generate():
            return False
        
        searches = Search.objects.filter(key_word__keysets=self.keyset)
        
        # total key words count in keyset
        keywords_count = len(searches)
        self.cross_searches.keywords_count = keywords_count
        
        for search in searches.all():
            competitors = simplejson.loads(search.search)
            for index, competitor in enumerate(competitors, start=1):
                com = self.CompetitorItem(competitor)
                
                position = {"search_vendor": search.search_vendor.id, "position": index}
                kwi = self.KeyWordItem(search.key_word.key, position)
                
                com.add(kwi)
                self.cross_searches.add(com)
        
        self.__clean_db()
        self.__save_to_db()

        return True
            
    def __check_need_generate(self):
        """
        Check if need to generate or regenerate cross search
        """
        try:
            last_cross = CacheCrossSearch.objects.filter(key_set=self.keyset).latest('last_update')
            last_search = Search.objects.filter(key_word__keysets=self.keyset).latest('last_update')
            
            if last_cross.last_update < last_search.last_update:
                return True
            else:
                return False
        except ObjectDoesNotExist, exc:
            return True
        
    def __clean_db(self):
        """
        Cleaning previous CacheCrossSearch items from database
        """
        CacheCrossSearch.objects.filter(key_set=self.keyset).delete()
        
    @transaction.commit_manually    
    def __save_to_db(self):
        """
        Save to database
        """
        try:
            for competitor in self.cross_searches.competitors:
                com = CacheCrossSearch()
                
                com.key_set = self.keyset
                com.competitor_domain = competitor.domain
                com.competitor_title = competitor.title
                com.competitor_snippet = competitor.snippet
                com.key_words = simplejson.dumps(competitor.get_keywords())
                com.percentage = competitor.get_percentage(self.cross_searches.keywords_count)

                com.save()   
        except Exception, exp: 
            transaction.rollback()
            raise Exception(exp)
        else:
            transaction.commit() 
            