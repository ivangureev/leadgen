# -*- coding: utf-8 -*-

from django.db import transaction

from tools.baseapi import BaseApi
from competitors.models import SelectedCompetitor
from keywords.models import KeySet

class Api(BaseApi):
    """
    """
    def __init__(self, *args, **kargs):
        super(Api, self).__init__(*args, **kargs)

    @transaction.commit_manually  
    def copy_competitors(self):
        """
        Copy all competitors of key_set_id to all other keysets
        """
        key_set_id = self.paramsQuery.get('key_set_id', None)

        keyset = KeySet.objects.get(pk=key_set_id)
        competitors_to_clone = SelectedCompetitor.objects.filter(key_set=keyset)
        keysets_to_clone_in = KeySet.objects.filter(category=keyset.category).exclude(pk=key_set_id)

        try:
            for key_set in keysets_to_clone_in:
                for competitor in competitors_to_clone:
                    if len(SelectedCompetitor.objects.filter(key_set=key_set, competitor_domain=competitor.competitor_domain)) != 0:
                        continue
                    new_competitor = SelectedCompetitor()
                    new_competitor.key_set = key_set
                    new_competitor.competitor_domain = competitor.competitor_domain
                    new_competitor.competitor_title = competitor.competitor_title
                    new_competitor.competitor_snippet = competitor.competitor_snippet
                    new_competitor.percentage = competitor.percentage

                    new_competitor.save()
        except Exception, exp: 
            transaction.rollback()
            raise Exception(exp)
        else:
            transaction.commit() 

        return True
