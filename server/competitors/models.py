# -*- coding: utf-8 -*-

import simplejson

from django.db import models
from django.core.exceptions import ObjectDoesNotExist

from spider.models import SearchVendor
from keywords.models import KeySet
from tools.common import Common as CommonTools

class CacheCrossSearchManager(models.Manager):
    """
    Manager for CacheCrossSearch
    """
    def get_keywords(self, keyset, domain):
        """
        Get keywords with positions of domain in SERP of keywords in keyset
        """
        try:
            possible_domains = CommonTools.get_possible_domains(domain)

            cross_serp = CacheCrossSearch.objects.get(key_set=keyset, competitor_domain__in=possible_domains)
            keywords = simplejson.loads(cross_serp.key_words)

            return keywords
        except (ObjectDoesNotExist, Exception), exc:
            return list()


class CacheCrossSearch(models.Model):
    """
    Class to represent cross values of searches
    """
    # search_vendor = models.ForeignKey(SearchVendor, related_name="searches")
    key_set = models.ForeignKey(KeySet, related_name="cache_cross_search")
    competitor_domain = models.CharField(max_length=255)
    competitor_title = models.TextField()
    competitor_snippet = models.TextField()
    key_words = models.TextField(null=True)
    percentage = models.DecimalField(max_digits=5, decimal_places=2)
    last_update = models.DateTimeField(auto_now=True)

    objects = CacheCrossSearchManager()

    def __unicode__(self):
        return self.competitor_domain
    
    class Meta:
        db_table = "s_cache_cross_search"

class SelectedCompetitor(models.Model):
    """
    Class to represent selected competitors of key sets
    """
    key_set = models.ForeignKey(KeySet, related_name="selected_competitors")
    competitor_domain = models.CharField(max_length=255)
    competitor_title = models.TextField()
    competitor_snippet = models.TextField()
    percentage = models.FloatField()
    last_update = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return self.competitor_domain
    
    class Meta:
        db_table = "s_selected_competitors"
