# -*- coding: utf-8 -*-

from django.conf import settings

from api import Api
from competitors.models import *
from keywords.models import KeySet
from competitors.cachegenerator import CacheCrossSearchGenerator
from tools.restmiddleware import RESTHandlerMiddleware
from tools.rest import RESTHandler
from tools.response import Response
from constants.models import Constant

@Response.standart
def competitors(request, id):
    handler = RESTHandler(
        request=request,
        model=CacheCrossSearch,
        element_id=id,
        cache_generator=CacheCrossSearchGenerator)
    
    if handler.format == 'GET':
        return handler.read(cache_generator_field='key_set__id')  
    # elif handler.format == 'POST':
    #     return handler.create()
    # elif handler.format == 'PUT':
    #     return handler.update()
    # elif handler.format == 'DELETE':
    #     return handler.delete()
    else:
        raise Exception("Undefined method")

@Response.standart
def selected_competitors(request, id):
    handler = RESTHandler(
        request=request,
        model=SelectedCompetitor,
        element_id=id)
    
    if handler.format == 'GET':
        return handler.read()  
    elif handler.format == 'POST':
        return handler.create(unique_fields=['competitor_domain'])
    elif handler.format == 'PUT':
        return handler.update()
    elif handler.format == 'DELETE':
        return handler.delete()
    else:
        raise Exception("Undefined method")

@Response.standart
def keywords(request, id):
    handler = RESTHandlerMiddleware(
        request=request,
        model=CacheCrossSearch,
        element_id=id)
    
    if handler.format == 'GET':
        max_count = Constant.objects.get_value('MAX_POSITION_IN_SERP')
        return handler.read(
        	json_field='key_words',
            extra_fields={
                'related_position_yandex': lambda p: (max_count - p['position'].itervalues().next() + 1) / (max_count / 100.0),
                'position_yandex': lambda p: p['position'].itervalues().next(),
            })
        	# extra_fields={'position_yandex': lambda p: p['position'].itervalues().next()})  
    # elif handler.format == 'POST':
    #     return handler.create()
    # elif handler.format == 'PUT':
    #     return handler.update()
    # elif handler.format == 'DELETE':
    #     return handler.delete()
    else:
        raise Exception("Undefined method")

@Response.api
def api(request, action): 
    api = Api(request)
    if action == 'copy_competitors':
        return api.copy_competitors()