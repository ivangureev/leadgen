# -*- coding: utf-8 -*-

from urlparse import urlparse

class Common:
    """
    Class for common operations
    """
    @staticmethod
    def get_possible_domains(domain):
        domains = []
        hostname = urlparse(u"http://{0}".format(domain.site.value)).hostname
            
        hostname = unicode(hostname).encode('idna')
        domains.append(hostname)
        if hostname.split(".")[:1][0] == u'www':
            domains.append(u".".join(hostname.split(".")[1:]))
        else:
            domains.append(u"www.{0}".format(hostname))

        return domains