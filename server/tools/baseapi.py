# -*- coding: utf-8 -*-

from django.http import QueryDict

class BaseApi(object):
    """
    Base API class
    """
    def __init__(self, request=None):
        self.paramsQuery = request.POST if request else None