# -*- coding: utf-8 -*-

import logging
import traceback
import simplejson
import json
import datetime
from functools import wraps

from django.utils.dateformat import format
from django.http import HttpResponse
from django.db import models

class Params:
    """
    Class to help get deal with Ext Js framework 
    """
    @staticmethod
    def standart_get(request):
        limit = int(request.GET.get('limit', 1000))
        start = int(request.GET.get('start', 0))
        filter = Params._filter(request)
        sort = Params._sort(request)
    
        return (limit, start, filter, sort)
        
    @staticmethod
    def standart_post(request):
        result = None
        if request.body:
            result = json.loads(request.body)
        return result
    
    @staticmethod
    def standart_put(request):
        result = None
        if request.body:
            result = json.loads(request.body)
        return result
    
    @staticmethod
    def _filter(request, *args, **kwargs):
        """
        Get filter merged with 'find' paramert
        """
        native_filter = Params._named_params(request, 'filter')
        find = FindParser.parse_find(request, 'find')
        
        return dict(native_filter.items() + find.items())
        
    @staticmethod
    def _sort(request, *args, **kwargs):
        """
        Get sort
        """
        wet_sort = json.loads(request.GET.get('sort', "[]"))
        
        result = []
        for item in wet_sort:
            if(item["direction"] == "DESC"):
                result.append("-{0}".format(item["property"]))
            else:
                result.append("{0}".format(item["property"]))
            
        return result
    
    @staticmethod
    def _named_params(request, key, *args, **kwargs):
        """
        Provides reorganazing list of dict of items, like:
            [{"property":"categories__id","value":null}]
        to:
            {"categories__id": null}
        """
        wet_params = json.loads(request.GET.get(key, "[]"))
        
        result = {}
        for item in wet_params:
            result[item["property"]] = item["value"]
            
        return result
    
class FindParser:
    """
    Class to parse GET find parameter.
    """
    @staticmethod
    def parse_find(request, par_name, *args, **kwargs):
        result = dict()
        
        find = json.loads(request.GET.get(par_name, "[]"))
        for item in find:
            if item["type"] == "numeric":
                result.update(FindParser._parse_numeric(item))
            elif item["type"] == "string":
                result.update(FindParser._parse_string(item))
            elif item["type"] == "date":
                result.update(FindParser._parse_date(item))
            elif item["type"] == "list":
                result.update(FindParser._parse_list(item))
            elif item["type"] == "boolean":
                result.update(FindParser._parse_boolean(item))
        
        return result
                
    @staticmethod
    def _parse_numeric(item):
        if item["comparison"] == 'lt' or item["comparison"] == 'gt':
            key = "{0}__{1}".format(item["field"], item["comparison"])
            value = item["value"]
            return {key: value}
        elif item["comparison"] == 'eq':
            return {item["field"]: item["value"]}
            
    @staticmethod
    def _parse_string(item):
        key = "{0}__icontains".format(item["field"])
        value = item["value"]
        return {key: value}
        
    @staticmethod
    def _parse_date(item):
        wet_date = item["value"].split('/')
        
        year = int(wet_date[2])
        month = int(wet_date[0])
        day = int(wet_date[1])
        
        date = datetime.date(year, month, day)
        
        if item["comparison"] == 'lt' or item["comparison"] == 'gt':
            key = "{0}__{1}".format(item["field"], item["comparison"])
            value = date
            return {key: value}
        elif item["comparison"] == 'eq':
            find = {
                "{0}__year".format(item["field"]): year,
                "{0}__month".format(item["field"]): month,
                "{0}__day".format(item["field"]): day
            }
            return find
        
    @staticmethod
    def _parse_list(item):
        key = "{0}__in".format(item["field"])
        value = item["value"]
        return {key: value}
        
    @staticmethod
    def _parse_boolean(item):
        key = item["field"]
        value = item["value"]
        return {key: value}
    
class RESTHandler:
    """
    Class to helps handle request,
    return items, creating, updating or deleting items
    (CRUD)
    """
    model = None
    
    format = None
    
    limit = None
    start = None
    filter = None
    sort = None
    
    data = None
    
    m2m = None
    
    annotate_fields=None
    aggregate_fields=None
    
    element_id = None

    cache_generator = None
    
    def __init__(self, request, model, m2m=None, annotate_fields=None, aggregate_fields=None, element_id=None, cache_generator=None):
        # django model's class
        self.model = model
        
        # GET/POST/PUT/DELETE
        self.format = request.method
        
        # set get params if exist
        self.limit, self.start, \
        self.filter, self.sort = Params.standart_get(request)
        
        # set POST, PUT params if exist
        self.data = Params.standart_post(request)
        
        # dict of related many to namy fields. {'category_id': ('categories', Category)}
        if m2m:
            self.m2m = m2m
        else:
            self.m2m = {}
            
        # dict of fields that contain Count, Sum , etc. {'count': models.Count('keysets')}
        if annotate_fields:
            self.annotate_fields = annotate_fields
        else:
            self.annotate_fields = {}
        
        # DOES NOT USE YET    
        # dict of fields that contain Min, Max , etc. {'max_price': models.Max('price')}
        if aggregate_fields:
            self.aggregate_fields = aggregate_fields
        else:
            self.aggregate_fields = {}
        
        # element id if exists
        self.element_id = element_id

        # class to generate cache if need
        self.cache_generator = cache_generator
    
    def read(self, extra_fields=None, extra_filters=None, cache_generator_field=None):
        """
        Handle GET method
        """
        response = dict(results=[], totalCount=0)

        # generate cache if need
        if self.cache_generator and cache_generator_field in self.filter.keys():
            self.cache_generator().generate(self.filter[cache_generator_field])
        
        model = self.model

        if extra_fields:
            self._reset_sort(extra_fields)

        items = model.objects.annotate(**self.annotate_fields)
        items = items.filter(**self.filter)
        #items = items.aggregate(**self.aggregate_fields)
        items = items.order_by(*self.sort)

        for item in items[self.start : self.start + self.limit]:
            fields = {}

            # all fields exclude many-to-many fields
            for field in item._meta.fields:
                value = getattr(item, field.name)
                fields[field.name] = self._prepare_field(field, value)
                
            # all annotate fields
            for field_name in self.annotate_fields.keys():
                value = getattr(item, field_name)
                fields[field_name] = value
                
            # all many-to-many fields
            # for example: new_name = "category_id" (returned field name),
            # m2m_props = ("categories", Category) (m2m field name, related model class)
            for new_name, m2m_props in self.m2m.items():
                try:
                    fields[new_name] = self._prepare_m2m_field(item, m2m_props[0])
                except Exception, e:
                    pass
                
            # set extra fields. You can rewrite standart fields
            if extra_fields:
                for field in extra_fields:
                    key = field['field_name']
                    function = field['getter']
                    fields[key] = function(item)
            
            response["results"].append(fields)

        # If the standard filters is not enough
        if extra_filters:
            for function in extra_filters:
                response["results"] = [item for item in response["results"] if function(item)]
            
        response["totalCount"] = len(items)
        
        return response
    
    def create(self, unique_fields=None):
        """
        Handle POST method
        """
        response = dict()
        
        model = self.model
        # set fields without relations
        data = self._prepare_to_set_data()

        # check for unique
        if unique_fields is not None and \
            not self._check_unique(data, unique_fields):
            return response

        # if unique
        item = model(**data)

        # set relations
        self._set_relations(item)

        item.save()
        
        return response
    
    def update(self):
        """
        Handle PUT method
        """
        response = dict()
        
        model = self.model
        data = self._prepare_to_set_data()
        item = model.objects.get(id=self.element_id)

        for attr_name, value in data.items():
            setattr(item, attr_name, value)
        
        self._set_relations(item)
        item.save()
        
        return response
    
    def delete(self):
        """
        Handle DELETE method
        """
        response = dict()
        
        model = self.model
        item = model.objects.get(id=self.element_id)
        if item:
            item.delete()
        
        return response

    def _reset_sort(self, extra_fields):
        new_sort = []
        for sort_param in self.sort:
            for field in extra_fields:
                key = field['field_name']
                new_sort_param = field['order_by']
                sort_param = sort_param.replace(key, new_sort_param)

            new_sort.append(sort_param)
        self.sort = new_sort
        
    def _prepare_field(self, field, value):
        field_type = field.get_internal_type()
        
        if field_type == 'ForeignKey':
            value = value.id
        elif field_type == 'DateTimeField':
            if isinstance(value, datetime.datetime):
                value = format(value, 'U')
        elif field_type == 'DecimalField':
            value = float(value)
            
        return value
    
    def _prepare_m2m_field(self, item, field_name):
        # get first of related items if there are many items
        related_items = getattr(item, field_name).all()
        valuesList = [f.id for f in related_items]
        value = simplejson.dumps(valuesList)
        
        return value
    
    def _prepare_to_set_data(self):
        data = dict(self.data)
        
        # check fields
        for key, value in data.items():
            # check for unnecessary fields
            if key not in self.model._meta.get_all_field_names():
                del data[key]

            # check for fields with one-to-many relations
            relation_fields = [f.name for f in self.model._meta.fields \
                               if f.get_internal_type() == "ForeignKey"]
            if key in relation_fields and key in data.keys():
                del data[key]
                
            # check for fields with many-to-many relations
            relation_fields = [f.name for f in self.model._meta.many_to_many]
            if key in relation_fields and key in data.keys():
                del data[key]
            
            # if m2m defined not in 'related_model'  
            for new_name, m2m_props in self.m2m.items():
                related_name, related_model = m2m_props
                if related_name in data.keys():
                    del data[related_name]
            
            # check for service fields
            if key in 'id last_update'.split(' ') and key in data.keys():
                del data[key]
        
        return data

    def _check_unique(self, data, unique_fields):
        model = self.model

        filter = dict((k,v) for k,v in data.items() if k in unique_fields)
        if not filter:
            return True

        items = model.objects.filter(**filter)
        if len(items):
            return False
        else:
            return True
    
    def _set_relations(self, item):
        data = dict(self.data)
      
        # one2many fields
        for field in self.model._meta.fields:
            field_type = field.get_internal_type()
            if field_type == 'ForeignKey':
                db_name = "{0}_id".format(field.name)
                if field.name in data.keys():
                    setattr(item, db_name, data[field.name])
        
        # if item is creating now, need to save it to database
        # before setting many-to-many relations
        if not item.id:
            item.save()
                    
        # m2m fields
        for new_name, m2m_props in self.m2m.items():
            related_name, related_model = m2m_props
            m2m_field = getattr(item, related_name)
            if new_name in data.keys() and data[new_name]:
                # data is list of ids of related objects
                relIds = json.loads(data[new_name])
                if len(relIds) > 0:
                    # already existing relations
                    related_items = getattr(item, related_name)
                    
                    # adding relations
                    for relId in relIds:
                        related_items_ids = [i.id for i in related_items.all()]
                        if relId not in related_items_ids:
                            related_item = related_model.objects.get(id=relId)
                            m2m_field.add(related_item)
                    
                    # removing relations
                    for rel_item in related_items.all():
                        if rel_item.id not in relIds:
                            related_item = related_model.objects.get(id=rel_item.id)
                            m2m_field.remove(related_item)
                else:
                    m2m_field.clear()

            