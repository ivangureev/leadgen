# -*- coding: utf-8 -*-

from catalogparser.models import Category, ExtCategory, Unit, ExtUnit

class Connector:
	def __init__(self):
		pass

	@staticmethod
	def connect_categories_units(ext_category):
		"""
		Connect ext_units of ext_category to units of category
		"""
		for category in ext_category.categories.all():
			ext_units = [item.id for item in ext_category.units.all()]
			units = Unit.objects.filter(ext_units__in=ext_units)
			category.units.add(*units)


		# category_ids_before = set([item.id for item in ext_category_before.categorie.all()])
		# category_ids_after = set([item.id for item in ext_category_after.categorie.all()])

		# category_ids_to_remove = category_ids_before.difference(category_ids_after) 
		# category_ids_to_create = category_ids_after.difference(category_ids_before) 

		# category_to_remove = Category.objects.filter(pk__in=category_ids_to_remove)
		# category_to_create = Category.objects.filter(pk__in=category_ids_to_create)

		# #connect
		# for category in category_to_create:
		# 	ext_units = [item.id for item in ext_category_after.units.all()]
		# 	units = Unit.objects.filter(ext_units__in=ext_units)

		# 	# clear relations for category
		# 	category.units.clear()
		# 	category.units.add(*units)

	@staticmethod
	def clear_categories_units(ext_category):
		for category in ext_category.categories.all():
			category.units.clear()