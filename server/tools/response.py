# -*- coding: utf-8 -*-

import logging
import traceback
import simplejson
from functools import wraps

from django.http import HttpResponse

logger = logging.getLogger('server')

class Response:
    """
    Class to wrap response to ext js by ajax 
    """
    @staticmethod
    def standart(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            response = {}
            try:
                response = func(*args, **kwargs)
                response["success"] = True
            except Exception, e:
                response["success"] = False
                response["exception"] = str(e)
                response["traceback"] = traceback.format_exc()

                request = args[0]
                logger.warning('Unsuccess standart response', exc_info=True, extra={
                    'culprit': request.path,
                    'request': request,
                    'response': response
                })

            finally:
                return HttpResponse(simplejson.dumps(response), mimetype="application/json")
        return wrapper

    @staticmethod
    def external(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            response = {}
            try:
                response = func(*args, **kwargs)
            except Exception, e:
                response["success"] = False
                response["exception"] = str(e)
                response["traceback"] = traceback.format_exc()

                request = args[0]
                logger.warning('Unsuccess external response', exc_info=True, extra={
                    'culprit': request.path,
                    'request': request,
                    'response': response
                })

            finally:
                return HttpResponse(simplejson.dumps(response), mimetype="application/json")
        return wrapper

    @staticmethod
    def api(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            response = {}
            try:
                request = args[0]
                if request.method != 'POST':
                    raise Exception("API actions must have POST method.")

                result = func(*args, **kwargs)
                response["result"] = result
                response["success"] = True
            except Exception, e:
                response["success"] = False
                response["exception"] = str(e)
                response["traceback"] = traceback.format_exc()

                request = args[0]
                logger.warning('Unsuccess api response', exc_info=True, extra={
                    'culprit': request.path,
                    'request': request,
                    'response': response
                })
            finally:
                return HttpResponse(simplejson.dumps(response), mimetype="application/json")
        return wrapper