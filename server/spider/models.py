# -*- coding: utf-8 -*-

from django.db import models

from catalogparser.models import UnitProperty
from keywords.models import KeySet, KeyWord

class SearchVendor(models.Model):
    """
    Class to represent search vendors like Yandex.ru, Google.com
    """
    name = models.CharField(max_length=1000)
    last_update = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        db_table = "s_search_vendors"

class Region(models.Model):
    """
    Class to represent regions properties of search vendor's
    """
    search_vendor = models.ForeignKey(SearchVendor, related_name="regions")
    code = models.CharField(max_length=255)
    name = models.CharField(max_length=1000, null=True)
    last_update = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return self.code
    
    class Meta:
        db_table = "s_regions"
        
class Domain(models.Model):
    """
    Class to represent domain's property
    """
    site = models.OneToOneField(UnitProperty, primary_key=True)
    creation_date = models.DateTimeField(null=True)
    expiration_date = models.DateTimeField(null=True)
    yaca = models.NullBooleanField()
    yaca_rubric = models.CharField(max_length=1000, null=True)
    dmoz = models.NullBooleanField()
    tic = models.PositiveIntegerField(null=True)
    pr = models.PositiveIntegerField(null=True)
    available = models.NullBooleanField()
    last_update = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return self.site.value
    
    class Meta:
        db_table = "s_domains"
        
class Search(models.Model):
    """
    Class to represent result of searching in json
    """
    #domain = models.ForeignKey(Domain, related_name="searches")
    search_vendor = models.ForeignKey(SearchVendor, related_name="searches")
    region = models.ForeignKey(Region, related_name="searches")
    key_word = models.ForeignKey(KeyWord, related_name="searches")
    search = models.TextField(null=True)
    count_results = models.PositiveIntegerField()
    last_update = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return self.search
    
    class Meta:
        db_table = "s_searches"

class Access(models.Model):
    """
    Class to represent accesses for workers
    """
    ip = models.IPAddressField()
    type = models.CharField(max_length=255)
    access = models.TextField()
    last_update = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return self.type
    
    class Meta:
        db_table = "s_accesses"
