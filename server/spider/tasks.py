# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
import logging

from django.utils.timezone import get_default_timezone, make_aware
from django.conf import settings
from celery import task
from celery.execute import send_task
from celery.task.schedules import crontab
from django.db.models import Q

from catalogparser.models import UnitProperty
from keywords.models import KeySet, KeyWord
from spider.models import *
from api import Api
from competitors.cachegenerator import CacheCrossSearchGenerator

def on_retry_handler(self, exc, task_id, args, kwargs, einfo):
    logger = logging.getLogger('celery_server')
    logger.warning('Task retry...', exc_info=True, extra={
        'exception': exc,
        'position arguments': args,
        'key arguments': kwargs,
        'task_id': task_id,
        'einfo': einfo
    })

@task(on_retry = on_retry_handler)
def refresh_sites_activity(*args, **kwargs):
    try:
        api = Api()
        api.update_sites_availability()

    except Exception, exc:
        raise refresh_sites_activity.retry(exc=exc, countdown=2, max_retries=3)

@task(on_retry = on_retry_handler)
def refresh_sites_authority(*args, **kwargs):
    try:
        api = Api()
        api.update_sites_authority()

    except Exception, exc:
        raise refresh_sites_authority.retry(exc=exc, countdown=2, max_retries=3)

@task(on_retry = on_retry_handler)
def refresh_serp(*args, **kwargs):
    try:
        api = Api()
        api.update_serp()

    except Exception, exc:
        raise refresh_serp.retry(exc=exc, countdown=2, max_retries=3)

@task(on_retry = on_retry_handler)
def regenerate_cross_serp(*args, **kwargs):
    try:
        keysets = KeySet.objects.all()
        for keyset in keysets.all():
            CacheCrossSearchGenerator().generate(keyset.pk)

    except Exception, exc:
        raise refresh_serp.retry(exc=exc, countdown=2, max_retries=3)

@task(on_retry = on_retry_handler)
def save_domain_info(result, site_id, *args, **kwargs):
    try:
        site = UnitProperty.objects.get(pk=site_id)

        try:
            # delete element if exists
            element = Domain.objects.get(site=site)
            element.delete()
        except Exception, exc:
            pass 

        element = Domain(site=site)
        
        if isinstance(result['whois']['creation_date'], datetime):
            element.creation_date = make_aware(result['whois']['creation_date'], get_default_timezone())
        if isinstance(result['whois']['expiration_date'], datetime):
            element.expiration_date = make_aware(result['whois']['expiration_date'], get_default_timezone())
        element.yaca = result['yaca']['in_yaca']
        element.yaca_rubric = result['yaca']['rubric']
        element.tic = result['yaca']['tic']
        element.dmoz = result['dmoz']['has']
        element.pr = result['pagerank']
        element.available = result['available']
        
        element.save()
        
        return result
    except Exception, exc:
        raise save_domain_info.retry(exc=exc, countdown=2, max_retries=3)

@task(on_retry = on_retry_handler)
def update_domain_info(result, site_id, *args, **kwargs):
    try:
        site = UnitProperty.objects.get(pk=site_id)

        element, created = Domain.objects.get_or_create(site=site)
        
        if 'whois' in result.keys():
            if isinstance(result['whois']['creation_date'], datetime):
                element.creation_date = make_aware(result['whois']['creation_date'], get_default_timezone())
            if isinstance(result['whois']['expiration_date'], datetime):
                element.expiration_date = make_aware(result['whois']['expiration_date'], get_default_timezone())

        if 'yaca' in result.keys():
            element.yaca = result['yaca']['in_yaca']

        if 'yaca' in result.keys():    
            element.yaca_rubric = result['yaca']['rubric']

        if 'yaca' in result.keys():
            element.tic = result['yaca']['tic']

        if 'dmoz' in result.keys():
            element.dmoz = result['dmoz']['has']

        if 'pagerank' in result.keys():    
            element.pr = result['pagerank']

        if 'available' in result.keys():
            element.available = result['available']
        
        element.save()
        
        return result
    except Exception, exc:
        raise save_domain_info.retry(exc=exc, countdown=2, max_retries=3)

@task(on_retry = on_retry_handler)
def save_serp_yandex(result_json, search_vendor_id, keyword_id, region_code, countresults, *args, **kwargs):
    try:
        search_vendor = SearchVendor.objects.get(pk=search_vendor_id)
        keyword = KeyWord.objects.get(pk=keyword_id)
        region, created = Region.objects.get_or_create(search_vendor=search_vendor, code=region_code)

        try:
            # delete element if exists
            element = Search.objects.get(search_vendor=search_vendor,
                region=region, key_word=keyword)
            element.delete()
        except Exception, exc:
            pass 

        element = Search(search_vendor=search_vendor,
            region=region, key_word=keyword,
            count_results=countresults, search=result_json).save()
    
        return result_json
    except Exception, exc:
        raise save_domain_info.retry(exc=exc, countdown=2, max_retries=3)