# -*- coding: utf-8 -*-

from datetime import datetime, timedelta

from celery.task import task
from celery.execute import send_task
from django.conf import settings
from django.utils.timezone import get_default_timezone, make_aware
from django.db.models import Q

from tools.baseapi import BaseApi
from catalogparser.models import UnitProperty
from keywords.models import KeySet, KeyWord
from spider.models import *
from constants.models import Constant

class Api(BaseApi):
    """
    Class contains actions that parse searching sites
    or use third partie's services
    """
    def __init__(self, *args, **kwargs):
        super(Api, self).__init__(*args, **kwargs)

    def update_sites_availability(self):
        """
        Complite sites base info like whois, pagerank, dmoz, yaca
        """
        if self.paramsQuery:
            id = self.paramsQuery.get('site_id', None)
            if id:
                self.__set_site_availability(id)
                return True
        
        refresh_rate = Constant.objects.get_value('DOMAINS_REFRESH_RATE_DAYS')
        point_date = make_aware(datetime.today() - timedelta(days=refresh_rate), get_default_timezone())

        # # sites need to update availability
        domain_rows = Domain.objects.filter(Q(last_update__lte=point_date) | Q(available=None))

        debug = getattr(settings, 'DEBUG', False)
        if debug:
            for domain_row in domain_rows[:1]:
                self.__set_site_availability(domain_row.site_id, domain_row.site.value)
        else:
            for domain_row in domain_rows.all():
                self.__set_site_availability(domain_row.site_id, domain_row.site.value)
            
        return True

    def update_sites_authority(self):
        """
        Complite sites base authority info like pagerank, dmoz, yaca
        """
        if self.paramsQuery:
            id = self.paramsQuery.get('site_id', None)
            if id:
                self.__set_site_authority(id)
                return True
        
        refresh_rate = Constant.objects.get_value('DOMAINS_REFRESH_RATE_DAYS')
        point_date = make_aware(datetime.today() - timedelta(days=refresh_rate), get_default_timezone())

        # # sites need to update availability
        domain_rows = Domain.objects.filter(Q(last_update__lte=point_date))

        debug = getattr(settings, 'DEBUG', False)
        if debug:
            for domain_row in domain_rows[:1]:
                self.__set_site_authority(domain_row.site_id, domain_row.site.value)
        else:
            for domain_row in domain_rows.all():
                self.__set_site_authority(domain_row.site_id, domain_row.site.value)
            
        return True

    def update_serp(self):
        """
        Update old SERP info
        """
        if self.paramsQuery:
            id = self.paramsQuery.get('keyword_id', None)
            if id:
                self.__set_serp(id, 'yandex')
                #self.__set_serp(id, 'google')
                return True

        refresh_rate = Constant.objects.get_value('SERP_REFRESH_RATE_DAYS')
        point_date = make_aware(datetime.today() - timedelta(days=refresh_rate), get_default_timezone())
        
        # sites need to update serp
        # first of all - get search for new keywords
        # if find keywords with out serp - need to update all keywords of this keyset
        # coz searches should be actual for whole keyset, not for particular keywords
        keywords_without_serp = KeyWord.objects.filter(searches__isnull=True)
        keysets_done = []
        for keyword in keywords_without_serp.all():
            for keyset in keyword.keysets.all():
                if keyset in keysets_done:
                    # if already done at previous steps
                    break
                else:
                    keywords_in_one_keyset = KeyWord.objects.filter(keysets__in=[keyset])
                    for keyword in keywords_in_one_keyset.all():
                        self.__set_serp(keyword.id, 'yandex', keyword.key)
                        #self.__set_serp(keyword.id, 'google', keyword.key)

                    keysets_done.append(keyset)

        # then - update serp for keywords that already has serp
        keywords_with_serp = KeyWord.objects.filter(searches__last_update__lte=point_date)
        keywords_with_serp = keywords_with_serp.exclude(searches__isnull=True)
        keywords_with_serp = keywords_with_serp.order_by('searches__last_update')

        keysets_done = []
        for keyword in keywords_with_serp.all():
            for keyset in keyword.keysets.all():
                if keyset in keysets_done:
                    # if already done at previous steps
                    break
                else:
                    keywords_in_one_keyset = KeyWord.objects.filter(keysets__in=[keyset]).order_by('searches__last_update')
                    for keyword in keywords_in_one_keyset.all():
                        self.__set_serp(keyword.id, 'yandex', keyword.key)
                        #self.__set_serp(keyword.id, 'google', keyword.key)

                    keysets_done.append(keyset)

        return True

    def complete_sites_info(self):
        """
        Update or set alaliability of domains
        Get all domains by last_update or domains have Null at alaliability
        """
        id = self.paramsQuery.get('site_id', None)
        if id:
            self.__set_site(id)
            return True
        
        complite_sites = [item.site.id for item in Domain.objects.all()]
        
        # sites need to complite
        sites = UnitProperty.objects.filter(ptype='Site').exclude(id__in=complite_sites)
        for site in sites.all():
            self.__set_site(site.id, site.value)
            
        return True
            
    def set_sites_info(self):
        """
        Set sites base info like whois, pagerank, dmoz, yaca
        Use ALL sites that exists in UnitProperty table
        """
        id = self.paramsQuery.get('site_id', None)
        if id:
            self.__set_site(id)
            return True
        
        # sites to set
        sites = UnitProperty.objects.filter(ptype='Site')
        for site in sites.all():
            self.__set_site(site.id, site.value)
            
        return True
    
    def __set_site(self, id, domain=None):
        """
        Set site info by id and domain
        gets domain if it's None
        """
        if not domain:
            domain = UnitProperty.objects.get(pk=id).value
        
        # sent tasksv to set information
        options = [domain, id]
        send_task("worker.get_domain_info", options)

    def __set_site_availability(self, id, domain=None):
        """
        Set site availability by id and domain
        gets domain if it's None
        """
        if not domain:
            domain = UnitProperty.objects.get(pk=id).value
        
        # sent task to set information
        options = [domain, id]
        send_task("worker.get_domain_availability", options)

    def __set_site_authority(self, id, domain=None):
        """
        Set site authority by id and domain
        gets domain if it's None
        """
        if not domain:
            domain = UnitProperty.objects.get(pk=id).value
        
        # sent task to set information
        options = [domain, id]
        send_task("worker.get_authority_info", options)
    
    def complete_serps(self):
        """
        Complite search engine results page (SERP)
        Do NOT touch keywords that
        already used for SERP(exists in searches table)
        """
        id = self.paramsQuery.get('keyword_id', None)
        if id:
            self.__set_serp(id, 'yandex')
            #self.__set_serp(id, 'google')
            return True
        
        complite_keywords = [item.key_word.id for item in Search.objects.all()]
        
        # keywords need to serp
        keywords = KeyWord.objects.exclude(id__in=complite_keywords)
        for keyword in keywords.all():
            self.__set_serp(keyword.id, 'yandex', keyword.key)
            #self.__set_serp(keyword.id, 'google', keyword.key)
            
        return True
    
    def set_serps(self):
        """
        Set search engine results page (SERP) of all search vendors
        Use all keywords
        """
        id = self.paramsQuery.get('keyword_id', None)
        if id:
            self.__set_serp(id, 'yandex')
            #self.__set_serp(id, 'google')
            return True
        
        # keywords need to serp
        keywords = KeyWord.objects.all()[:1]
        for keyword in keywords.all():
            self.__set_serp(keyword.id, 'yandex', keyword.key)
            #self.__set_serp(keyword.id, 'google', keyword.key)
            
        return True
    
    def complete_serps_yandex(self):
        """
        Complite search engine results page (SERP)
        Do NOT touch keywords that
        already used for SERP(exists in searches table)
        """
        id = self.paramsQuery.get('keyword_id', None)
        return self.__complete_serps_by_vendor('yandex', id)
    
    def complete_serps_google(self):
        """
        Complite search engine results page (SERP)
        Do NOT touch keywords that
        already used for SERP(exists in searches table)
        """
        id = self.paramsQuery.get('keyword_id', None)
        return self.__complete_serps_by_vendor('google', id)
    
    def set_serps_yandex(self):
        """
        Set search engine results page (SERP) of yandex search vendor
        Use all keywords
        """
        id = self.paramsQuery.get('keyword_id', None)
        return self.__set_serp_by_vendor('yandex', id)
    
    def set_serps_google(self):
        """
        Set search engine results page (SERP) of google search vendor
        Use all keywords
        """
        id = self.paramsQuery.get('keyword_id', None)
        return self.__set_serp_by_vendor('google', id)
    
    def __complete_serps_by_vendor(self, search_vendor_name, id):
        """
        Complete search engine results page (SERP) for selected search vendor
        Do NOT touch keywords that
        already used for SERP(exists in searches table)
        """
        if id:
            self.__set_serp(id, search_vendor_name)
            return True
        
        complite_keywords = [item.key_word.id for item in Search.objects.filter(search_vendor__name=search_vendor_name)]
        
        # keywords need to serp
        keywords = KeyWord.objects.exclude(id__in=complite_keywords)
        for keyword in keywords.all():
            self.__set_serp(keyword.id, search_vendor_name, keyword.key)
            
        return True
    
    def __set_serp_by_vendor(self, search_vendor_name, id=None):
        """
        Set search engine results page (SERP) for selected search vendor
        Use all keywords
        """
        if id:
            self.__set_serp(id, search_vendor_name)
            return True
        
        # keywords need to serp
        keywords = KeyWord.objects.all()
        for keyword in keywords.all():
            self.__set_serp(keyword.id, search_vendor_name, keyword.key)
            
        return True
        
    def __set_serp(self, id, search_vendor_name, keyword=None):
        """
        Set single SERP
        """
        if not keyword:
            keyword = KeyWord.objects.get(pk=id).key
            
        search_vendor = SearchVendor.objects.get(name=search_vendor_name)
        
        # sent tasks to set serp
        options = [keyword, search_vendor.id, id]
        
        if search_vendor_name == 'yandex':
            send_task("worker.get_serp_yandex", options)
            
        if search_vendor_name == 'google':
            send_task("worker.get_serp_google", options)
