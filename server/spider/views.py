# -*- coding: utf-8 -*-

import json
import random

from django.conf import settings

from models import Access
from api import Api
from tools.response import Response

@Response.api
def api(request, action): 
    api = Api(request)
    if action == 'complete_sites_info':
        return api.complete_sites_info()

    elif action == 'set_sites_info':
        return api.set_sites_info()

    elif action == 'update_sites_availability':
        return api.update_sites_availability()

    elif action == 'set_serps':
        return api.set_serps()

    elif action == 'complete_serps':
        return api.complete_serps()

    elif action == 'set_serps_yandex':
        return api.set_serps_yandex()

    elif action == 'complete_serps_yandex':
        return api.complete_serps_yandex()

@Response.external
def accesses(request, type):
    user_ip = request.META.get('REMOTE_ADDR')
    accesses = Access.objects.filter(type=type).filter(ip=user_ip).values('access')

    if accesses:
        ac_raw = random.choice(accesses)['access']
        access = json.loads(ac_raw)

        return access
    else:
        return ""

def testing(request):
    debug = getattr(settings, 'DEBUG', False)
    if debug:
        Api().update_serp()
        
        from uploading.factors import UnitCoeff
        UnitCoeff(1).factor_tic('volga.ru')
    else:
        pass
