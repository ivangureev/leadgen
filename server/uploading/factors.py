# -*- coding: utf-8 -*-

from urlparse import urlparse
import simplejson
from datetime import datetime
import pytz

from django.db import transaction
from django.conf import settings
from django.db import models
from django.db.models import Q, Max
from django.core.exceptions import ObjectDoesNotExist

from competitors.cachegenerator import CacheCrossSearchGenerator
from catalogparser.models import Category, Unit
from competitors.models import CacheCrossSearch
from keywords.models import KeySet
from spider.models import Domain
from models import Coeff
from constants.models import Constant

class Factors:
    """
    """
    def __init__(self):
        pass

    def set_coeffs(self, category=None):
        self.__clean_db(category)

        if category:
            units = Unit.objects.filter(categories__in=[category])
        else:
            units = Unit.objects.all()

        for unit in units:
            result = UnitCoeff(unit).get_rate()
            self.__save_to_db(unit, result)

    def __clean_db(self, category=None):
        """
        Cleaning previous Coeffs items by category from database
        """
        if category:
            Coeff.objects.filter(unit__categories__in=[category]).delete()
        else:
            Coeff.objects.all().delete()

    @transaction.commit_manually    
    def __save_to_db(self, unit, items):
        """
        Save to database
        """
        try:
            if items:
                for keyset, domains in items.items():
                    if domains:
                        for domain, properties in domains.items():
                            item = Coeff()
                            
                            item.unit = unit
                            item.key_set = keyset
                            item.domain = domain
                            item.coeff = properties['coeff']

                            item.save()   
        except Exception, exp: 
            transaction.rollback()
            raise Exception(exp)
        else:
            transaction.commit() 
            

class UnitCoeff:
    """
    Class to represent rate of coeffs of any units
    """
    def __init__(self, unit):
        self.unit = unit

    def get_rate(self):
        """
        Get total coefficient for all the factors
        """
        keysets = self.__get_max_count_keysets()

        today = datetime.utcnow()

        sites = self.unit.properties.filter(ptype='Site')
        domains = Domain.objects.filter(site__in=sites)
        domains = domains.filter(Q(available=True) | Q(Q(available=False) & Q(available__lt=today)))

        if len(sites) != len(domains):
            # raise Exception("Complite all domain's info before get factor's rate.")
            return 0

        # generate cache if need
        self.__generate_cross_search(keysets)

        rates = {}
        for keyset in keysets:
            rates[keyset] = {}
            for domain in domains:
                factors = {}

                factors['factor_third_level_domain'] = self.factor_third_level_domain(domain)
                factors['factor_not_top_1_10'] = self.factor_not_top_1_10(keyset, domain)
                factors['factor_top_11_30'] = self.factor_top_11_30(keyset, domain)
                factors['factor_already_top'] = self.factor_already_top(keyset, domain)
                factors['factor_yaca'] = self.factor_yaca(domain)
                factors['factor_tic'] = self.factor_tic(domain)
                factors['factor_pr'] = self.factor_pr(domain)

                if not factors['factor_not_top_1_10'][0] and not factors['factor_third_level_domain'][0]:
                    factors['factor_old_site'] = self.factor_old_site(domain)

                # getting total value by multiply every factor weights (using left folding)
                coeff = reduce(lambda x, y: x*y, [i[1] for i in factors.values()])

                rates[keyset][domain] = {
                    'coeff': coeff
                }  

        return rates    

    def factor_third_level_domain(self, domain):
        """
        Сайт с доменом стретьего уровня, например xxx.tui.ru
        """
        weight = float(Constant.objects.get_value('FACTOR_THIRD_LEVEL_DOMAIN'))

        if domain.creation_date is None:
            return (True, weight)
        else:
            return (False, 1)   

    def factor_old_site(self, domain):
        """
        У кого есть старый сайт
        """
        weight = float(Constant.objects.get_value('FACTOR_OLD_SITE'))
        years = int(Constant.objects.get_value('OLD_SITE_YEARS'))

        today = datetime.utcnow()
        years_ago = today.replace(year=today.year - years, tzinfo=pytz.UTC)

        if domain.creation_date < years_ago:
            return (True, weight)
        else:
            return (False, 1)

    def factor_yaca(self, domain):
        """
        Есть в Яндекс каталоге
        """
        weight = float(Constant.objects.get_value('FACTOR_YACA'))

        if domain.yaca:
            return (True, weight)
        else:
            return (False, 1)

    def factor_tic(self, domain):
        """
        Множитель по значению ТИЦ
        """
        weights = Constant.objects.get_value('FACTOR_TIC')
        
        if not weights:
            raise Exception("Empty factor value: {0}".format('FACTOR_TIC'))
        
        result = 1
        for key, weight in weights.items():
            interval = [int(item) for item in key.split(":")]
            if len(interval) != 2:
                raise Exception("Wrong factor key: {0}".format('FACTOR_TIC'))

            tic = domain.tic
            if interval[0] <= tic <= interval[1]:
                result = weight
                break
        return (True, result)

    def factor_pr(self, domain):
        """
        Множитель по значению PR
        """
        weights = Constant.objects.get_value('FACTOR_PR')
        
        if not weights:
            raise Exception("Empty factor value: {0}".format('FACTOR_PR'))
        
        result = 1
        for key, weight in weights.items():
            pr_sign = int(key)
            pr = domain.pr
            if pr_sign == pr:
                result = weight
                break
        return (True, result)

    def factor_not_top_1_10(self, keyset, domain):
        """
        Не в ТОП 10 по более чем 50% запросов от максимальной группы
        """
        weight = float(Constant.objects.get_value('FACTOR_TOP_1_10'))
        position_rate = self.__get_position_rate(1, 10, 50.0, keyset, domain)
        if position_rate is None:
            return (False, 1)

        if not position_rate:
            return (True, weight)
        else:
            return (False, 1)

    def factor_top_11_30(self, keyset, domain):
        """
        В ТОП 11 - 30 по более чем 60% запросов от максимальной группы
        """
        weight = float(Constant.objects.get_value('FACTOR_TOP_11_30'))
        position_rate = self.__get_position_rate(11, 30, 60.0, keyset, domain)
        if position_rate is None:
            return (False, 1)
        
        if position_rate:
            return (True, weight)
        else:
            return (False, 1)

    def factor_already_top(self, keyset, domain):
        """
        В ТОП 10 по более чем 80% запросов от максимальной группы
        """
        weight = float(Constant.objects.get_value('FACTOR_ALREADY_TOP'))
        position_rate = self.__get_position_rate(1, 10, 80.0, keyset, domain)
        if position_rate is None:
            return (False, 1)

        if position_rate:
            return (True, weight)
        else:
            return (False, 1)

    def __get_position_rate(self, top_min, top_max, percent, keyset, domain):
        positions = self.__get_positions(keyset, domain)
        if positions is None:
            return None

        total = 0
        for position in positions:
            if top_min <= position <= top_max:
                total += 1

        if total > ((keyset.keywords_count / 100.0) * percent):
            return True
        else:
            return False

    def __get_max_count_keysets(self):
        keysets = []

        categories = self.unit.categories
        for category in categories.all():
            keyset = KeySet.objects.filter(category=category)
            keyset = keyset.annotate(keywords_count=models.Count('keywords'))
            keyset = keyset.order_by('-keywords_count')[:1]

            if keyset:
                keysets.append(keyset[0])

        return keysets

    def __generate_cross_search(self, keysets):
        """
        Generate cache if need
        """
        for keyset in keysets:
            CacheCrossSearchGenerator().generate(keyset.id)

    def __get_positions(self, keyset, domain):
        keywords = CacheCrossSearch.objects.get_keywords(keyset, domain)
        positions = [item['position'].itervalues().next() for item in keywords]

        return positions