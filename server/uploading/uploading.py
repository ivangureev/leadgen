# -*- coding: utf-8 -*-

from __future__ import division
import cStringIO as StringIO
from datetime import date, datetime
import pytz
from functools import wraps
import simplejson
import json
import uuid

import trans
from django.conf import settings
from django.db import transaction
from django.db.models import Q, Max
from django.utils.dateformat import format
from celery.task.http import HttpDispatchTask, URL
from celery.task import task
from celery.execute import send_task

from keywords.models import KeySet, KeyWord
from spider.models import Domain
from competitors.models import CacheCrossSearch, SelectedCompetitor
from catalogparser.models import Category, Unit, UnitProperty
from models import Coeff, Upload, UploadProperty, Offer
from offer import PdfOffer, DocxOffer
from factors import Factors
from constants.models import Constant
import tasks


class Uploading(object):
    """
    Class to represent uploading units
    """
    def __init__(self):
        pass

    def upload(self, category):
        """
        Make uploading to database
        """

        # set coefficients for categorie's units
        Factors().set_coeffs(category)

        self.uploaded = dict(normal=0, young=0, without_site=0)
        
        self.category = category
        self.session = str(uuid.uuid4())
        self.step = self.__get_last_step() + 1

        self.uploaded['normal'] = self.__upload_normal(category=category, utype="Normal", step=self.step, session=self.session)
        self.uploaded['young'] = self.__upload_young(category=category, utype="Young", step=self.step, session=self.session)
        self.uploaded['without_site'] = self.__upload_without_site(category=category, utype="WithoutSite", step=self.step, session=self.session)
        
        self.send_to_queue()

    def send_to_queue(self):
        """
        Send last upload units to queue by Web hook celery
        """
        session = self.__get_last_session
        # if not session:
        #     raise Exception('There is nothing to send to queue.')

        uploads = Upload.objects.filter(session=session)
        for upload in uploads:
            offers = Offer.objects.filter(upload=upload)
            # offers_list = [{'type': o.type, 'domain': o.domain.site.value, 'file': o.data_base64}
            #     for o in offers if o.domain.site]

            offers_list = list()
            for o in offers.all():
                item = dict(type=o.type.lower(), file=o.data_base64)

                if o.domain:
                    item['domain'] = o.domain.site.value
                    item['domain_latin'] = o.domain.site.value.encode('trans')
                else:
                    item['domain'] = None

                offers_list.append(item)

            unit_properties = dict(
                name=upload.unit.name,
                address=self.__get_unit_prop(upload.unit, 'Address'),
                email=self.__get_unit_props(upload.unit, 'Email'),
                phone=self.__get_unit_props(upload.unit, 'Phone'),
            )

            message = dict(
                upload_type=upload.get_utype_display(),
                unit_id=upload.unit.id,
                unit_props=unit_properties,
                category_id=upload.category.id,
                category_name=upload.category.name,
                price=float(upload.properties.get(ptype="Price").value),
                coeff=float(upload.properties.get(ptype="Coeff").value),
                comments=upload.properties.get(ptype="TOPs").value,
                date=format(upload.last_update, 'U'),
                offers=offers_list)

            message_json = simplejson.dumps(message)
            # self.__send_unit(message_json)

            # lid_id = tasks.send_units.delay(message=message_json)
            lid_id_async = tasks.send_units.apply_async(args=[message_json])
            # send_task("uploading.tasks.send_units", [message_json])

    def __get_count_to_upload(self, upload_type):
        c_normal = int(Constant.objects.get_value('UPLOAD_NORMAL'))
        c_young = int(Constant.objects.get_value('UPLOAD_YOUNG'))
        c_without_site = int(Constant.objects.get_value('UPLOAD_WITHOUT_SITE'))

        max_count = c_normal + c_young + c_without_site

        normal_cur = c_normal if self.uploaded['normal'] == None else min(c_normal, self.uploaded['normal'])
        young_cur = c_young if self.uploaded['young'] == None else min(c_young, self.uploaded['young'])
        without_site_cur = c_without_site if self.uploaded['without_site'] == None else min(c_without_site, self.uploaded['without_site'])

        if upload_type == 'normal':
            count = max_count - young_cur - without_site_cur
        elif upload_type == 'young':
            count = max_count - normal_cur - without_site_cur
        elif upload_type == 'without_site':
            count = max_count - young_cur - normal_cur

        return count

    def __get_unit_prop(self, unit, ptype):
        props = unit.properties.filter(ptype=ptype).values()
        props_srt = ",".join([i['value'] for i in props])

        return props_srt

    def __get_unit_props(self, unit, ptype):
        props = unit.properties.filter(ptype=ptype).values()
        props_normal = [i['value'] for i in props]

        return props_normal

    def __get_last_step(self):
        # step = Upload.objects.all().aggregate(Max('step'))
        step = Upload.objects.filter(category=self.category).aggregate(Max('step'))
        if step['step__max']:
            return step['step__max']
        else:
            return 0

    def __get_last_session(self):
        if 'session' in self.__dict__.keys():
            session = self.session
        else:
            last_uploaded = Upload.objects.order_by('-last_update')[:1]
            if not last_uploaded:
                return 0
            session = last_uploaded[0].session
        
        return session

    # deprecated
    def __send_unit(self, message):
        url_to_send = Constant.objects.get_value('UPLOAD_URL')
        res = HttpDispatchTask.delay(
            url=url_to_send,
            method='POST', message=message)   

    def upload_routine(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            # get `self`
            self = args[0].__class__

            utype = kwargs['utype']
            del kwargs['utype']

            session = kwargs['session']
            del kwargs['session']

            step = kwargs['step']
            del kwargs['step']

            category = kwargs['category']
            kwargs['unit_ids_already_uploaded'] = [i["unit__id"] for i in 
                Upload.objects.filter(category=category).values('unit__id')]

            kwargs['unit_ids_with_site'] = [i["unit__id"] for i in 
                UnitProperty.objects.filter(ptype='Site').values('unit__id')]
            
            count, units = func(*args, **kwargs)

            for unit in units[:count]:
                pdfs = PdfOffer().make_offers(unit, category)
                docxs = DocxOffer().make_offers(unit, category)

                offers = {
                    'Pdf': pdfs,
                    'Docx': docxs
                }

                props = self._Uploading__get_props(self(), unit, category)
                
                # call hidden function
                self._Uploading__save_to_db(self(), unit, category, offers, props, utype, step, session)

            return len(units[:count])

        return wrapper

    @upload_routine
    def __upload_without_site(self, category, unit_ids_already_uploaded, unit_ids_with_site):
        """
        Make uploading to database for units without sites
        """
        count = self.__get_count_to_upload('without_site')

        units = Unit.objects.filter(categories=category)
        units = units.exclude(pk__in=unit_ids_with_site)
        # units = units.exclude(pk__in=unit_ids_already_uploaded)
        units = units.order_by('name')

        return (count, units)

    @upload_routine
    def __upload_young(self, category, unit_ids_already_uploaded, unit_ids_with_site):
        """
        Make uploading to database for units with young sites
        """
        count = self.__get_count_to_upload('young')

        young_month = Constant.objects.get_value('YOUNG_SITE_MONTH')
        today = datetime.utcnow()
        young_date_min = today.replace(month=today.month-young_month, tzinfo=pytz.UTC)

        units = Unit.objects.filter(categories=category)
        units = units.filter(properties__domain__creation_date__gte=young_date_min)
        units = units.filter(pk__in=unit_ids_with_site)
        units = units.filter(properties__ptype='Site').filter(Q(properties__domain__available=True) | Q(Q(properties__domain__available=False) & Q(properties__domain__available__lt=today)))
        # units = units.exclude(pk__in=unit_ids_already_uploaded)
        units = units.annotate(max_coeff=Max('coeffs__coeff'))
        units = units.order_by('-max_coeff')

        return (count, units)

    @upload_routine
    def __upload_normal(self, category, unit_ids_already_uploaded, unit_ids_with_site):
        """
        Make uploading to database for units with nornal sites
        """
        count = self.__get_count_to_upload('normal')

        today = datetime.utcnow()

        units = Unit.objects.filter(categories=category)
        units = units.filter(pk__in=unit_ids_with_site)
        units = units.filter(properties__ptype='Site').filter(Q(properties__domain__available=True) | Q(Q(properties__domain__available=False) & Q(properties__domain__available__lt=today)))
        # units = units.exclude(pk__in=unit_ids_already_uploaded)
        units = units.annotate(max_coeff=Max('coeffs__coeff'))
        units = units.order_by('-max_coeff')

        return (count, units)

    def __get_props(self, unit, category):
        """
        Get uploading props such a price, TOPs and ect.
        """
        #price
        price = KeySet.objects.filter(category=category).aggregate(max=Max('price'))['max']

        #coeff
        coeff = unit.coeffs.aggregate(coeff=Max('coeff'))['coeff']

        if coeff:
            coeff = round(coeff, 2)
        else:
            coeff = 0

        #tops
        all_tops = """""".decode(encoding="utf-8")
        sites = unit.properties.filter(ptype='Site')
        domains = Domain.objects.filter(site__in=sites)

        for domain in domains:
            keysets = KeySet.objects.filter(category=category)
            domain_tops = """<b>{0}</b> <br><br>""".decode(encoding="utf-8").format(domain.site.value)
            for keyset in keysets:

                mixed_positions = CacheCrossSearch.objects.get_keywords(keyset, domain)
                keyword_count = KeyWord.objects.filter(keysets=keyset).count()

                if not mixed_positions:
                    break

                positions = [i['position'].itervalues().next() for i in mixed_positions]
                top = dict(top_10=0, top_20=0, top_30=0, top_100=0)

                # get result persent like: 35.67 or 10.00
                top['top_10'] = round(len(filter(lambda p: 1 <= p <= 10, positions)) / float(keyword_count) * 100, 2)
                top['top_20'] = round(len(filter(lambda p: 1 <= p <= 20, positions)) / float(keyword_count) * 100, 2)
                top['top_30'] = round(len(filter(lambda p: 1 <= p <= 30, positions)) / float(keyword_count) * 100, 2)
                top['top_100'] = round(len(filter(lambda p: 1 <= p <= 100, positions)) / float(keyword_count) * 100, 2)

                tops = """
Набор: <b>{0}</b>, бюджет: <b>{1}</b> р.<br>
ТОП 10: {2}%<br>
ТОП 20: {3}%<br>
ТОП 30: {4}%<br>
ТОП 100: {5}%<br><br>
""".decode(encoding="utf-8").format(keyset.name, keyset.price, top['top_10'], top['top_20'], top['top_30'], top['top_100'])

                domain_tops += tops
            all_tops += domain_tops

        return dict(price=price, coeff=coeff, tops=all_tops)


    @transaction.commit_manually    
    def __save_to_db(self, unit, category, offers, props, utype, step, session):
        """
        Save to database
        """
        try:
            #save upload
            upload = Upload(unit=unit, category=category, utype=utype, step=step, session=session)
            upload.save()

            #save upload properties
            for key, value in props.items():
                if key == 'price':
                    upload_props = UploadProperty(upload=upload, ptype="Price", value=value)
                elif key == 'coeff':
                    upload_props = UploadProperty(upload=upload, ptype="Coeff", value=value)
                elif key == 'tops':
                    upload_props = UploadProperty(upload=upload, ptype="TOPs", value=value)
                
                upload_props.save()

            #save upload offers
            for type, offer_set in offers.items():
                if not isinstance(offer_set, dict):
                    new_offer = Offer(upload=upload, type=type, data=offer_set)
                    new_offer.save()
                else:
                    for domain, offer in offer_set.items():
                        new_offer = Offer(upload=upload, domain=domain, type=type, data=offer)
                        new_offer.save()
        except Exception, exp: 
            transaction.rollback()
            # raise Exception(exp)
        else:
            transaction.commit() 
            