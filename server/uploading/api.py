# -*- coding: utf-8 -*-

from celery.task import task
from celery.execute import send_task

from tools.baseapi import BaseApi
from factors import Factors
from uploading import Uploading
from catalogparser.models import Category

class Api(BaseApi):
    """
    Class contains actions that parse searching sites
    or use third partie's services
    """
    def __init__(self, category_id=None, *args, **kwargs):
        super(Api, self).__init__(*args, **kwargs)

        if category_id:
            self.category_id = category_id
        elif self.paramsQuery:
            category_id = self.paramsQuery.get('category_id', None)
        else:
            self.category_id = None

    def upload(self):
        """
        Upload units in by category.
        1) Set sites coefficients
        2) Upload units to db
        3) Send just uploaded units to queue(for Bitrix)
        """

        id = self.category_id
        if not id:
            raise Exception("Missing required param 'category_id' for uploading")

        category = Category.objects.get(pk=id)
        Uploading().upload(category)
            
        return True
            
    def set_coeffs(self):
        """
        Set coeffs for all sites
        """
        Factors().set_coeffs()
            
        return True

    def send_to_queue(self):
        """
        Send last upload units to queue by Web hook celery
        """
        Uploading().send_to_queue()
            
        return True