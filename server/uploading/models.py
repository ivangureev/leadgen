# -*- coding: utf-8 -*-

import base64

from django.db import models

from catalogparser.models import Unit, Category
from spider.models import Domain
from keywords.models import KeySet

class Base64Field(models.TextField):
    """
    Field to store BLOB in TextField
    """
    def contribute_to_class(self, cls, name):
        if self.db_column is None:
            self.db_column = name
        self.field_name = name + '_base64'
        super(Base64Field, self).contribute_to_class(cls, self.field_name)
        setattr(cls, name, property(self.get_data, self.set_data))

    def get_data(self, obj):
        return base64.decodestring(getattr(obj, self.field_name))

    def set_data(self, obj, data):
        setattr(obj, self.field_name, base64.encodestring(data))

class Coeff(models.Model):
    """
    Class to represent total coefficients of every keyset, every domian of every unit
    """
    unit = models.ForeignKey(Unit, related_name="coeffs")
    key_set = models.ForeignKey(KeySet, related_name="coeffs")
    domain = models.ForeignKey(Domain, related_name="coeffs")
    coeff = models.FloatField()
    last_update = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return self.coeff
    
    class Meta:
        db_table = "s_coeffs"

class Upload(models.Model):
    """
    Class to represent uploads
    """
    UTYPE_CHOICES = (
        ('Normal', 'Обычный сайт'),
        ('Young', 'Молодой сайт'),
        ('WithoutSite', 'Нет сайта'),
    )
    unit = models.ForeignKey(Unit, related_name="uploads")
    utype = models.CharField(max_length=50, choices=UTYPE_CHOICES)
    category = models.ForeignKey(Category, related_name="uploads")
    step = models.PositiveIntegerField()
    session = models.CharField(max_length=36)
    last_update = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return self.unit.name
    
    class Meta:
        db_table = "s_uploads"

class UploadProperty(models.Model):
    """
    Class to represent property of uploads
    """
    PTYPE_CHOICES = (
        ('Price', 'Цена максимального набора'),
        ('Coeff', 'Коэффициент привлекательности'),
        ('TOPs', 'Топы'),
    )
    upload = models.ForeignKey(Upload, related_name="properties")
    ptype = models.CharField(max_length=50, choices=PTYPE_CHOICES)
    value = models.TextField(db_index=True, null=True)
    
    def __unicode__(self):
        return self.value
    
    class Meta:
        db_table = "s_uploads_props"

class Offer(models.Model):
    """
    Class to represent offers
    """

    TYPE_CHOICES = (
        ('Pdf', 'Pdf'),
        ('Docx', 'Docx'),
    )

    upload = models.ForeignKey(Upload, related_name="offers") 
    domain = models.ForeignKey(Domain, related_name="offers", null=True) 
    type = models.CharField(max_length=50, choices=TYPE_CHOICES)
    data = Base64Field()
    last_update = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return self.domain.site.value
    
    class Meta:
        db_table = "s_offers"