# -*- coding: utf-8 -*-

import tasks
# from api import Api
from tools.rest import RESTHandler
from tools.response import Response
from models import Upload

@Response.standart
def uploads(request, id):
    handler = RESTHandler(
        request=request,
        model=Upload,
        element_id=id)
    
    if handler.format == 'GET':
        return handler.read(
            extra_fields=[{
                'field_name': 'domains', 
                'getter': lambda p: ",".join([i['value'] for i in p.unit.properties.filter(ptype='Site').values('value')]), 
                'order_by': ''
            },{
                'field_name': 'unit_name', 
                'getter': lambda p: p.unit.name, 
                'order_by': 'unit__name'
            }])   
    # elif handler.format == 'POST':
    #     return handler.create()
    # elif handler.format == 'PUT':
    #     return handler.update()
    # elif handler.format == 'DELETE':
    #     return handler.delete()
    else:
        raise Exception("Undefined method")

@Response.api
def api(request, action): 
    if action == 'upload':
        # for asynchronous call (task)
        category_id = request.POST.get('category_id', None)
        tasks.upload.apply_async(args=[category_id])

    elif action == 'set_coeffs':
        # for asynchronous call (task)
        tasks.set_coeffs.apply_async()

    elif action == 'send_to_queue':
        # for asynchronous call (task)
        tasks.send_to_queue.apply_async()

def get_test_pdf(request):
    from django.http import HttpResponse
    from django.template.loader import render_to_string
    from django.template import RequestContext
    from django.template.loader import get_template
    from django.template import Context
    from django.conf import settings
    import ho.pisa as pisa
    import cStringIO as StringIO
    import cgi

    # # test
    # from models import Offer
    # return HttpResponse(Offer.objects.filter(pk=1)[0].data, mimetype='application/pdf')

    context_dict = dict(pagesize='A4')
    context_dict['path'] = getattr(settings, 'TEMPLATE_STATIC_PATH', "")

    from keywords.models import KeySet
    from spider.models import Domain
    from competitors.models import CacheCrossSearch, SelectedCompetitor
    from catalogparser.models import Category, Unit, UnitProperty
    from models import Coeff, Upload, UploadProperty, Offer
    from datetime import date, datetime

    unit = Unit.objects.get(pk=140)
    category = Category.objects.get(pk=24)

    sites = unit.properties.filter(ptype='Site')
    domains = Domain.objects.filter(site__in=sites)
    domain = domains[0]

    info = {}
    info["name"] = unit.name
    info["date"] = date.today()
    info["site"] = domain.site.value

    keysets = KeySet.objects.filter(category=category)
    info["keysets"] = keysets.all()

    for keyset in info["keysets"]:
        keyset.keywords_values = [k.key for k in keyset.keywords.all()]
        keyset.count = len(keyset.keywords_values)

    info["competitors"] = {}
    for keyset in keysets:
        info["competitors"] = SelectedCompetitor.objects.filter(key_set=keyset).order_by('percentage').all()

    context_dict['info'] = info

    # context_dict['info'] = {
    #     'site': 'site.com',
    #     'keysets': [
    #         {
    #             'name': 'Обычный', 
    #             'price': 2500,
    #             'count': 5,
    #             'keywords': ['Слово1', 'Слово2', 'Слово3', 'Слово4', 'Слово5']
    #         },
    #         {
    #             'name': 'Дорогой', 
    #             'price': 5000,
    #             'count': 15,
    #             'keywords': ['Слово1', 'Слово2', 'Слово3', 'Слово4', 'Слово5', 'Слово1', 'Слово2', 'Слово3', 'Слово4', 'Слово5', 'Слово1', 'Слово2', 'Слово3', 'Слово4', 'Слово5']
    #         }
    #     ],
    #     'competitors': [{'competitor_domain': 'www.google.ru'},{'competitor_domain': 'www.google.ru'},{'competitor_domain': 'www.google.ru'},{'competitor_domain': 'www.google.ru'},{'competitor_domain': 'www.google.ru'},]
    # }

    template = get_template('offer_complite.html')
    context = Context(context_dict)
    html  = template.render(context)
    result = StringIO.StringIO()

    # html  = render_to_string('offer_template.html', { 'pagesize' : 'A4', }, context_instance=RequestContext(request))
    # result = StringIO.StringIO()
    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), result, encoding='UTF-8')
    if not pdf.err:
        return HttpResponse(result.getvalue(), mimetype='application/pdf')
        # return HttpResponse(result.getvalue(), mimetype='text/plain')
    return HttpResponse('Gremlins ate your pdf! %s' % cgi.escape(html))