# -*- coding: utf-8 -*-

import urllib2
import urllib
import simplejson
import logging

from celery import task
from celery.signals import task_failure
from django.conf import settings

from api import Api
from constants.models import Constant

def on_retry_handler(self, exc, task_id, args, kwargs, einfo):
    logger = logging.getLogger('celery_server')
    logger.warning('Task retry...', exc_info=True, extra={
        'exception': exc,
        'position arguments': args,
        'key arguments': kwargs,
        'task_id': task_id,
        'einfo': einfo
    })

@task(on_retry = on_retry_handler)
def send_units(message, *args, **kwargs):
    try:
        url_to_send = Constant.objects.get_value('UPLOAD_URL')
        req = urllib2.Request(url_to_send, urllib.urlencode(dict(message=message.encode('utf-8'))))
        response = urllib2.urlopen(req)
        
        return simplejson.loads(response.read())
    except Exception, exc:
        raise send_units.retry(exc=exc, countdown=2, max_retries=3)

@task(on_retry = on_retry_handler)
def upload(category_id, *args, **kwargs):
    try:
        api = Api(category_id=category_id)
        return api.upload()
    except Exception, exc:
        raise upload.retry(exc=exc, countdown=2, max_retries=3)

@task(on_retry = on_retry_handler)
def set_coeffs(*args, **kwargs):
    try:
        api = Api()
        return api.set_coeffs()
    except Exception, exc:
        raise set_coeffs.retry(exc=exc, countdown=2, max_retries=3)

@task(on_retry = on_retry_handler)
def send_to_queue(*args, **kwargs):
    try:
        api = Api()
        return api.send_to_queue()
    except Exception, exc:
        raise send_to_queue.retry(exc=exc, countdown=2, max_retries=3)