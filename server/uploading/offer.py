# -*- coding: utf-8 -*-

from abc import ABCMeta, abstractmethod, abstractproperty
import cStringIO as StringIO
from datetime import date, datetime
import pytz

import ho.pisa as pisa
import docx
from django.conf import settings
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from cgi import escape

from keywords.models import KeySet
from spider.models import Domain
from competitors.models import CacheCrossSearch, SelectedCompetitor
from constants.models import Constant
from tools.common import Common as CommonTools

class BaseOffer(object):
    """
    Base class to represent offers generating
    """

    __metaclass__ = ABCMeta

    def make_offers(self, unit, category):
        """
        Get offers for every unit for every domain's of unit.
        One offer - one domain
        """
        sites = unit.properties.filter(ptype='Site')

        if not sites:
            # Unit without site
            return self.__offer_without_site(unit, category)
        else:   
            domains = Domain.objects.filter(site__in=sites)

            if len(sites) != len(domains):
                # raise Exception("Complete all domain's info before get offers.")
                return

            pdfs = {}
            for domain in domains:
                if self.__young_site(domain):
                    # Unit with young site
                    pdfs[domain] = self.__offer_complite(unit, category, domain)
                else:
                     # Unit with normal site
                    pdfs[domain] = self.__offer_new_site(unit, category, domain)
                # return pdfs[domain]

        return pdfs

    def __young_site(self, domain):
        young_month = Constant.objects.get_value('YOUNG_SITE_MONTH')

        today = datetime.utcnow()
        young_date_min = today.replace(month=today.month-young_month, tzinfo=pytz.UTC)

        if not domain.creation_date:
            return False

        if domain.creation_date < young_date_min:
            return True
        else:
            return False

    def __get_competitors(self, keysets, domain=None):
        competitors = list()
        if domain:
            possible_domains = CommonTools.get_possible_domains(domain)
        else:
            possible_domains = list()
        for keyset in keysets:
            keysets_competitors_raw = SelectedCompetitor.objects.filter(key_set=keyset).order_by('percentage').all()

            for comp in keysets_competitors_raw:
                has = False
                for d in possible_domains:
                    if d in comp.competitor_domain:
                        has = True

                if not has and comp.competitor_domain not in [o.competitor_domain for o in competitors]:
                    competitors.append(comp)

        return competitors

        # reduce(lambda pos_domain: map(lambda competitor: pos_domain in [o. in competitor.competitor_domain], keysets_competitors), 

    def __offer_complite(self, unit, category, domain):
        info = {}
        info["name"] = unit.name
        info["date"] = date.today()
        info["site"] = domain.site.value

        keysets = KeySet.objects.filter(category=category)
        info["keysets"] = keysets.all()

        for keyset in info["keysets"]:
            keyset.keywords_values = [k.key for k in keyset.keywords.all()]
            keyset.count = len(keyset.keywords_values)

        info["competitors"] = self.__get_competitors(keysets, domain)

        return self.render_offer_complite(info)

    def __offer_new_site(self, unit, category, domain):
        info = {}
        info["name"] = unit.name
        info["date"] = date.today()
        info["site"] = domain.site.value

        keysets = KeySet.objects.filter(category=category)
        info["keysets"] = keysets.all()

        for keyset in info["keysets"]:
            keyset.keywords_values = [k.key for k in keyset.keywords.all()]
            keyset.count = len(keyset.keywords_values)

        info["competitors"] = self.__get_competitors(keysets, domain)

        return self.render_offer_new_site(info)

    def __offer_without_site(self, unit, category):
        info = {}
        info["name"] = unit.name
        info["date"] = date.today()

        keysets = KeySet.objects.filter(category=category)
        info["keysets"] = keysets.all()

        for keyset in info["keysets"]:
            keyset.keywords_values = [k.key for k in keyset.keywords.all()]
            keyset.count = len(keyset.keywords_values)

        info["competitors"] = self.__get_competitors(keysets)

        return self.render_offer_without_site(info)

    @abstractmethod
    def render_offer_complite(self, info):
        pass

    @abstractmethod
    def render_offer_new_site(self, info):
        pass

    @abstractmethod
    def render_offer_without_site(self, info):
        pass


class PdfOffer(BaseOffer):
    """
    Class to represent pdf generating
    """
    def __init__(self):
        pass

    def render_offer_complite(self, info):
        return self.__render(
            'offer_complite.html', {
                'pagesize':'A4',
                'info': info
            }
        )

    def render_offer_new_site(self, info):
        return self.__render(
            'offer_new_site.html', {
                'pagesize':'A4',
                'info': info
            }
        )

    def render_offer_without_site(self, info):
        return self.__render(
            'offer_without_site.html', {
                'pagesize':'A4',
                'info': info
            }
        )

    def __render(self, template_src, context_dict):
        context_dict['path'] = getattr(settings, 'TEMPLATE_STATIC_PATH', "")

        template = get_template(template_src)
        context = Context(context_dict)
        html  = template.render(context)
        result = StringIO.StringIO()

        pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), result, encoding='UTF-8')

        if pdf.err:
            raise Exception("Error while create PDF: {0}".format(html))

        # return HttpResponse(result.getvalue(), mimetype='application/pdf')
        return result.getvalue()


class DocxOffer(BaseOffer):
    """
    Class to represent docx generating
    """
    def __init__(self):
        pass

    def render_offer_complite(self, info):
        debug = getattr(settings, 'DEBUG', False)
        if debug:
            pass
            # Пока вообще не работает


            # # TESTING
            # # Default set of relationshipships - these are the minimum components of a document
            # relationships = docx.relationshiplist()

            # # Make a new document tree - this is the main part of a Word document
            # document = docx.newdocument()
            
            # # This xpath location is where most interesting content lives 
            # docbody = document.xpath('/w:document/w:body', namespaces=docx.nsprefixes)[0]
            
            # # Append two headings and a paragraph
            # docbody.append(docx.heading('''Welcome to Python's docx module''',1)  )   
            # docbody.append(docx.paragraph('Привет!! The module:'.decode('utf-8')))

            # # Add a pagebreak
            # docbody.append(docx.pagebreak(type='page', orient='portrait'))


            # result = StringIO.StringIO()

            # # Create our properties, contenttypes, and other support files
            # coreprops = docx.coreproperties(title='Python docx demo',subject='A practical example of making docx from Python',creator='Mike MacCana',keywords=['python','Office Open XML','Word'])
            # appprops = docx.appproperties()
            # contenttypes = docx.contenttypes()
            # websettings = docx.websettings()
            # wordrelationships = docx.wordrelationships(relationships)

            # # Save our document
            # docx.savedocx(document,coreprops,appprops,contenttypes,websettings,wordrelationships,result)
            # return result.getvalue()
        else:
            return ""

    def render_offer_new_site(self, info):
        return ""

    def render_offer_without_site(self, info):
        return ""
