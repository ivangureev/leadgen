# -*- coding: utf-8 -*-
from __future__ import with_statement

import os.path
import inspect

from fabric.colors import *
from fabric.api import *
from fabric.contrib.console import confirm
from fabtools import require, python_distribute, python, deb, service
from fabtools.files import *

env.hosts = [
    'ivan@192.168.1.254',
    'ivan@78.47.206.92'
]
env.passwords = {
    'ivan@192.168.1.254': 'lemonpost',
    'ivan@78.47.206.92': 'lemonpost'
}
env.forward_agent = True

env.roledefs = {
    "local": ['192.168.1.254'],
    "remote":['78.47.206.92']
}

env.project_path = 'projects/node'
env.git_url = 'git@bitbucket.org:varche1/salesystem.git'
env.git_branch = 'worker'
env.supervisord_conf = 'supervisord.conf'
env.celery_conf = 'celeryconfig.py'

env.worker_conf = {
    'local': {
        'SERVER_URL': '176.9.36.48',
        'SENTRY_DSN': 'http:\/\/faf97f22b87f4352847ea09d542376d2:ec67e77c3f184f3eac763d5b0ad35a48@logs.webpp.ru\/2',
        'SITE_URL': 'http:\/\/176.9.36.48',
        'SITE_ACCESS': 'admin:que5t10n'
    },
    'remote': {
        'SERVER_URL': '176.9.36.48',
        'SENTRY_DSN': 'http:\/\/faf97f22b87f4352847ea09d542376d2:ec67e77c3f184f3eac763d5b0ad35a48@logs.webpp.ru\/2',
        'SITE_URL': 'http:\/\/176.9.36.48',
        'SITE_ACCESS': 'admin:que5t10n'
    }
}

env.deb_packages = [
    'python-setuptools',
    'python-dev',
    'build-essential',
    'libxml2-dev',
    'libxslt-dev',
    'curl',
    'libcurl3-dev',
    'libmysqlclient-dev',
    'python-mysqldb',
    'git'
]

env.pip_packages = [
    'celery',
    'simplejson',
    'lxml',
    'grab',
    'raven',
    'sqlalchemy',
    'MySQL-python',
    'pycurl'
]

def update_system():
    # update system
    deb.update_index()
    deb.upgrade()

def setup_system_env():
    try:
        update_system()

        pkg_list = [pkg for pkg in env.deb_packages if not deb.is_installed(pkg)]
        warn(pkg_list)
        if pkg_list:
            deb.install(pkg_list)

        # install pip
        python_distribute.install('pip', use_sudo=True)

        # install supervisord
        python_distribute.install('supervisor', use_sudo=True)

        # install 
        python.install('virtualenv', use_sudo=True)

        # create directory
        if not is_dir(env.project_path):
            run("mkdir -p {path}".format(path=env.project_path))
    except Exception, e:
        raise Exception("Error at {func_name}: {error}".format(inspect.stack()[1][3], e))

@with_settings(cwd=env.project_path)
def setup_python_env():
    try:
        if python.is_installed('virtualenv'):
            if not is_dir('env'):
                run('virtualenv --no-site-packages env')

            with prefix("source env/bin/activate"):
        
                pkg_list = [pkg for pkg in env.pip_packages if not python.is_installed(pkg)]
                if pkg_list:
                    python.install(pkg_list)

    except Exception, e:
        raise Exception("Error at {func_name}: {error}".format(inspect.stack()[1][3], e))

# @roles('local')
# @with_settings(cwd=env.project_path)
# def set_celery_settings():
    # run("sed -i 's/SERVER_URL = \(.*\)/SERVER_URL = \"{value}\"/g' {file}".format(value=env.worker_conf['local']['SERVER_URL'], file=env.celery_conf))
    # run("sed -i 's/SENTRY_DSN = \(.*\)/SENTRY_DSN = \"{value}\"/g' {file}".format(value=env.worker_conf['local']['SENTRY_DSN'], file=env.celery_conf))
    # run("sed -i 's/SITE_URL = \(.*\)/SITE_URL = \"{value}\"/g' {file}".format(value=env.worker_conf['local']['SITE_URL'], file=env.celery_conf))
    # run("sed -i 's/SITE_ACCESS = \(.*\)/SITE_ACCESS = \"{value}\"/g' {file}".format(value=env.worker_conf['local']['SITE_ACCESS'], file=env.celery_conf))

# @roles('remote')
# @with_settings(cwd=env.project_path)
# def set_celery_settings():
    # run("sed -i 's/SERVER_URL = \(.*\)/SERVER_URL = \"{value}\"/g' {file}".format(value=env.worker_conf['remote']['SERVER_URL'], file=env.celery_conf))
    # run("sed -i 's/SENTRY_DSN = \(.*\)/SENTRY_DSN = \"{value}\"/g' {file}".format(value=env.worker_conf['remote']['SENTRY_DSN'], file=env.celery_conf))
    # run("sed -i 's/SITE_URL = \(.*\)/SITE_URL = \"{value}\"/g' {file}".format(value=env.worker_conf['remote']['SITE_URL'], file=env.celery_conf))
    # run("sed -i 's/SITE_ACCESS = \(.*\)/SITE_ACCESS = \"{value}\"/g' {file}".format(value=env.worker_conf['remote']['SITE_ACCESS'], file=env.celery_conf))

@with_settings(cwd=env.project_path)
def set_celery_settings(role):
    run("sed -i 's/SERVER_URL = \(.*\)/SERVER_URL = \"{value}\"/g' {file}".format(value=env.worker_conf[role]['SERVER_URL'], file=env.celery_conf))
    run("sed -i 's/SENTRY_DSN = \(.*\)/SENTRY_DSN = \"{value}\"/g' {file}".format(value=env.worker_conf[role]['SENTRY_DSN'], file=env.celery_conf))
    run("sed -i 's/SITE_URL = \(.*\)/SITE_URL = \"{value}\"/g' {file}".format(value=env.worker_conf[role]['SITE_URL'], file=env.celery_conf))
    run("sed -i 's/SITE_ACCESS = \(.*\)/SITE_ACCESS = \"{value}\"/g' {file}".format(value=env.worker_conf[role]['SITE_ACCESS'], file=env.celery_conf))

@with_settings(cwd=env.project_path)
def get_sources(git_url, branch, destination):
    try:
        if not is_dir('.git'):
            with settings(cwd='~'):
                run("git clone {git_url} {destination}".format(git_url=git_url, destination=destination))
        
        # remove local changes
        with settings(hide('running', 'warnings'), warn_only=True):
            run("git stash")
            run("git stash drop")

        run("git pull")
        run('git checkout {branch}'.format(branch=branch))
        
        #set settings
        for role in env.roledefs:
            if env.host in env.roledefs.get(role, []):
                set_celery_settings(role)

        # log directory
        if not is_dir('logs'):
            run("mkdir -p {path}".format(path='logs'))
    except Exception, e:
        raise Exception("Error at {func_name}: {error}".format(inspect.stack()[1][3], e))

@with_settings(cwd=env.project_path)
def set_supervisord_settings():
    run("sed -i 's/WORKER_ID_FROM_FABRIC/{user}:{host}/g' {file}".format(user=env.user, host=env.host, file=env.supervisord_conf))

    modified_path = os.path.join('/home/', env.user, env.project_path).replace('/', '\/')
    run("sed -i 's/PATH_TO_PROJECT_FROM_FABRIC/{path}/g' {file}".format(path=modified_path, file=env.supervisord_conf))

def setup_supervisord():
    try:
        service = """description "supervisord_workers"
author "Ivan Gureev <ivangureev@gmail.com>"

start on runlevel [2345]
stop on runlevel [!2345]

env PROJECT_DIR={project_path}

respawn

pre-start script
    cd $PROJECT_DIR
end script

exec /usr/local/bin/supervisord --nodaemon --configuration {conf_path}
        """.format(
            project_path=os.path.join('/home/', env.user, env.project_path),
            conf_path=os.path.join('/home/', env.user, env.project_path, env.supervisord_conf))

        set_supervisord_settings()
        # init service under Upstart
        if not is_file('/etc/init/supervisord_workers.conf'):
            sudo("echo '{servise_content}' > /etc/init/supervisord_workers.conf".format(servise_content=service))
            sudo('start supervisord_workers')

    except Exception, e:
        raise Exception("Error at {func_name}: {error}".format(inspect.stack()[1][3], e))

def deploy():
    try:
        update_system()
        setup_system_env()
        get_sources(env.git_url, env.git_branch, env.project_path)
        setup_python_env()
        setup_supervisord()
    except Exception, e:
        red(str(e))

def reboot():
    try:
        sudo('reboot')
    except Exception, e:
        red(str(e))

def clean_servise():
    try:
        with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
            sudo('killall -r supervisord')
            sudo('update-rc.d -f supervisord_workers remove')
            sudo('rm /etc/init.d/supervisord_workers')
    except Exception, e:
        red(str(e))