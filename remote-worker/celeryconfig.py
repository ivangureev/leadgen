SERVER_URL = 'from deploy'
SITE_URL = 'from deploy'
SITE_ACCESS = 'from deploy'
SENTRY_DSN = 'from deploy'

# Broker settings
BROKER_URL = "amqp://celery:lemon@{server}:5672/celeryhost".format(server=SERVER_URL)

CELERY_REDIRECT_STDOUTS = False

# Backend settings
# echo enables verbose logging from SQLAlchemy.
CELERY_RESULT_ENGINE_OPTIONS = {"echo": True}

CELERY_RESULT_BACKEND = "database"
CELERY_RESULT_DBURI = "mysql://sas_user:lemur@{server}/sales_system".format(server=SERVER_URL)

# Additional settings
CELERY_IMPORTS = ("worker", "worker_base")

# Routing
# Current workers will listen this queue
CELERY_DEFAULT_QUEUE = 'getting_results_q'
# Task: queue to send
CELERY_ROUTES = {
    'spider.tasks.save_serp_yandex': {'queue': 'pushing_results_q'},
	'spider.tasks.save_domain_info': {'queue': 'pushing_results_q'},
	'spider.tasks.update_domain_info': {'queue': 'pushing_results_q'}}

# Max task executing time(soft)
CELERYD_TASK_SOFT_TIME_LIMIT = 120

# Max task executing time(hard)
CELERYD_TASK_TIME_LIMIT = 360

YANDEX_XML_REGION = 38
YANDEX_XML_COUNT = 100

import logging
from raven.handlers.logging import SentryHandler
from celery import signals

handler = SentryHandler(SENTRY_DSN)
logger = logging.getLogger('celery_worker')
logger.addHandler(handler)

from tools import Tools
try:
    YANDEX_XML_API = Tools().get_accesses('YANDEX_XML_API', SITE_URL, SITE_ACCESS)
    ROBOWHOIS_API = Tools().get_accesses('ROBOWHOIS_API', SITE_URL, SITE_ACCESS)
except Exception, e:
    logger.error('Config initialization error', exc_info=True, extra={
        'culprit': "Worker init",
        'exception': e
    })

def worker_ready_log(signal, **kw):
    sender = kw['sender']
    hostname = getattr(sender, 'hostname', 'Undefined worker')
    logger.info('Worker started', exc_info=True, extra={
        'culprit': hostname
    })

def task_prerun_log(sender, task_id, task, signal, args, kwargs, **kw):
    logger.info('Start task running...', exc_info=True, extra={
        'culprit': task.name,
        'position arguments': args,
        'key arguments': kwargs,
        'task_id': task_id
    })

def task_success_log(result, sender, signal, **kw):
    logger.info('Task success', exc_info=True, extra={
        'culprit': sender.name,
        'result': result
    })

def task_failure_log(exception, traceback, sender, task_id, signal, args, kwargs, einfo, **kw):
    logger.error('Task failure', exc_info=True, extra={
        'culprit': sender.name,
        'exception': exception,
        'traceback': traceback,
        'position arguments': args,
        'key arguments': kwargs,
        'task_id': task_id
    })

signals.task_prerun.connect(task_prerun_log)
signals.task_success.connect(task_success_log)
signals.task_failure.connect(task_failure_log)
signals.worker_ready.connect(worker_ready_log)