# -*- coding: utf-8 -*-

import logging

from celery import task
from celery.loaders.default import Loader
from celery.utils.log import get_task_logger
from celery.exceptions  import MaxRetriesExceededError, RetryTaskError

import worker_base
from internet.yandex import YandexSearchError

def on_retry_handler(self, exc, task_id, args, kwargs, einfo):
    logger = logging.getLogger('celery_worker')
    logger.warning('Task retry...', exc_info=True, extra={
        'exception': exc,
        'position arguments': args,
        'key arguments': kwargs,
        'task_id': task_id,
        'einfo': einfo
    })
    
@task(on_retry = on_retry_handler)
def get_domain_info(*args, **kwargs):
    try:
        return worker_base.get_domain_info(*args, **kwargs)
    except Exception, exc:
        raise get_domain_info.retry(exc=exc, countdown=2, max_retries=3)

@task(on_retry = on_retry_handler)
def get_authority_info(*args, **kwargs):
    try:
        return worker_base.get_authority_info(*args, **kwargs)
    except Exception, exc:
        raise get_authority_info.retry(exc=exc, countdown=2, max_retries=3)

@task(on_retry = on_retry_handler)
def get_domain_availability(*args, **kwargs):
    try:
        return worker_base.get_domain_availability(*args, **kwargs)
    except Exception, exc:
        raise get_domain_availability.retry(exc=exc, countdown=2, max_retries=3)

@task(on_retry = on_retry_handler)
def get_serp_yandex(*args, **kwargs):
    try:
        return worker_base.get_serp_yandex(*args, **kwargs)
    except MaxRetriesExceededError, exc:
        raise Exception("Yandex error - Code: {0}".format(exc))
    except YandexSearchError, exc:
        raise get_serp_yandex.retry(exc=exc, countdown=2, max_retries=10)
    except Exception, exc:
        raise get_serp_yandex.retry(exc=exc, countdown=2, max_retries=10)