# -*- coding: utf-8 -*-
import simplejson
import time

from celery import task
from celery.execute import send_task
from celery.loaders.default import Loader
from celery.exceptions import RetryTaskError, MaxRetriesExceededError

from internet.dmoz import Dmoz
from internet.google import Google
from internet.whois_info import Whois
from internet.yandex import Yandex, YandexSearch, YandexSearchError
from internet.availability import Availability

def get_domain_info(domain, site_id):
    result = {}

    config = Loader().read_configuration()

    result['whois'] = Whois(key=config['ROBOWHOIS_API']['key'], domain=domain).get_all()
    result['yaca'] = Yandex().get_yaca(domain)
    result['dmoz'] = Dmoz(domain).get_all()
    result['pagerank'] = Google().get_page_rank(domain)
    result['available'] = Availability(domain).get_availability()
    
    send_task("spider.tasks.save_domain_info", [result, site_id])
    
    return (domain, site_id, result)

def get_authority_info(domain, site_id):
    result = {}

    config = Loader().read_configuration()

    result['yaca'] = Yandex().get_yaca(domain)
    result['dmoz'] = Dmoz(domain).get_all()
    result['pagerank'] = Google().get_page_rank(domain)
    
    send_task("spider.tasks.update_domain_info", [result, site_id])
    
    return (domain, site_id, result)

def get_domain_availability(domain, site_id):
    result = {}

    config = Loader().read_configuration()

    result['available'] = Availability(domain).get_availability()
    
    send_task("spider.tasks.update_domain_info", [result, site_id])
    
    return (domain, site_id, result)

def get_serp_yandex(query, search_vendor_id, keyword_id):
    config = Loader().read_configuration()
    ya = YandexSearch(user=config['YANDEX_XML_API']['user'], \
        key=config['YANDEX_XML_API']['key'], \
        region=config['YANDEX_XML_REGION'], \
        countresults=config['YANDEX_XML_COUNT'])
    
    result = ya.search(query)
    result_json = simplejson.dumps(result)
    
    options = [result_json, search_vendor_id, keyword_id, config['YANDEX_XML_REGION'], config['YANDEX_XML_COUNT']]
    send_task("spider.tasks.save_serp_yandex", options)
    
    return (query, result_json, search_vendor_id, keyword_id)