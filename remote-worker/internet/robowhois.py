# -*- coding: utf-8 -*-

import urllib2
import random
import json
import re
from datetime import datetime

class AllCreditError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class RoboWhois:

    def __init__(self, api_keys):
        self.api_keys = api_keys
        self.username = random.choice(self.api_keys)
        self.password = "X"

    def __get_new_key(self):
        try:
            self.api_keys = filter(lambda a: a != self.username, self.api_keys)
            self.username = random.choice(self.api_keys)
        except IndexError, e:
            raise AllCreditError("All API keys became empty") 

    def account_abulity(self):
        template  = "http://api.robowhois.com/account"

        passman   = urllib2.HTTPPasswordMgrWithDefaultRealm()
        passman.add_password(None, "http://api.robowhois.com/", self.username, self.password)
        handler   = urllib2.HTTPBasicAuthHandler(passman)
        opener    = urllib2.build_opener(handler)
        request   = urllib2.Request(template)
        response  = opener.open(request)

        data = json.loads(response.read())

        if data['account']['credits_remaining'] > 0:
            return True
        else:
            return False

    def __get_date(self, str_date):
        try:
            if str_date is None:
                return None

            pattern = re.compile('(?P<year>\d+)\-(?P<month>\d+)\-(?P<day>\d+)T.*')
            result = pattern.findall(str_date)
            if result:
                year, month, day = result[0] 
                return datetime(year=int(year), month=int(month), day=int(day))
        except Exception, e:
            return None

    def whois(self, domain):
        try:
            template  = "http://api.robowhois.com/whois/%s/properties"

            passman   = urllib2.HTTPPasswordMgrWithDefaultRealm()
            passman.add_password(None, "http://api.robowhois.com/", self.username, self.password)
            handler   = urllib2.HTTPBasicAuthHandler(passman)
            opener    = urllib2.build_opener(handler)
            request   = urllib2.Request(template % domain)
            response  = opener.open(request)

            data = json.loads(response.read())

            props = data['response']['properties']

            return dict(
                creation_date = self.__get_date(props['created_on']),
                expiration_date = self.__get_date(props['expires_on']),
                registrar = props['registrar']['id'] if props['registrar'] and props['registrar']['id'] else None
            )
        except urllib2.HTTPError, e:
            self.__get_new_key()
            self.whois(domain)

        except AllCreditError, e:
            raise Exception(e.msg) 

        except Exception, e:
            raise Exception("RoboWhois service is not working - {0}".format(e)) 


# if __name__ == "__main__" :

#     domain = 'neotek.su'
#     # domain = 'jasminbride.ucoz.ru'

#     domain = unicode(domain, 'utf-8').encode('idna')

#     keys = [
#         "c00f9c9722a06ef359c891b391231875",
#         # "4d5bcc73318d5c5d4b20bdc903c052ee",
#         # "26a9450b74ccb9e09b23bfb0952f4e0d",
#         # "d975581fd5f8d5c0e5c74e14388217ce",
#         # "f26d6b872dc541140ec86019f0838933",
#         # "b0610293c3b4a75a576e131ad8157f2d",
#         # "8cb8325d9c9e221d4802a620fa1421ff",
#         # "81a6b15faa450ff425e32946a9b05cfb",
#         # "b08c364e6643dfb80305d636e23f5be3"
#     ]

#     robowhois = RoboWhois(keys)

#     print robowhois.whois(domain)
#     # print robowhois.account_abulity()