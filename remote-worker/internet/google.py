# -*- coding: utf-8 -*-

import httplib
from urlparse import urlparse
from urllib2 import Request, urlopen, URLError, HTTPError

from grab import Grab

class Google:
    """
    Class for manipulation with google.com servicies
    """
    def __init__(self):
        self.g = Grab()
        
        self.pr = None
     
    def get_page_rank(self, domain):
        """
        Gets google PageRank of domain
        """
        try:
            domain = unicode(domain).encode('idna')
            hostname = urlparse("http://{0}".format(domain)).hostname
            if hostname.split(".")[:1][0] == 'www':
                domain = ".".join(hostname.split(".")[1:])
            
            try:
                domain.encode('ascii')
            except Exception, e:
                return self.__get_pr_atypical(domain)
            else:
                # DOES NOT work with some domians, for example ('agromh.com')
                return self.__get_pr(domain)
        except Exception, e:
            self.pr = None

    def __get_pr_atypical(self, domain):
        """
        Get Google PageRank of сyrillic domain
        """
        # cheking URL
        url = u"http://pr-cy.ru/analysis/{0}".format(domain)
        url = urlparse(url)

        if not (url.netloc and url.scheme and url.path):
            raise Exception("URL {0} is malformed.".format(url.geturl()))

        self.g.go(url.geturl())

        try:
            # get PR
            query = '//*[@id="base"]/div[2]/div[2]/div[1]/b'
            pr_text = self.g.xpath(query)
            self.pr = int(pr_text.text.split('/')[0])
        except ValueError, e:
            self.pr = 0
        except IndexError, e:
            raise Exception("Can't get Google PR: {0}".format(e))
        finally:
            return self.pr

    def __get_pr(self, domain):
        """
        Get Google PageRank of normal domain
        """
        prhost = u'toolbarqueries.google.com'
        prpath = u'/tbr?client=navclient-auto&ch=%s&features=Rank&q=info:%s'
        conn = httplib.HTTPConnection(prhost)
        hash = self.__get_hash(domain)
        path = prpath % (hash, domain)
        conn.request("GET", path)
        response = conn.getresponse()
        data = response.read()
        conn.close()
        
        return int(data.split(":")[-1])

    def __get_hash(self, query):
        SEED = "Mining PageRank is AGAINST GOOGLE'S TERMS OF SERVICE. Yes, I'm talking to you, scammer."
        Result = 0x01020345
        for i in range(len(query)) :
            Result ^= ord(SEED[i%len(SEED)]) ^ ord(query[i])
            Result = Result >> 23 | Result << 9
            Result &= 0xffffffff
        return '8%x' % Result
