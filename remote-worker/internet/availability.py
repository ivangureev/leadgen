# -*- coding: utf-8 -*-

from urlparse import urlparse
from httplib import HTTPConnection, socket


class Availability:
    """
    Checking ability of domain
    """
    def __init__(self, domain):
        domain = unicode(domain).encode('idna')
        hostname = urlparse("http://{0}".format(domain)).hostname
        if hostname.split(".")[:1][0] == 'www':
            domain = ".".join(hostname.split(".")[1:])
        
        self.domain = domain
        self.available = None

        try:
            if self.__get_site_status(self.domain) != 'down':
                self.available = True
            else:
                if self.__is_internet_reachable():
                    self.available = False
                else:
                    raise Exception('Internet connection is down.')
                
        except Exception, e:
            self.available = None

    def __get_site_status(self, url):
        response = self.__get_response(url)
        try:
            status = str(getattr(response, 'status'))
            if status[0] == '2' or status[0] == '3':
                return 'up'
        except AttributeError:
            pass
        return 'down'
        
    def __get_response(self, url):
        '''
        Return response object from URL
        '''
        try:
            conn = HTTPConnection(url)
            conn.request('HEAD', '/')
            return conn.getresponse()
        except Exception, e:
            return None
            
    def __is_internet_reachable(self):
        '''
        Checks Google then Yahoo just in case one is down
        '''
        if self.__get_site_status('http://www.google.ru/') == 'down' and self.__get_site_status('www.yahoo.com') == 'down':
            return False
        return True

    def get_availability(self):
        return self.available

if __name__ == "__main__" :
    domain = u'webpp.ru'
    # domain = u'россия2555.рф'

    a = Availability(domain).get_availability()

    print(a)