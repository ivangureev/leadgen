# -*- coding: utf-8 -*-

# need to install:
# `$ sudo apt-get install libxml2-dev libxslt-dev` 
# `$ sudo easy_install --allow-hosts=lxml.de,*.python.org lxml`
import urllib
from urlparse import urlparse

from lxml import etree

class Dmoz:
    """
    Class check if domain is in DMOZ catalog

    Has:
    self.domain
    self.has
    self.categories
    """
    def __init__(self, domain):
        domain = unicode(domain).encode('idna')
        hostname = urlparse("http://{0}".format(domain)).hostname
        if hostname.split(".")[:1][0] == 'www':
            domain = ".".join(hostname.split(".")[1:])
        
        self.domain = domain
        self.has = None
        self.categories = None

        try:
            url = u'http://data.alexa.com/data?cli=10&dat=s&url={0}'.format(domain)
            xml = urllib.urlopen(url.encode('utf-8')).read()
            
            root = etree.fromstring(xml)
            xml_catofories = root.xpath('/ALEXA/DMOZ/SITE/CATS/CAT')
            
            if len(xml_catofories) > 0:
                self.has = True
                self.categories = [{'url': c.get("ID"), 'name': c.get("TITLE")} for c in xml_catofories]
            else:
                self.has = False
        except Exception, e:
            self.domain = domain
            self.has = None
            self.categories = None
            
    def get_all(self):
        """
        Get dict of all dmoz
        """
        info = {
            'domain': self.domain,
            'has': self.has,
            'categories': self.categories}
        
        return info

