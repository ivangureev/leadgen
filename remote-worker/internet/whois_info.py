# -*- coding: utf-8 -*-

# need to install `$ sudo easy_install whois`
import re
from urlparse import urlparse
from urllib2 import Request, urlopen, URLError, HTTPError
from datetime import datetime

# from grab import Grab
# from whois import query

from robowhois import RoboWhois

class Whois:
    """
    Class to get WHOIS information
    about domain. It's wrapper to `whois` module

    Has:
    self.creation_date
    self.expiration_date
    self.last_updated
    self.name
    self.registrar
    """
    def __init__(self, key, domain):
        domain = unicode(domain).encode('idna')
        hostname = urlparse("http://{0}".format(domain)).hostname
        if hostname.split(".")[:1][0] == 'www':
            domain = ".".join(hostname.split(".")[1:])

        self.creation_date = None
        self.expiration_date = None
        self.last_updated = None
        self.name = domain
        self.registrar = None

        try:
            self.__whois(key, domain)
        except Exception, e:
            self.creation_date = None
            self.expiration_date = None
            self.last_updated = None
            self.name = domain
            self.registrar = None

    def __whois(self, key, domain):
        """
        Normal domain
        """
        keys = list()
        keys.append(key)

        robowhois = RoboWhois(keys)
        info = robowhois.whois(domain)

        if info:
            for key, value in info.items():
                setattr(self, key, value)
            
    def get_all(self):
        """
        Get dict of all whois properties
        """
        info = {
            'creation_date': self.creation_date,
            'expiration_date': self.expiration_date,
            'last_updated': self.last_updated,
            'name': self.name,
            'registrar': self.registrar}
        
        return info
