# -*- coding: utf-8 -*-

from urlparse import urlparse
from urllib2 import Request, urlopen, URLError, HTTPError

from grab import Grab

class Yandex:
    """
    Class for manipulation with yandex.ru servicies

    Has:
    self.in_yaca
    self.tic
    """
    def __init__(self):
        self.g = Grab()

        self.in_yaca = None
        self.rubric = None
        self.tic = None
 
    def get_yaca(self, domain):
        """
        Checking if domain in yandex catalog (True, False)
        and return yandex `tIC` of domain
        """
        domain = unicode(domain).encode('idna')
        hostname = urlparse("http://{0}".format(domain)).hostname
        if hostname.split(".")[:1][0] == 'www':
            domain = ".".join(hostname.split(".")[1:])

        # cheking URL
        url = u"http://search.yaca.yandex.ru/yca/cy/ch/{0}/".format(domain)
        url = urlparse(url)
        
        if not (url.netloc and url.scheme and url.path):
            raise Exception("URL {0} is malformed.".format(url.geturl()))
        
        self.g.go(url.geturl())
        
        try:
            # the sign than page has infortaion about domain in yaca
            query = '//img[@src="http://img.yandex.net/i/arr-hilite.gif"]'
            in_yaca_sign = self.g.xpath(query)
        
            # if domain is in yandex catalog
            if in_yaca_sign is not None:
                self.in_yaca = True

                # get rubric
                path = self.g.xpath('//div[@class="b-path"]').text_content()
                title = self.g.xpath('//h1[@class="b-rubric__title"]').text_content()
                self.rubric = u"{0} / {1}".format(path, title)
                
                # get tic
                try:
                    tic_list = in_yaca_sign.xpath('../../td[4]')
                    if len(tic_list) > 0 and tic_list[0].text:
                        self.tic = int(tic_list[0].text)
                    else:
                        self.tic = self.__get_atypical_tic(domain)
                except ValueError, e:
                    self.tic = 0
            else:
                self.in_yaca = False
        except IndexError, e:
            # if domain is not in yandex catalog
            self.in_yaca = False
            
            tic_title = self.g.xpath_text('//p[@class="b-cy_error-cy"]')
            self.tic = int(tic_title.replace(u'Индекс цитирования (тИЦ) ресурса — ',u''))
        except Exception, e:
            self.in_yaca = None
            self.rubric = None
            self.tic = None
        finally:
            return {'in_yaca': self.in_yaca, 'rubric': self.rubric, 'tic': self.tic}

    def __get_atypical_tic(self, domain):
        # cheking URL
        url = u"http://pr-cy.ru/analysis/{0}".format(domain)
        url = urlparse(url)

        if not (url.netloc and url.scheme and url.path):
            raise Exception("URL {0} is malformed.".format(url.geturl()))

        self.g.go(url.geturl())

        try:
            # get TIC
            query = '//*[@id="base"]/div[2]/div[1]/div[1]/a/b'
            tic_text = self.g.xpath(query)
            return int(tic_text.text.split('/')[0])
        except ValueError, e:
            return 0
        except IndexError, e:
            return 0
    
class YandexSearch:
    """
    Class for getting yandex search results by XML

    Has:
    -
    """
    def __init__(self, user, key, region, countresults=100):
        self.__g = Grab()

        self.__user = unicode(user)
        self.__key = unicode(key)
        self.__region = unicode(region)
        self.__countresults = unicode(countresults)

        self.__url = self.__get_url()
 
    def __get_url(self):
        """
        Gets url for searching
        """
        url = u'http://xmlsearch.yandex.ru/xmlsearch?user={0}&key={1}&lr={2}'.format(self.__user, self.__key, self.__region)
        return url

    def __get_xml_query(self, query):
        """
        Gets xml for searching
        """
        xml_template = u"""<?xml version="1.0" encoding="UTF-8"?>  
            <request>   
                <query>{query}</query>
                <groupings>
                    <groupby attr="d" mode="deep" groups-on-page="{countresults}"  docs-in-group="1" />    
                </groupings>    
            </request>"""
        query_normalized = self.__get_escape(query)

        return xml_template.format(query=query_normalized, countresults=self.__countresults)

    def __get_escape(self, text):
        """
        Produce entities within text
        """
        html_escape_table = {
            u"&": u"&amp;",
            u'"': u"&quot;",
            u"'": u"&apos;",
            u">": u"&gt;",
            u"<": u"&lt;",
        }
        
        return "".join(html_escape_table.get(c,c) for c in text)

    def __check_error(self):
        """
        Check for yandex errors in response
        """
        if not self.__g.response:
            return None

        try:
            error = self.__g.xpath('//error').text
            error_code = self.__g.xpath('//error').get('code')
        except IndexError, e:
            return None
        else:
            # return u"{code}: {error}".format(code=error_code, error=error)
            return dict(code=error_code, description=error)

    def __get_parsed_prop(self, element, property):
        """
        Get property
        """
        prop = element.find(property)
        if prop is not None:
            return prop.text_content()
        else:
            return ""

    def __parse_searching(self):
        """
        Parse searching result.
        Return list of dicts, like:
        [{
            'domain': 'www.webpp.ru',
            'title': 'Webpp create site',
            'snippet': 'We can make site fast!'
        },{
        ...
        }]
        """
        if not self.__g.response:
            return None

        result = []

        groups = self.__g.xpath_list('//group')
        for group in groups:
            for doc in group.xpath('doc'):
                domain = self.__get_parsed_prop(doc, 'domain')
                title = self.__get_parsed_prop(doc, 'title')
                
                headline = self.__get_parsed_prop(doc, 'headline')
                passages = self.__get_parsed_prop(doc, 'passages')
                snippet = "".join((headline, passages))

                if domain not in [item['domain'] for item in result]:
                    result.append({
                        'domain': domain,
                        'title': title,
                        'snippet': snippet})

        return result

    def search(self, query):
        xml = self.__get_xml_query(query)

        self.__g.setup(post=xml)
        self.__g.go(self.__url)

        error_dict = self.__check_error()
        if error_dict:
            raise YandexSearchError(error_dict['code'])

        items = self.__parse_searching()

        return items

class YandexSearchError(Exception):
    def __init__(self, code):
        self.code = code
    def __str__(self):
        return str(self.code)

